import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
from base.plotDG import plotDG

# Save the pdf?
savefig = False
# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(0.01)
# Discretization parameters
elements_coarse = 5
elements_fine = 512
element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
# VMS parameters
projector = "L2" # "L2" (not for mode=="FE"), "H10", "Nitsche" or "IP"
scale_interaction_mode = "FE" if not projector == "L2" else "CS" 
        # "CS" (original bilinear form, full inversion) or 
        # "FE" (finite element form, inversion of remaining scale-interaction)
h = width/elements_coarse
eta = 10/h
eta_bdy = 2/h +float(a_vec(0)/kappa(0)) # if projector == "Nitsche" or "IP"
DG_params = (eta,eta_bdy) 

# Connector
element_types_coarse = [ element_types_fine[0] ] if projector != "IP" else [ ('DG',element_types_fine[0][1]) ]
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
BCset = BCSet(connector)
if projector=="L2" or projector=="H10":
    BCset.add_BC(lambda x : near(x,0) or near(x,width) , lambda x : 0 ,subspace=-1,CS_subspace=0)
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa, BCset=BCset, projector=projector, \
                    DG_parameters = DG_params, scale_interaction_mode=scale_interaction_mode)

# Solve for 'true' solution and 'true' projection
Uf = VMS_problem.compute_full_scale_solution()
phi_sol_P = VMS_problem.compute_projection(Uf)
phi_sol_CS = VMS_problem.compute_coarse_scale_solution()

# Export pdf
print("Plotting solution")
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
f = plt.figure()
x,y = connector.get_1d_solution(Uf,refinement=elements_fine)
plt.plot(x,y,'-g',label="True solution")
plotDG(phi_sol_P,ls='-k',label="True projection")
plotDG(phi_sol_CS,ls=':r',label="FE w. fine-scale closure function")
plt.legend()
if savefig:
    exportname = "./Output/03/AdvDif_primal_1D_P%i_%s.pdf"%(element_types_fine[0][1],projector)
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()

