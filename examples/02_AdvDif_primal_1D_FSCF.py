import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savefig = False
# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(0.01)
# Discretization parameters
elements_coarse = 5
elements_fine = 128
element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
# VMS parameters
projector = "IP" # "L2", "H10", "Nitsche" or "IP"
h = width/elements_coarse
eta = 10/h # if projector == "IP"
eta_bdy = 2/h +float(a_vec(0)/kappa(0)) # if projector == "Nitsche" or "IP"
DG_params = (eta,eta_bdy)
# Output parameters
mode = "FSCF" # "FSGF" or "FSCF" or "OptTest" for fine-scale Green's function, fine-scale closure function or optimal testfunction
scale_interaction_mode = "FE" # For fine-scale closure functions. Either "FE" for only the advective term, or "CS" for all terms
fine_scale_adjoint_loc = 0.49

# Connector
element_types_coarse = [ element_types_fine[0] ] if projector != "IP" else [ ('DG',element_types_fine[0][1]) ]
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
BCset = BCSet(connector)
if projector=="L2" or projector=="H10":
    BCset.add_BC(lambda x : near(x,0) or near(x,width) , lambda x : 0 ,subspace=-1,CS_subspace=0)
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa, BCset=BCset, projector=projector, \
                    DG_parameters = DG_params, scale_interaction_mode=scale_interaction_mode)

# Produce force vector
if mode == "FSGF":
    print("Computing the fine-scale Green's function corresponding to point %f"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_Greens_function( fine_scale_adjoint_loc )
else:
    print("Computing the closure function corresponding to dof at loc %f"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc)
x,y = connector.get_1d_solution(U,refinement=elements_fine)

# Export pdf
print("Plotting solution")
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
f = plt.figure()
plt.plot(x,y)
plt.plot(mesh_c,np.zeros(len(mesh_c)),'o')
plt.plot(dof_c,np.zeros(len(dof_c)),'x')
if savefig:
    subdir = "Closure_functions" if mode=="FSCF" else "Greens_functions" if mode=="FSGF" else "Optimal_testfunctions"
    exportname = "./Output/02/%s/AdvDif_primal_1D_%s_%s_P%i.pdf"%(subdir,mode,projector,element_types_fine[0][1])
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()
