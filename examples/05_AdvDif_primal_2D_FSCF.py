import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
import time

# Save the pdf?
savesol = False
# Problem formulation
width = 1
a_mag = 1
a_vec = Constant( (1/sqrt(5)*a_mag, 2/sqrt(5)*a_mag) )
kappa = Constant(0.1)
elements_coarse_x = 4
elements_fine_refinement = 3
element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
# VMS parameters
projector = "IP" # "L2", "H10", "Nitsche" or "IP"
h = width/elements_coarse_x
eta = Constant( 10/h )
eta_bdy = Constant( 2/h +float(a_mag/kappa(0)) ) # if projector == "Nitsche" or "IP"
DG_params = (eta,eta_bdy)
# Output parameters
mode = "FSCF" # "FSGF" or "FSCF" for fine-scale Green's function or fine-scale closure function
fine_scale_adjoint_loc = (37/64,50/64) # Either point for FSGF or nearest dof for FSCF

# Connector
element_types_coarse = [ element_types_fine[0] ] if projector != "IP" else [ ('DG',element_types_fine[0][1]) ]
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)
    
# iterators
BCset = BCSet(connector, lambda x : near(x[0],0) or near(x[0],width) or near(x[1],0) or near(x[1],width) , lambda x : 0 ,subspace=-1,CS_subspace=0)
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa, BCset=BCset,\
                            projector=projector, DG_parameters = DG_params)

# Produce force vector
if mode == "FSGF":
    print("Computing the fine-scale Green's function corresponding to point (%f,%f)"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_Greens_function( fine_scale_adjoint_loc )
elif mode == "FSCF":
    print("Computing the closure function corresponding to dof at loc (%f,%f)"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc)

# Export pvd
subdir = "Closure_functions" if mode=="FSCF" else "Greens_functions"
name = "./Output/05/%s/Advdif_primal_2D_%s_P%i"%(subdir,projector,element_types_fine[0][1])
if savesol:
    print("Exporting solution: " + name)
    connector.exportSolution(U, name, subspace=0)
    
