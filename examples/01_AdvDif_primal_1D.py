import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savefig = False
# Problem formulation
width = 1
a_vec = Constant((1,))
kappa = Constant(0.05)
def force(ul,vq,ds,dI,**kwargs):
    v,q = split(vq)
    n = FacetNormal(vq.function_space().mesh())
    s = 0.5*inner(a_vec,n)/abs(inner(a_vec,n)) # inflow outflow switch
    return (0.5-s)*Constant(0)*q*ds + (0.5+s)*Constant(1)*q*ds
# VMS parameters 
elements_coarse = 4
elements_fine = 64
element_types_fine   = [("CG",1),("CG",1)] # Last space LMP space
element_types_coarse = [("CG",1)]

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get iterator
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa, force=force)

# Obtaining solutions
print("Starting solve")
U = VMS_problem.compute_full_scale_solution()
x,y = connector.get_1d_solution(U)
y_true = [ None if xx==None else (np.exp(a_vec(0)/kappa(0)*xx)-1)/(np.exp(a_vec(0)/kappa(0)*width)-1) for xx in x]

# Export pdf
print("Plotting solution")
connector.create_coarse_spaces([('CG',1)]) # For visualization
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
f = plt.figure()
plt.plot(x,y,'-x')
plt.plot(x,y_true)
plt.plot(mesh_c,np.zeros(len(mesh_c)),'o')
if savefig:
    exportname = "./Output/01/AdvDif_primal_1D.pdf"
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()
