import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savesol = False
# Problem formulation
width = 1
a_vec = Constant((1/sqrt(5),2/sqrt(5)))
kappa = Constant(0.1)
elements_coarse_x = 4
elements_fine_refinement = 4 # Choosing 4 due to higher order 
element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
P_coarse = 1 # Order for coarse-scale sigma
# VMS parameters
projector = "MM" # "MM" or "L2" or "LDG"
h = width/elements_coarse_x
eta = Constant( 0.001 ) if projector == "LDG" else Constant( 0 ) 
eta_bdy = Constant( kappa(0)/h ) if projector == "LDG" else Constant( 0 ) 
beta = Constant( -0.5 ) if projector == "LDG" else  Constant( (0,) ) 
C = Constant( 1 ) if projector == "LDG" else Constant( 0 )
DG_params = (eta,eta_bdy,beta,C)
# Output parameters
mode = "FSCF" # "FSGF" or "FSCF" or "OptTest" for fine-scale Green's function, fine-scale closure function or optimal testfunction
# Set either one of these to a value, the other to None
fine_scale_adjoint_loc = (37/64,50/64) # or (39/64,51/64). Either point for FSGF or nearest dof for FSCF

# Connector
element_types_coarse = [ ("BDM",P_coarse), ("DG",P_coarse-1) ] if projector == "MM" \
                  else [ ('DG',P_coarse), ('DG',P_coarse) , ('DG',P_coarse)   ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)

# Iterator
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa,\
                                projector=projector, DG_parameters = DG_params)
if element_types_fine[0][1] > 1:
    print("Should probably manually increase the bandwidth of the stiffness matrices")

# Produce force vector
if mode == "FSGF":
    print("Computing the fine-scale Green's function corresponding to point (%f,%f)"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_Greens_function( fine_scale_adjoint_loc,subspace=1 , solver="lu")
elif mode == "FSCF":
    print("Computing the closure function corresponding to dof at loc (%f,%f)"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc , solver="lu")
    
# Export to pvd
subdir = "Closure_functions" if mode=="FSCF" else "Greens_functions"
name = "./Output/10/%s/Advdif_mixed_2D_%s_P%i"%(subdir,projector,P_coarse)
if savesol:
    connector.exportSolution(U, name, subspace=0, fieldname="sig")
    connector.exportSolution(U, name, subspace=1, fieldname="phi")
    
