import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savefig = False
# Problem formulation
width = 1
a_vec = Constant((-1,))
kappa = Constant(0.05)
BC = (lambda x : near(x,0) or near(x,1) , lambda x : x/width )
def force(sul,tvq,ds,dI,*args):
    t,v,q = split(tvq)
    n = FacetNormal(tvq.function_space().mesh())
    s = 0.5*inner(a_vec,n)/abs(inner(a_vec,n)) # inflow outflow switch
    return (0.5-s)*Constant(-1)*t*ds + (0.5+s)*Constant(0)*t*ds
elements_coarse = 4
elements_fine = 256
element_types_fine = [("CG",1),("DG",0),("CG",1)] # Last space LMP space
element_types_coarse = [("CG",1),("DG",0)]

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Iterator
BCset = BCSet(connector, *BC, subspace=2, CS_subspace=-1 ) # Set LMP's on domain boundary to phi (purely for consistency)
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, force=force, BCset=BCset)

# Obtaining solutions
print("Starting solve")
U = VMS_problem.compute_full_scale_solution(solver="lu") # Mumps struggles for the 1D saddle point case
x,y_sig = connector.get_1d_solution(U)
x,y_phi = connector.get_1d_solution(U,subspace=1)
x,y_lmb = connector.get_1d_solution(U,subspace=2)
y_phi_true = [ None if xx==None else (np.exp(a_vec(0)/kappa(0)*xx)-1)/(np.exp(a_vec(0)/kappa(0)*width)-1) for xx in x]
y_sig_true = [ None if xx==None else -a_vec(0)/(np.exp(a_vec(0)/kappa(0)*width)-1) for xx in x]

# Export pdf
print("Plotting solution")
connector.create_coarse_spaces([('CG',1)] ) # for visualization
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
fig, ax = plt.subplots(2)
ax[0].set_title('Phi')
ax[0].plot(x,y_phi,'-x')
ax[0].plot(x,y_lmb)
ax[0].plot(x,y_phi_true)
ax[0].plot(mesh_c,np.zeros(len(mesh_c)),'o')
ax[1].set_title('Sig')
ax[1].plot(x,y_sig,'-x')
ax[1].plot(x,y_sig_true)
ax[1].plot(mesh_c,np.zeros(len(mesh_c)),'o')
if savefig:
    exportname = "./Output/06/AdvDif_mixed_1D.pdf"
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()   
