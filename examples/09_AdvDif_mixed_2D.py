import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savesol = False
# Problem formulation
width = 1
a_vec = Constant((1/sqrt(5),2/sqrt(5)))
kappa = Constant(0.05)
elements_coarse_x = 4
elements_fine_refinement = 3
element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
element_types_coarse = [("BDM",1),("DG",0)]

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)

# Iterator
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa )
if element_types_fine[0][1] > 1:
    print("Should probably manually increase the bandwidth of the stiffness matrices")

# Solve
print("Starting solve")
U = VMS_problem.compute_full_scale_solution()

# Export to pvd
print("Exporting solution")
name = "./Output/09/Advdif_mixed_2D"
if savesol:
    connector.exportSolution(U, name, subspace=0, fieldname="sig")
    connector.exportSolution(U, name, subspace=1, fieldname="phi")
    print("Finished export")

    ####
    # Below follows an equivalent FEniCS implementation for verification
    ####
    elements_x = elements_coarse_x*2**elements_fine_refinement
    mesh = RectangleMesh(Point(0,0),Point(width,width),elements_x,elements_x)
    BDMe = FiniteElement('BDM',mesh.ufl_cell(),element_types_fine[0][1]); DGe = FiniteElement('DG',mesh.ufl_cell(),element_types_fine[1][1])
    V = FunctionSpace(mesh,MixedElement(BDMe,DGe))
    sig,phi = TrialFunctions(V); tau,w = TestFunctions(V); sig_phi_sol = Function(V)
    solve( inner(kappa**(-1)*tau, sig )*dx - inner(kappa**(-1)*tau, a_vec*phi )*dx - (div(tau) * phi)*dx + (w * div(sig))*dx == w*dx , sig_phi_sol)
    sig_sol,phi_sol = sig_phi_sol.split()
    sig_sol.rename("sig","sig"); phi_sol.rename("phi","phi")
    File("./Output/09/FEniCS_sig.pvd") << sig_sol; File("./Output/09/FEniCS_phi.pvd") << phi_sol
