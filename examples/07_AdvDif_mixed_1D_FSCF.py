import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savefig = False
# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(0.01)
# Discretization parameters
elements_coarse = 4
elements_fine = 128
element_types_fine = [("CG",2),("DG",1),("CG",1)] # Last space LMP space
P_coarse = 1 # Order for coarse-scale sigma
# VMS parameters
projector = "LDG" # "MM" or "L2" or "LDG"
h = width/elements_coarse
eta = Constant( 0.001 ) if projector == "LDG" else Constant( 0 ) 
eta_bdy = Constant( 1 ) if projector == "LDG" else Constant( 0 ) 
beta = Constant( -0.5 ) if projector == "LDG" else  Constant( (0,) ) 
C = Constant( 1 ) if projector == "LDG" else Constant( 0 ) 
DG_params = (eta,eta_bdy,beta,C)
# Output parameters
mode = "FSGF" # "FSGF" or "FSCF" or "OptTest" for fine-scale Green's function, fine-scale closure function or optimal testfunction
scale_interaction_mode = "FE" # For fine-scale closure functions. Either "FE" for only the advective term, or "CS" for all terms
fine_scale_adjoint_loc = 0.63 # set if mode == "FSCF"

# Connector
element_types_coarse = [ ("CG",P_coarse), ("DG",P_coarse-1) ] if (projector == "MM") \
                  else [ ('DG',P_coarse), ('DG',P_coarse)   ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, projector=projector, \
                    DG_parameters = DG_params, scale_interaction_mode=scale_interaction_mode)

# Produce force vector
print("Starting solve")
if mode == "FSGF":
    print("Computing the fine-scale Green's function corresponding to point %f"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_Greens_function( fine_scale_adjoint_loc ,subspace=1, solver="lu")
else:
    print("Computing the closure function corresponding to dof at loc %f"%fine_scale_adjoint_loc)
    U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc, solver="lu")
x,y_sig = connector.get_1d_solution(U,refinement=elements_fine)
x,y_phi = connector.get_1d_solution(U,subspace=1,refinement=elements_fine)

# Export pdf
print("Exporting solution")
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
fig, ax = plt.subplots(2)
ax[0].set_title('Phi')
ax[0].plot(x,y_phi)
ax[0].plot(mesh_c,np.zeros(len(mesh_c)),'o')
ax[0].plot(dof_c,np.zeros(len(dof_c)),'x')
ax[1].set_title('Sig')
ax[1].plot(x,y_sig)
ax[1].plot(mesh_c,np.zeros(len(mesh_c)),'o')
ax[1].plot(dof_c,np.zeros(len(dof_c)),'x')
if savefig:
    subdir = "Closure_functions" if mode=="FSCF" else "Greens_functions" if mode=="FSGF" else "Optimal_testfunctions"
    exportname = "./Output/07/%s/AdvDif_mixed_1D_%s_%s_P%i.pdf"%(subdir,mode,projector,P_coarse)
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()
