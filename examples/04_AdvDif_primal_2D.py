import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

# Save the pdf?
savesol = False
# Problem formulation
width = 1
a_vec = Constant((1/sqrt(5),2/sqrt(5)))
kappa = Constant(0.1)
elements_coarse_x = 4
elements_fine_refinement = 3
element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
element_types_coarse = [("CG",1)]

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa )

# Solve
print("Starting solve")
U = VMS_problem.compute_full_scale_solution()

# Export pvd
print("Exporting solution")
name = "./Output/04/Advdif_primal_2D"
if savesol:
    connector.exportSolution(U, name, subspace=0,fieldname='phi')

    ####
    # Below follows an equivalent FEniCS implementation for verification
    ####
    elements_x = elements_coarse_x*2**elements_fine_refinement
    mesh = RectangleMesh(Point(0,0),Point(width,width),elements_x,elements_x)
    V = FunctionSpace(mesh,element_types_fine[0][0],element_types_fine[0][1])
    phi = TrialFunction(V); v = TestFunction(V); phi_sol = Function(V)
    bc = DirichletBC(V,Constant(0),lambda x,bdy: bdy)
    solve( inner(a_vec*v,grad(phi))*dx+inner(kappa*grad(phi),grad(v))*dx == v*dx , phi_sol, bc )
    phi_sol.rename("phi","phi")
    File("./Output/04/FEniCS_phi.pvd") << phi_sol
