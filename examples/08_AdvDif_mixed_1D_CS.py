import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
from base.plotDG import plotDG

# Save the pdf?
savefig = False
# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(0.01)
# Discretization parameters
elements_coarse = 4
elements_fine = 256//2
element_types_fine = [("CG",2),("DG",1),("CG",1)] # Last space LMP space
P_coarse = 1 # Order for coarse-scale sigma
# VMS parameters
projector = "LDG" # "MM", "L2" or "LDG"
scale_interaction_mode = "CS" if not projector == "L2" else "CS"  
        # "CS" (full coarse-scale bilinear form, full inversion) or 
        # "FE" (finite element bilinear form, inversion of remaining scale-interaction)
h = width/elements_coarse
eta = Constant( 0.001 ) if projector == "LDG" else Constant( 0 ) 
eta_bdy = Constant( 0.001 ) if projector == "LDG" else Constant( 0 ) 
beta = Constant( -0.5 ) if projector == "LDG" else  Constant( 0 ) 
C = Constant( 1 ) if projector == "LDG" else Constant( 0 ) 
DG_params = (eta,eta_bdy,beta,C)

# Connector
element_types_coarse = [ ("CG",P_coarse), ("DG",P_coarse-1) ] if (projector == "MM") \
                  else [ ("DG",P_coarse), ("DG",P_coarse)   ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, projector=projector, \
                    DG_parameters = DG_params, scale_interaction_mode=scale_interaction_mode)

# Solve for 'true' solution and 'true' projection
Uf = VMS_problem.compute_full_scale_solution(solver="lu")
sig_phi_sol_P = VMS_problem.compute_projection(Uf)
sig_phi_sol_CS = VMS_problem.compute_coarse_scale_solution(solver="lu")

# Export pdf
print("Exporting solution")
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
f = plt.figure()
x,y = connector.get_1d_solution(Uf,subspace=0,refinement=elements_fine)
plt.plot(x,y,'-g',label="True solution")
plotDG(sig_phi_sol_P.split()[0],ls='-k',label="True projection")
plotDG(sig_phi_sol_CS.split()[0],ls=':r',label="FE w. fine-scale closure function")
plt.legend()
if savefig:
    exportname = "./Output/08/AdvDif_mixed_1D_sig_P%i_%s.pdf"%(element_types_coarse[0][1],projector)
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
f = plt.figure()
x,y = connector.get_1d_solution(Uf,subspace=1,refinement=elements_fine)
plt.plot(x,y,'-g',label="True solution")
plotDG(sig_phi_sol_P.split()[1],ls='-k',label="True projection")
plotDG(sig_phi_sol_CS.split()[1],ls=':r',label="FE w. fine-scale closure function")
plt.legend()
if savefig:
    exportname = "./Output/08/AdvDif_mixed_1D_phi_P%i_%s.pdf"%(element_types_coarse[0][1],projector)
    plt.savefig(exportname, bbox_inches='tight')
    print("Solution is exported: " + exportname)
plt.show()