import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
import imageio

def export_fine_scale_flosure_function(eta_,C_,exportname):
    # Physical parameters
    width = 1
    a_vec = Constant((1,))
    kappa = Constant(0.01)
    # Discretization parameters
    elements_coarse = 4
    elements_fine = 128
    element_types_fine = [("CG",2),("DG",1),("CG",1)] # Last space LMP space
    P_coarse = 1 # Order for coarse-scale sigma
    # VMS parameters
    projector = "LDG" # "MM" or "L2" or "LDG"
    h = width/elements_coarse
    eta = Constant( eta_ )
    eta_bdy = Constant( 1000 )
    beta = Constant( -0.5 )
    C = Constant( 1E-5 )
    DG_params = (eta,eta_bdy,beta,C)
    fine_scale_adjoint_loc = 0.63 # set if mode == "FSCF"

    # Connector
    element_types_coarse = [ ('DG',P_coarse), ('DG',P_coarse)   ] 
    lmp_subspace = len(element_types_fine)-1
    connector = Connector_1D(width,elements_coarse)
    connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
    connector.create_coarse_spaces(element_types_coarse)

    # Get VMS_problem
    VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, projector=projector,DG_parameters = DG_params)

    U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc, solver="lu")
    x,y_sig = connector.get_1d_solution(U,refinement=elements_fine)
    x,y_phi = connector.get_1d_solution(U,subspace=1,refinement=elements_fine)

    # Export pdf
    mesh_c = connector.coarse_spaces[0].mesh().coordinates()
    dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
    fig, ax = plt.subplots(2,figsize=(3.5,5))
    ax[0].set_title('Phi')
    ax[0].plot(x,y_phi)
    ax[0].plot(mesh_c,np.zeros(len(mesh_c)),'o')
    ax[0].plot(dof_c,np.zeros(len(dof_c)),'x')
    ax[0].set_ylim([-200,200])
    ax[1].set_title('Sig')
    ax[1].plot(x,y_sig)
    ax[1].plot(mesh_c,np.zeros(len(mesh_c)),'o')
    ax[1].plot(dof_c,np.zeros(len(dof_c)),'x')
    ax[1].set_ylim([-50,50])
    plt.savefig(exportname, bbox_inches='tight')
    plt.close()



RECOMPUTE = False

subrange = range(10) # [0,5]
exp_range = [-2,-1,0,1,2,3,4]
eta_range = [ 10**(b+a/10) for b in exp_range for a in subrange ] + [10**(exp_range[-1]+1)]


#eta_range = [0.001, 1,  1000]

subdir = "./Output/1DFSCF/"
filenames = []
for i,eta in enumerate(eta_range):
    # create file name and append it to a list
    filename = f'{subdir}{i}.png'
    filenames.append(filename)
    
    # save frame
    if RECOMPUTE:
        export_fine_scale_flosure_function(eta,1E-5,filename)
        if i%5 == 0:
            print("############ %i out of %i"%(i,len(eta_range)))

# build gif
with imageio.get_writer(f'{subdir}mygif.gif', mode='I',duration=0.1) as writer:
    for filename in filenames:
        image = imageio.imread(filename)
        writer.append_data(image)
        
# Remove files
# for filename in set(filenames):
#     os.remove(filename)