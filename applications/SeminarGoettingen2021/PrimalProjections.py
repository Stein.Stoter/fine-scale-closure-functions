import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
from base.plotDG import exportDG
import time

# Variables
a_mag  = 1
kappa_ = 0.001
projector = sys.argv[1] # "L2", "H10", "Nitsche" or "IP"
if projector=="IP":
    eta_   = sys.argv[2]
    eta_bdy_  = sys.argv[3]
    summary = "IP projector, a_mag %s, kappa %s, eta %s, eta_bdy %s"%(a_mag,kappa_,eta_,eta_bdy_)
elif projector=="Nitsche":
    eta_bdy_  = sys.argv[2]
    eta_ = "0"
    summary = "Nitsche projector, a_mag %s, kappa %s, eta %s"%(a_mag,kappa_,eta_bdy_)
else:
    eta_ = eta_bdy_ = "0"
    summary = projector + " a_mag %s, kappa %s, eta %s, eta_bdy %s"%(a_mag,kappa_,eta_,eta_bdy_)
print(summary)

# Export flags
exportTrueSol = False

# Problem formulation
width = 1
a_mag = 1
a_vec = Constant( (1/sqrt(5)*a_mag, 2/sqrt(5)*a_mag) )
kappa = Constant(0.001)
f_glob = Expression("( 2*(x[0]) - (x[1]) < 0  ) ? 1 : -1",degree=0)
def force(sul,tvq,ds,dI,x,*args):
    f = Expression("( 2*(%f+x[0]) - (%f+x[1]) < 0  ) ? 1 : -1"%(x[0],x[1]),degree=0)
    v,q = split(tvq)
    return f*v*dx
# Discretization parameters
elements_coarse_x = 16
elements_fine_refinement = 5
element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
# VMS parameters
h = width/elements_coarse_x
eta = Constant( eval(eta_) ) 
eta_bdy = Constant( eval(eta_bdy_) )
DG_params = (eta,eta_bdy)

# Connector
element_types_coarse = [ element_types_fine[0] ] if projector != "IP" else [ ('DG',element_types_fine[0][1]) ]
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)
    
# iterators
if projector == "H10" or projector == "L2":
    BCset = BCSet(connector, lambda x : near(x[0],0) or near(x[0],width) or near(x[1],0) or near(x[1],width) , lambda x : 0 ,subspace=-1,CS_subspace=0)
else:
    BCset = BCSet()
VMS_problem = getVMSProblem_advection_diffusion_primal(connector, a_vec,kappa, BCset=BCset,force=force,\
                            projector=projector, DG_parameters = DG_params)

# Solve true solution
U_f = VMS_problem.compute_full_scale_solution()
if exportTrueSol:
    name = "./Output/Projections/TrueSolutionPrimal"
    connector.exportSolution(U_f, name, subspace=0, fieldname="phi")
print("Solved full-scale problem")

# Obtain true projection
print("Computing projection")
phi_p = VMS_problem.compute_projection(U_f)
phi_p.rename("phi","phi")
exportDG(phi_p,"./Output/Projections/%s_e%s_eb%s"%(projector,eta_,eta_bdy_) ,refinefactor=1,scale=0.3)
