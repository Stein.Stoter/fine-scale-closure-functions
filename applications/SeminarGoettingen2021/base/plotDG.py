from dolfin import *
import mpl_toolkits.mplot3d as a3
import matplotlib
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import pylab as pl
import numpy as np
##import scipy as sp


def plotDG(u,**kwargs):
    d = u.function_space().mesh().geometry().dim()
    if d == 1:
        return plotDG1D(u,**kwargs)
    if d == 2:
        return plotDG2D(u,**kwargs)


def plotDG1D(u,refinefactor=2,plot=True, figax=False,ls=None,**kwargs):
    V = u.function_space()
    PolDeg = V.ufl_element().degree()
    dofmap = V.dofmap()

    X = []
    Y = []
    for c in cells(V.mesh()):
        X.append(None)
        Y.append(None)

        x0,x1 = c.get_vertex_coordinates()
        c_dofs = dofmap.cell_dofs(c.index())
        
        X.append(x0)
        Y.append(float(u.vector()[c_dofs[0]]))
            
        if PolDeg > 1:
            for x in np.linspace(x0,x1,refinefactor**PolDeg)[1:-1]:
                X.append(x)
                Y.append(float(u(x)))

        X.append(x1)            
        if PolDeg > 0:
            Y.append( float(u.vector()[c_dofs[1]]) )
        else:
            Y.append( float(u.vector()[c_dofs[0]]) )

    ls = "-" if ls is None else ls
    plt.plot(X,Y,ls,**kwargs)
##    fig, ax = plt.subplots(1) if figax == False else figax
##    if ls == None:
##        ax.plot(X,Y)
##    else:
##        ax.plot(X,Y,ls)
##    fig.tight_layout()
##    if plot:
##        plt.show()
##    return fig,ax


      
def plotDG2D(u,refinefactor=3,exactSol=None,showedges = False,plot=True):
    V = u.function_space()
    mesh = V.mesh()
    PolDeg = V.ufl_element().degree()
    V = FunctionSpace(mesh,'DG',PolDeg) #fixes an issue with mixed methods
    u = project(u,V) #fixes an issue with mixed methods
    if PolDeg > 1:
        for i in range(refinefactor):
            mesh = refine(mesh)
        V = FunctionSpace(mesh, "DG", 1)
        
        
    dofmap = V.dofmap()
    gdim = mesh.geometry().dim()
    dofs = dofmap.dofs()
    dofs_x = V.tabulate_dof_coordinates().reshape((-1, gdim))
        
    ax = a3.Axes3D(pl.figure())
    cmap = matplotlib.cm.get_cmap('jet')
    minz = -1#, min(u.vector().array())
    maxz = 1#max(u.vector().array())
    for c in cells(V.mesh()):
        c_dofs = dofmap.cell_dofs(c.index())
        
        XY_mp = c.midpoint()
        z_mp = u(XY_mp)
        vec_mp = np.array([XY_mp.x(),XY_mp.y()])
        zcolor = cmap(  (z_mp-minz)/(maxz-minz)  )

        vtx = np.zeros((3,3))
        for i,d in enumerate(c_dofs):
            x,y = dofs_x[d][:]
            vec = np.array([x,y])
            z = u(Point(vec))
            vec2 = vec+0.001*(vec_mp-vec)
            z2 = u(  Point(vec2)  )
            if abs(z-z2) > 0.001:
                z = z2
                vec = vec2
            if not exactSol == None:
                z = exactSol(vec)-z

            vtx[i][:] = [x,y,z]

        tri = a3.art3d.Poly3DCollection([vtx])
        tri.set_color(colors.rgb2hex(zcolor) )
        if showedges:
            tri.set_edgecolor('k')
        ax.add_collection3d(tri)

    if plot:
        pl.show()





def exportDG(u,name,refinefactor=5,exactSol=None,scale=1):
    print("Exporting solution")
    V = u.function_space()
    mesh = V.mesh()
    PolDeg = V.ufl_element().degree()
    V = FunctionSpace(mesh,'DG',PolDeg) #fixes an issue with mixed methods
    uname = u.name()
    u = project(u,V) #fixes an issue with mixed methods
    u.rename(uname,uname)
    if PolDeg > 1:
        for i in range(refinefactor):
            mesh = refine(mesh)
        V = FunctionSpace(mesh, "DG", 1)


    dofmap = V.dofmap()
    gdim = mesh.geometry().dim()
    dofs = dofmap.dofs()
    dofs_x = V.tabulate_dof_coordinates().reshape((-1, gdim))
        

    ptxyz = ""
    connec = ""
    offsets = ""
    types = ""
    dataval = ""
    ct = 0
    for c in cells(V.mesh()):
        c_dofs = dofmap.cell_dofs(c.index())
        
        XY_mp = c.midpoint()
        z_mp = u(XY_mp)
        vec_mp = np.array([XY_mp.x(),XY_mp.y()])
        for i,d in enumerate(c_dofs):
            x,y = dofs_x[d][:]
            vec = np.array([x,y])
            z = u(Point(vec))
            vec2 = vec+0.00001*(vec_mp-vec)
            z2 = u(  Point(vec2)  )
            if abs(z-z2) > 0.0000001:
                z = z2
                vec = vec2
            if not exactSol == None:
                z = exactSol(vec)-z
            
            ptxyz+=" "+str(x)+" "+str(y)+" "+str(z*scale)
            dataval+= " "+str(z)
            connec+= " "+str(ct)
            ct+=1
        offsets+=" "+str(ct)
        types+=" 5"
        
    vtktxt = """<?xml version="1.0"?>
<VTKFile type="UnstructuredGrid"  version="0.1"  >
<UnstructuredGrid>
<Piece  NumberOfPoints=" """+str(ct)+""" " NumberOfCells=" """+str(ct/3)+""" ">
<Points>
<DataArray  type="Float64"  NumberOfComponents="3"  format="ascii">"""+ptxyz+"""</DataArray>
</Points>
<Cells>
<DataArray  type="UInt32"  Name="connectivity"  format="ascii">"""+connec+"""</DataArray>
<DataArray  type="UInt32"  Name="offsets"  format="ascii">"""+offsets+"""</DataArray>
<DataArray  type="UInt8"  Name="types"  format="ascii">"""+types+"""</DataArray>
</Cells>
<PointData  Scalars="f_31"> 
<DataArray  type="Float64"  Name=" """+u.name()+""" "  format="ascii">"""+dataval+"""</DataArray> 
</PointData> 
</Piece>
</UnstructuredGrid>
</VTKFile>"""
    f = open(name+'.vtu','w')
    f.write(vtktxt)
    f.close()

if __name__ == '__main__':
    mesh = IntervalMesh(4, 0, 1.5)
    V = FunctionSpace(mesh,'CG',1)
    u = project(Expression('1',degree=1),V)
    figax = plotDG(u,plot=False)
    figax[0].set_size_inches(6, 3, forward=True)
    figax[1].set_ylim([-0.7,0.7])
    figax[1].set_xlabel(r'$x$',fontsize=18)
    figax[1].set_xlim([0,1.5])
    figax[1].spines['bottom'].set_position('zero')
    figax[1].spines['top'].set_position('zero')
    figax[1].spines['right'].set_position('zero')
    h = figax[1].set_ylabel(r'$\tau$',fontsize=18)
    h.set_rotation(0)
    figax[1].xaxis.set_label_coords(0.87, -0.05)
    figax[1].yaxis.set_label_coords( -0.09,0.88)
    figax[0].tight_layout()

    plt.show()

    
