import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt

"""
Plots the fine-scale closure function of the LDG projector.
"""

# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(0.01)
# Discretization parameters
elements_coarse = 5
elements_fine = 256*2*2
element_types_fine = [("CG",1),("DG",0),("CG",1)] # Last space LMP space
P_coarse = 1 # Order for coarse-scale sigma
# VMS parameters
projector = "LDG"
h = width/elements_coarse
eta = Constant( 0.1 )
eta_bdy = Constant( 10 )
beta = Constant( -0.5 )
C = Constant( 0.01 )
DG_params = (eta,eta_bdy,beta,C)
# Output parameters
fine_scale_adjoint_loc = 0.6

# Connector
element_types_coarse = [ ('DG',P_coarse), ('DG',P_coarse)   ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Get VMS_problem
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, projector=projector, \
                    DG_parameters = DG_params)

# Compute closure function
print("Computing the closure function corresponding to dof at loc %f"%fine_scale_adjoint_loc)
U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc, solver="lu")

# Get xy data of all fields
x,y_sig = connector.get_1d_solution(U,refinement=elements_fine)
x,y_phi = connector.get_1d_solution(U,subspace=1,refinement=elements_fine)
x,y_lmp = connector.get_1d_solution(U,subspace=2,refinement=elements_fine)
x_lmp_left = []; y_lmp_left = []; x_lmp_right = []; y_lmp_right = []
sad = len(x)//elements_coarse
for el in range(elements_coarse-1):
    x_lmp_left.append(x[sad*el+1])
    y_lmp_left.append(y_lmp[sad*el+1])
    x_lmp_right.append(x[sad*(el+1)-1])
    y_lmp_right.append(y_lmp[sad*(el+1)-1])
x_lmp_left.append(x[sad*(elements_coarse-1)+1])
y_lmp_left.append(y_lmp[sad*(elements_coarse-1)+1])
x_lmp_left.append(x[sad*(elements_coarse)-1])
y_lmp_left.append(y_lmp[sad*(elements_coarse)-1])
    
# Get xy data of coarse-scale basis function
basis_subspace = 0
dof = connector.get_coarse_dof_from_loc( fine_scale_adjoint_loc, subspace=basis_subspace )
u = Function(connector.coarse_spaces[basis_subspace])
u.vector()[dof] = 1
U_basis = create_empty_vector( connector.total_elements*connector.W_size )
for el_nr,el,el_type in connector.iterate_elements():
    dofs,locs = get_local_dofs_and_locs(el.W,subspace=basis_subspace,bdy_spaces=connector.bdy_spaces)
    dofs += el_nr*connector.W_size
    locs += connector.get_element_location(el_nr).array()[:connector.dimension]
    vals = np.array( [u(loc) for loc in locs] )

    # The following is a DG fix
    mindof = np.argmin(locs); minloc = locs[mindof]
    maxdof = np.argmax(locs); maxloc = locs[maxdof]
    if sum(vals)/len(vals) < 0.01:
        vals = np.zeros(len(vals))
    else:
        vals[mindof] = 2*u(minloc+1E-6)-u(minloc+2E-6)
        vals[maxdof] = 2*u(maxloc-1E-6)-u(maxloc-2E-6)
        
    U_basis.vec().setValues(dofs,vals)
x,y_tau = connector.get_1d_solution(U_basis,refinement=elements_fine)

# Export pdf
print("Exporting solution")
mesh_c = connector.coarse_spaces[0].mesh().coordinates()
dof_c = connector.coarse_spaces[0].tabulate_dof_coordinates()
fig, ax = plt.subplots( 1,figsize=(5,2.5) )
ax.set_xlim(-0.01, 1.01)
ax.plot(x,y_phi,label="$g'$")
ax.plot(x_lmp_left ,y_lmp_left, '<',markersize=3,label="$l^-\,\!'$")
ax.plot(x_lmp_right,y_lmp_right,'>',markersize=3,label="$l^+\,\!'$")
ax.legend(frameon=False)
plt.savefig("./Output/Sec61/1D_LDG_phi.pdf", bbox_inches='tight')
fig, ax = plt.subplots(1,figsize=(5,2.5) )
ax.set_xlim(-0.01, 1.01)
ax.plot(x,y_tau,'--k',label="$\\tau^h$")
ax.plot(x,y_sig,'brown',label="$h'$")
ax.legend(frameon=False)
plt.savefig("./Output/Sec61/1D_LDG_sig.pdf", bbox_inches='tight')
plt.show()
