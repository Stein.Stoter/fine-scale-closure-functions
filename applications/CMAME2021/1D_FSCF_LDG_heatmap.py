import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import datetime

"""
Generates the heatmap of the spread of the fine-scale closure function of the LDG projector as a function
of the parameters in that projector. 
"""

# CLI variables
try:
    kappa_ = float(sys.argv[1])
    elements_coarse_ = int(sys.argv[2])
    beta_sign = int(sys.argv[3])
except:
    kappa_ = 0.01
    elements_coarse_ = 5
    beta_sign = 1

# Physical parameters
width = 1
a_vec = Constant((1,))
kappa = Constant(kappa_)
# Discretization parameters
elements_coarse = elements_coarse_
elements_fine = 256
element_types_fine = [("CG",1),("DG",0),("CG",1)] # Last space LMP space
P_coarse = 1 # Order for coarse-scale sigma
projector = "LDG" # "MM" or "L2" or "LDG"
h = width/elements_coarse
fine_scale_adjoint_dof = 4 if elements_coarse == 5 else 20 # 20 for 16 elements, 4 for 5 elements

# Connector
element_types_coarse = [ ('DG',P_coarse), ('DG',P_coarse)   ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_1D(width,elements_coarse)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine)
connector.create_coarse_spaces(element_types_coarse)

# Eta range and labels
subrange = range(10) # [0,5]
exp_range = [-2,-1,0,1,2,3,4]
eta_range = [ 10**(b+a/10) for b in exp_range for a in subrange ] + [10**(exp_range[-1]+1)]
labels_Nones = [ [i]+[None]*(len(subrange)-1) for i in exp_range ] + [[exp_range[-1]+1]]
labels_Nones = [item for sublist in labels_Nones for item in sublist]
eta_labels = ["" if i is None else "$10^{%i}$"%i for i in labels_Nones ]

# C range and labels
exp_range = [-5,-4,-3,-2,-1,0]
C_range = [ 10**(b+a/10) for b in exp_range for a in subrange ] + [10**(exp_range[-1]+1)]
labels_Nones = [ [i]+[None]*(len(subrange)-1) for i in exp_range ] + [[exp_range[-1]+1]]
labels_Nones = [item for sublist in labels_Nones for item in sublist]
C_labels = ["" if i is None else "$10^{%i}$"%i for i in labels_Nones ]
eta_x_C = np.meshgrid(eta_range,C_range)

support = np.zeros( eta_x_C[0].shape )
start = time.time()
for i,eta_ in enumerate(eta_range):
    for j,C_ in enumerate(C_range):
        eta = Constant( eta_ )
        eta_bdy = Constant( 100 )
        beta = Constant( beta_sign*0.5 )
        C = Constant( C_ )
        DG_params = (eta,eta_bdy,beta,C)
        
        # Get VMS_problem
        VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa, projector=projector, \
                            DG_parameters = DG_params, scale_interaction_mode="FE")

        # Produce force vector
        U = VMS_problem.compute_fine_scale_closure_function( dof=fine_scale_adjoint_dof, solver="lu")
        x,y_sig = connector.get_1d_solution(U,refinement=elements_fine)
        x,y_phi = connector.get_1d_solution(U,subspace=1,refinement=elements_fine)
        y_sig = y_sig[ y_sig != np.array(None) ]
        y_phi = y_phi[ y_phi != np.array(None) ]
        
        support[j,i] = (len(np.where(abs(y_sig)>0.1)[0] ) + len(np.where(abs(y_phi)>1)[0]) ) / ( len(y_sig) + len(y_phi) )
    if i%5 == 0:
        print("############ %i out of %i in %s"%(i,len(eta_range), time.time()-start))

np.save("./Output/Sec63_heatmap/%s"%(datetime.datetime.now()),support)

fig, ax = plt.subplots(figsize=(5,4))
im = ax.imshow(support,origin='lower',cmap=plt.cm.plasma,vmin=0,vmax=1)
ax.set_xticks(np.arange(len(eta_range)))
ax.set_yticks(np.arange(len(C_range)))
ax.set_xticklabels(eta_labels)
ax.set_yticklabels(C_labels)
ax.set_xlabel("$\eta$")
h = ax.set_ylabel("$C$")
h.set_rotation(0)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
plt.colorbar(im, cax=cax)
fig.tight_layout()
name = "e%i_k%s_beta_%s"%(elements_coarse_,str(kappa_)[2:], "positive" if beta_sign>0 else "negative")
plt.savefig("./Output/Sec63_heatmap/%s.png"%name, bbox_inches='tight', dpi=300)
#plt.show()
