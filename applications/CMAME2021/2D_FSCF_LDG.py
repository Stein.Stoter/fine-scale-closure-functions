import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
import time

"""
Commandline runnable file to generate 2D fine-scale closure functions for the LDG projector
Run as:
python3 ./2D_FSCF_LDG.py kappa eta eta_bdy beta C
e.g.:
python3 ./2D_FSCF_LDG.py 0.001 0.1 100 0.5 0.1
"""

# Variables
a_mag  = 1
kappa_ = float(sys.argv[1]) # 0.01
eta_   = sys.argv[2] # "0.2"
eta_bdy_  = sys.argv[3]
beta_  = sys.argv[4]
C_     = sys.argv[5]
summary = "a_mag %s, kappa %s, eta %s, eta_bdy %s, beta %s, C %s"%(a_mag,kappa_,eta_,eta_bdy_,beta_,C_)
print(summary)

# Problem formulation
width = 1
a_vec = Constant((a_mag/sqrt(5),a_mag*2/sqrt(5)))
kappa = Constant(kappa_)
elements_coarse_x = 16
elements_fine_refinement = 5 # Choosing 4 due to higher order 
element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
element_types_coarse = [ ('DG',1), ('DG',1) , ('DG',1)   ] 
# VMS parameters
h = width/elements_coarse_x
eta = Constant( eval(eta_) ) 
eta_bdy = Constant( eval(eta_bdy_) )
beta = Constant( beta_ )
C = Constant( eval(C_) )
DG_params = (eta,eta_bdy,beta,C)
# Point for nearest dof for FSCF
fine_scale_adjoint_loc = (0.62,0.8)

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)

# Iterator
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa,\
                                projector="LDG", DG_parameters = DG_params)

# Solve
print("Computing the closure function corresponding to dof at loc (%f,%f)"%fine_scale_adjoint_loc)
U = VMS_problem.compute_fine_scale_closure_function(fine_scale_adjoint_loc)

# Export test, must be verified in Paraview
name = "./Output/Sec63/LDG_k%s_e%s_eb%s_b%s_C%s"%(kappa_, eta_,eta_bdy_, beta_, C_)
connector.exportSolution(U, name, subspace=0, fieldname="sig")
connector.exportSolution(U, name, subspace=1, fieldname="phi")
connector.exportSolution(U, name, subspace=2, fieldname="lmp")
