import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
import time

"""
Create 2D fine-scale closure functions for either the LDG or the mixed methods projector, with the BDM basis for the adjoint
"""

##
# Cases: kappa_ = 0.01,  eta_ = eta_bdy = 20, beta_ = 0.5, C_ = 0
# Cases: kappa_ = 0.002, eta_ = eta_bdy = 20, beta_ = 0.5, C_ = 0
##

compute_MM = True

# Variables
a_mag    = 1
kappa_   = 0.01
eta_     = "20"
eta_bdy_ = "20"
beta_    = 0.5
C_       = "0"
summary  = "a_mag %s, kappa %s, eta %s, eta_bdy %s, beta %s, C %s"%(a_mag,kappa_,eta_,eta_bdy_,beta_,C_)
print(summary)

# Problem formulation
width = 1
a_vec = Constant((a_mag/sqrt(5),a_mag*2/sqrt(5)))
kappa = Constant(kappa_)
elements_coarse_x = 4
elements_fine_refinement = 4
element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
# VMS parameters
h = width/elements_coarse_x
eta = Constant( eval(eta_) )
eta_bdy = Constant( eval(eta_bdy_) )
beta = Constant( beta_ )
C = Constant( eval(C_) )
DG_params = (eta,eta_bdy,beta,C)
# Point for nearest dof for FSCF
fine_scale_adjoint_loc = (38/64,51/64)

# Create the DG VMS problem
element_types_coarse = [ ('DG',1), ('DG',1) , ('DG',1) ] 
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa,\
                                projector="LDG", DG_parameters = DG_params)

# Create the MM VMS problem 
# (little inefficient: has to reassemble large parts of the stiffness matrix twice)
connector_MMdof = Connector_2D_triangular(width,elements_coarse_x)
connector_MMdof.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector_MMdof.create_coarse_spaces( [ ("BDM",1), ("DG",0) ] )
VMS_problem_MM = getVMSProblem_advection_diffusion_mixed(connector_MMdof, a_vec,kappa,\
                                projector="MM")

# Get the MM closure function
if compute_MM:
    U_MM = VMS_problem_MM.compute_fine_scale_closure_function( fine_scale_adjoint_loc )

# Get the LDG closure function with the tau_MM RHS basis
VMS_problem.fine_scale_problem.initialize_F()
place_vector(VMS_problem_MM.fine_scale_problem.F,VMS_problem.fine_scale_problem.F)
VMS_problem.fine_scale_problem.solve(solver="lu")
U = VMS_problem.fine_scale_problem.U

# Export closure function mixed method
if compute_MM:
    name = "./Output/Sec62/MM_k%i"%(int(kappa_*1000))
    connector.exportSolution(U_MM, name, subspace=0, fieldname="sig")
    connector.exportSolution(U_MM, name, subspace=1, fieldname="phi")
    connector.exportSolution(U_MM, name, subspace=2, fieldname="lmp")
    
# Export closure function LDG method
name = "./Output/Sec62/LDG_k%i"%(int(kappa_*1000))
connector.exportSolution(U, name, subspace=0, fieldname="sig")
connector.exportSolution(U, name, subspace=1, fieldname="phi")
connector.exportSolution(U, name, subspace=2, fieldname="lmp")


