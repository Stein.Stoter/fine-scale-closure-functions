import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from VMSpy import *
from dolfin import *
import matplotlib.pyplot as plt
from base.plotDG import exportDG
import time

"""
Commandline runnable file to generate 2D fine-scale closure functions for the LDG projector
Run as:
python3 ./2D_FSCF_LDG.py kappa eta eta_bdy beta C
e.g.:
python3 ./2D_FSCF_LDG.py 0.001 0.1 100 0.5 0.1
"""

# Export flags
exportFEniCS_fullscale = False
exportFEniCS_LDG = False
method = 1 # eta = 1, C = 0.1
method = 2 # eta = 1E5, C = 1E-5

# Problem formulation
width = 1
a_mag  = 1
a_vec = Constant((a_mag/sqrt(5),a_mag*2/sqrt(5)))
kappa = Constant(0.001)
f_glob = Expression("( 2*(x[0]) - (x[1]) < 0  ) ? 1 : -1",degree=0)
def force(sul,tvq,ds,dI,x,*args):
    f = Expression("( 2*(%f+x[0]) - (%f+x[1]) < 0  ) ? 1 : -1"%(x[0],x[1]),degree=0)
    t,v,q = split(tvq)
    return -f*v*dx
# Discretization parameters
elements_coarse_x = 16
elements_fine_refinement = 5 # Choosing 4 due to higher order 
element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
element_types_coarse = [ ('DG',1), ('DG',1), ('DG',1) ] # sig_x, phi, sig_y
# VMS parameters
projector = "LDG"
if method == 1:
    eta = Constant( 1 ) 
    eta_bdy = Constant( 1 )
    beta = Constant( 0.5 )
    C = Constant( 0.1 )
elif method == 2:
    eta = Constant( 1E5 ) 
    eta_bdy = Constant( 1E5 )
    beta = Constant( 0.5 )
    C = Constant( 1E-5 )
DG_params = (eta,eta_bdy,beta,C)

if exportFEniCS_fullscale:
    #Compute the FEniCS true solution
    mesh = RectangleMesh(Point(0,0),Point(width,width),1000,1000)
    VCG = FunctionSpace(mesh,'CG',1)
    phi = TrialFunction(VCG); v = TestFunction(VCG); phi_sol = Function(VCG)
    bc = DirichletBC(VCG,Constant(0),lambda x,bdy: bdy)
    solve( inner(a_vec*v,grad(phi))*dx+inner(kappa*grad(phi),grad(v))*dx == f_glob*v*dx , phi_sol, bc )
    phi_sol.rename("phi","phi")
    File("./Output/Sec63_projections/TrueSolutionFEniCS.pvd") << phi_sol

if exportFEniCS_LDG:
    # Compute the FEniCS LDG solution true solution (quick reference)
    mesh = RectangleMesh(Point(0,0),Point(width,width),16,16)
    BDMe,DGe = FiniteElement('BDM', mesh.ufl_cell(), 1),FiniteElement('DG', mesh.ufl_cell(), 1)
    VDG = FunctionSpace(mesh, MixedElement(BDMe,DGe)  )
    sig,phi = TrialFunctions(VDG); tau,w = TestFunctions(VDG); sig_phi_sol_FE = Function(VDG)
    n = FacetNormal(VDG.mesh())
    nn = Constant((1,0.5))
    n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
    a_CS = kappa**(-1)*inner(tau, sig)*dx - kappa**(-1)*inner(a_vec*phi,tau)*dx \
                - div(tau) * phi * dx - w * div(sig) * dx \
                + jump(tau,n)*avg(phi)*dS + jump(sig,n)*avg(w)*dS \
                + jump(tau,n)*inner(avg(beta*n_tilde),jump(phi,n))*dS + jump(sig,n)*inner(avg(beta*n_tilde),jump(w,n))*dS \
                + kappa**-1*C*jump(tau,n)*jump(sig,n)*dS + kappa*eta*inner(jump(phi,n),jump(w,n))*dS \
                + kappa*eta_bdy*phi*w*ds
    solve( a_CS == -f_glob*w*dx , sig_phi_sol_FE )
    sig_FE,phi_FE = sig_phi_sol_FE.split()
    exportDG(phi_FE,"./Output/Sec63_projections/phi_FE",refinefactor=1,scale=0.3)

# Connector
lmp_subspace = len(element_types_fine)-1
connector = Connector_2D_triangular(width,elements_coarse_x)
connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
connector.create_coarse_spaces(element_types_coarse)

# Iterator
VMS_problem = getVMSProblem_advection_diffusion_mixed(connector, a_vec,kappa,force=force,\
                                projector=projector, DG_parameters = DG_params)

# Solve true solution
name = "./Output/Sec63_projections/TrueSolution"
U_f = VMS_problem.compute_full_scale_solution()
connector.exportSolution(U_f, name, subspace=1, fieldname="phi")
print("Solved full-scale problem")

# Obtain true projection
print("Computing projection")
U_p = VMS_problem.compute_projection(U_f)
sigx_p,phi_p,sigy_p = U_p.split()
exportDG(phi_p,"./Output/Sec63_projections/phi_p_%i"%method,refinefactor=1,scale=0.3)