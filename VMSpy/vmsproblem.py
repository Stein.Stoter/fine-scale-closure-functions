from dolfin import *
import numpy as np
from petsc4py import PETSc
from multipledispatch import dispatch
from . import *

def CONFIRM_IMPORT_VMSPROBLEM():
    return "confirmed"

class WeakFormSet:
    """
    Simple data holder set for collecting multiple weak forms, that involve
    different element iteration routines and potentially different subspaces.
    """
    def __init__(self):
        self.data = []
        
    def add_routine(self,WF,routine,space=0):
        """
        Add a weak formulation as a function weak_form(u,v,ds,dI) and an assembly routine.
        Assembly routines are probably either "full_element", "per_internal_face_column", 
        "per_internal_face_row", "per_internal_face" or empty "". When applicable, the 
        coarse-scale space that it involves can also be specified (for coarse-scale 
        constraints and scale interaction).
        """
        self.data.append([WF,routine,space])

    def get_routines(self):
        """
        Returns all the routines that have previously been added.
        """
        return [r for wf,r,s in self.data]

    def get_weak_forms(self):
        """
        Returns all the weak formulations that have previously been added.
        """
        return [wf for wf,r,s in self.data]

    def get_spaces(self):
        """
        Returns all the space numbers that have previously been added.
        """
        return [s for wf,r,s in self.data]

    def __len__(self):
        """
        Returns the number of forms/routines that have been supplied.
        """
        return len(self.data)

class BCSet:
    """
    Once the BCs are collected, this object can be passed to the VMSProblem object. This
    then simplifies handling the differen boundary conditions:
    -  on the full-scale problem: immediately as a true boundary condition.
    -  on the fine-scale problem: if subspace is not the LMP space, then the
       boundary condition constrains the coarse-scale basis functions
       and thereby removes their constraint equations.
    -  on the foarse-scale problem: if subspace is not the LMP space, then 
       the same BC is added as a DirichletCondition on the coarse-scale
       FE formulation.
    which otherwise all require a slightly different structure.
    """
    def __init__(self,connector=None,*args,**kwargs):
        self.data = []
        self.connector = connector
        if len(kwargs) > 0:
            self.add_BC(*args,**kwargs)
        
    def add_BC(self,insidefunc=lambda x: False,valuefunc=lambda x: 0,subspace=0,CS_subspace=None):
        """
        insidefunc : function locations -> bools
        valuefun: optional function array locations -> array floats (defaults to zero's)
        subspace: optional int for the fine-scale subspace. -1 can be set to only apply
            the bc to the coarse-scale space.
        CS_subspace: optional int for the coarse-scale subspace (defaults to `subspace`)
            -1 can be set to only apply the bc to the fine-scale space.

        `subspace` and `CS_subspace` can be different, for instance if subspace refers 
        to the LMP.
        """
        CS_subspace = subspace if type(CS_subspace)==type(None) else CS_subspace
        self.data.append([insidefunc,valuefunc,subspace,CS_subspace])

    def iterate_fine_scale_BCs(self):
        """
        Usable as:
        for (insidefunc,valuefunc,subspace) in iterate_fine_scale_BCs
        which is the required input for apply_boundary_condition
        """
        for insidefunc,valuefunc,subspace,_ in self.data:
            ins = lambda X: [insidefunc(x) for x in X]
            vals = lambda X: [valuefunc(x) for x in X]
            if subspace != -1:
                yield (ins,vals,subspace)

    def iterate_coarse_scale_BCs(self):
        """
        Usable as:
        for (insidefunc,CS_subspace) in iterate_coarse_scale_BCs
        which is the required input for eliminate_coarse_scale_constraints
        """
        for insidefunc,_,_,CS_subspace in self.data:
            if CS_subspace != -1:
                yield (insidefunc,CS_subspace)

    def iterate_FEniCS(self):
        """
        Usable as:
        for bc in iterate_FEniCS
        where bc may be used to bc.apply(K,F)
        """
        for insidefunc,valuefunc,_,CS_subspace in self.data:
            if CS_subspace == -1:
                continue
            V = self.connector.coarse_space # inside of loop to avoid query when empty
            Vsub = V if V.num_sub_spaces() == 0 else V.sub(CS_subspace)
            bc = DirichletBC(Vsub,Constant(0),insidefunc )
            ins = lambda x,b: insidefunc(x)
            class Expr(UserExpression):
                def eval(self, values, x):
                    values[:] = valuefunc(x)
            val = Expr()
            yield DirichletBC(Vsub,val,ins)


class VMSProblem:
    """
    This is the main interface for user-interaction. Given a connector (=coarse-scale
    and fine-scale discretization), and the appropriate weak forms, this class offers
    functionality for computing fine-scale Green's functions, fine-scale closure
    functions, full-scale solutions, projections and coarse-scale solutions for
    various choices of closures (either exact or approximate).

    The following weak forms are can be supplied:
    - Those for the full-scale problem (*1,*2,*3,*4,*6)
    - Those for the coarse-scale constraints (*2,*3,*6)
    - Scale interaction (*3,*6)
    - Coarse-scale projector on coarse-scale bases (*4)
    - Coarse-scale problem without fine scales (*5,*6,*7)
    - Coarse-scale/fine-scale bilinear form for full problem (*6,*7)

    *1: for getting the full-scale solution
    *2: for getting the fine-scale Green's function
    *3: for getting the fine-scale closure function
    *4: for getting the projection of the full-scale solution
    *5: for getting the basic finite element solution
    *6: for getting the coarse-scale solution with exact scale interaction
    *7: for getting the coarse-scale solution with modeled scale interaction
    """
    
    # @dispatch(Connector)
    def __init__(self,connector):
        self.connector = connector
        self.fine_scale_problem = VMSIterator(connector)
        self.full_scale_problem = ElementIterator(connector)
        self.BCset = BCSet()
        # Coarse-scale problem data:
        self.Ph = None # Projector on coarse-scales
        self.Fh = None # FE formulation on coarse-scales
        self.Fh = None # FE formulation RHS on coarse-scales
        self.__Se_spaces = []
        self.Ses = []
        self.Se_element_routines = []

    @property
    def Se_spaces(self):
        if len(self.connector.coarse_spaces) == 1:
            self.__Se_spaces = [0]*len(self.Ses)
        return self.__Se_spaces

    @Se_spaces.setter
    def Se_spaces(self,Se_spaces):
        self.__Se_spaces = Se_spaces

    @property
    def S_set(self):
        return [self.Ses,self.Se_element_routines,self.Se_spaces]
    
    def set_bilinear_form_full_scale(self,WFset,assemble=False):
        """
        Set the bilinear forms that must be assembled for the full-scale problem,
        as a `WeakFormSet` supplied with weak forms and assembly routines.
        The (adjoint) fine-scale problem is obtained as the transpose thereof.
        """
        self.full_scale_problem.Kes = WFset.get_weak_forms()
        self.full_scale_problem.element_routines = WFset.get_routines()
        if assemble:
            self.full_scale_problem.assemble_K()
    
    def set_linear_form_full_scale(self,WFset,assemble=False):
        """
        Set the linear form for assembling the RHS vector of the full-scale problem
        The WFset should be filled with getter functions that take the element number
        and the connector and return an Fe vector. A generic implementation would be:

        def getFe(el_nr, connector):
            element = connector.get_element(el_nr)
            element_loc = connector.get_element_location(el_nr)
            element_type = connector.get_element_type(el_nr)
            f = force(element_loc)
            L = lambda ul,vq,ds,dI,f=f: weak_form_body_force(ul,vq,ds,dI,f)
            return element.integrate_full_element(L,element_type)

        where `force` and `weak_form_body_force` would be external functions.
        """
        self.full_scale_problem.Fes = [ lambda e, c=self.connector: getFe(e,c) for getFe in WFset.get_weak_forms() ]
        self.full_scale_problem.assemble_functions_force = [assemble_vectors]*len(WFset)
        self.full_scale_problem.mappings_force = [self.connector.get_map_all_dofs]*len(WFset)
        if assemble:
            self.full_scale_problem.assemble_F()
                                        
    def set_bilinear_form_constraints(self,WFset,assemble=False):
        """
        Set the bilinear forms corresponding to the coarse-scale constraint equations,
        as a `WeakFormSet` supplied with weak forms, assembly routines, and coarse-scale
        space numbers (defaults to zero). The weak forms should be of the form:

        def H10_constraint(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return inner(grad(v),grad(coarse_basis))*dx
        """
        self.fine_scale_problem.Ces = WFset.get_weak_forms()
        self.fine_scale_problem.Ce_element_routines = WFset.get_routines()
        self.fine_scale_problem.Ce_spaces = WFset.get_spaces()
        if assemble:
            self.full_scale_problem.assemble_K()
        
    def set_linear_form_scale_interaction(self,WFset):
        """
        Set the linear forms corresponding to the missing scale-interaction term, 
        as a `WeakFormSet` supplied with weak forms, assembly routines (either 
        "full_element" or "per_internal_face_row"), and coarse-scale space numbers 
        (defaults to zero). The weak forms should be of the form:
        
        def S(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return inner(grad(v),a_vec*coarse_basis)*dx
        """
        self.fine_scale_problem.Ses = WFset.get_weak_forms()
        self.fine_scale_problem.Se_element_routines = WFset.get_routines()
        self.fine_scale_problem.Se_spaces = WFset.get_spaces()
    
    def set_bilinear_form_projector_coarse_scales(self,WFset):
        """
        Set the bilinear forms corresponding to the coarse-scale constraint equations,
        acting on the coarse-scale basis (i.e., with a simplified LMP representation).
        Can be either as a `WeakFormSet` supplied with weak forms, or directly as a weak
        form of the form:

        def H10_constraint_coarse(u,v):
            return inner(grad(u),grad(v))*dx
        """
        if type(WFset) is WeakFormSet:
            self.Ph = WFset.get_weak_forms()[0]
        else:
            self.Ph = WFset
    
    def set_bilinear_form_coarse_scale(self,WFset):
        """
        Set the bilinear forms corresponding to the coarse-scale problem, after most
        of the scale inversion. Would be a classical finite element method (or DG method).
        Should simply be of the form:

        def a(u,v):
            ...
        """
        if type(WFset) is WeakFormSet:
            self.Kh = WFset.get_weak_forms()[0]
        else:
            self.Kh = WFset
    
    def set_linear_form_coarse_scale(self,WFset):
        """
        Set the linear forms corresponding to the coarse-scale problem, after most of the
        scale inversion. Would be the RHS of a classical finite element method (or DG method).
        Should simply be of the form:
        
        def L(v):
            ...
        """
        if type(WFset) is WeakFormSet:
            self.Fh = WFset.get_weak_forms()[0]
        else:
            self.Fh = WFset
        
    def set_bilinear_form_full_scale_interaction(self,WFset):
        """
        Set the 'bilinear' forms corresponding to the full scale-interaction term, 
        as a `WeakFormSet` supplied with weak forms, assembly routines (either 
        "full_element" or "per_internal_face_row"), and coarse-scale space numbers 
        (defaults to zero). The weak forms should be of the form:
        
        def S(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return inner(grad(v),a_vec*coarse_basis)*dx + ...

        This information is used in the compute_coarse_scale_solution method, to add 
        the influence of the fine-scale closure function onto the FE formulation.
        """
        self.Ses = WFset.get_weak_forms()
        self.Se_element_routines = WFset.get_routines()
        self.Se_spaces = WFset.get_spaces()
        
    def set_boundary_condition(self,BCset):
        """
        Store the boundary conditions as a BCSet object.

        This boundary condition affects the:
        -  Full-scale problem: immediately as a true boundary condition.
        -  Fine-scale problem: if subspace is not the LMP space, then the
           boundary condition constrains the coarse-scale basis functions
           and thereby removes their constraint equations.
        -  Coarse-scale problem: if subspace is not the LMP space, then 
           the same BC is added as a DirichletCondition on the coarse-scale
           FE formulation.
        """
        self.BCset = BCset

    def build_all(self):
        """
        After all bilinear forms have been set, initiate all the assemblies and incorporate
        all boundary conditions.
        """
        self.build_linear_form_full_scale_problem(apply_boundary_conditions=False)
        self.build_bilinear_form_full_scale_problem()
        self.build_bilinear_form_fine_scale_problem()

    def build_bilinear_form_full_scale_problem(self, apply_boundary_conditions=True):
        """
        After all bilinear forms have been set, this function assembles all the matrices
        of the full-scale problem and takes care of the boundary conditions. This is mostly 
        an internal function, not intended to be called manually.
        """
        self.full_scale_problem.assemble_K(empty=True,assemble=False)
        if apply_boundary_conditions:
            for insidefunc,valuefunc,subspace in self.BCset.iterate_fine_scale_BCs():
                self.full_scale_problem.apply_boundary_condition(insidefunc,valuefunc,subspace)
        self.full_scale_problem.K.mat().assemble()

    def build_linear_form_full_scale_problem(self, apply_boundary_conditions=True):
        """
        After all linear forms have been set, this function assembles all the matrices
        of the full-scale problem and takes care of the boundary conditions. This is mostly 
        an internal function, not intended to be called manually.
        """
        self.full_scale_problem.assemble_F(empty=True)
        if apply_boundary_conditions:
            for insidefunc,valuefunc,subspace in self.BCset.iterate_fine_scale_BCs():
                self.full_scale_problem.apply_boundary_condition(insidefunc,valuefunc,subspace)

    def build_bilinear_form_fine_scale_problem(self,reassembleK=False):   
        """
        After all bilinear forms have been set, this function assembles all the matrices
        of the coarse-scale problem, and takes care of the boundary conditions. This is mostly 
        an internal function, not intended to be called manually.

        The body of the problem is copied over as the transpose stiffness matrix of the assembled 
        full-scale problem.
        """
        if reassembleK or not self.full_scale_problem.Kisinit:
            self.build_bilinear_form_full_scale_problem()
        self.fine_scale_problem.initialize_K()
        Kfull = self.full_scale_problem.K.copy()
        place_block_diagonal_matrix(PETScMatrix(Kfull.mat().transpose()),\
                                    self.fine_scale_problem.K, assemble=False)
        self.fine_scale_problem.assemble_K_coarse_scale_constraints(assemble=False)
        for insidefunc,CS_subspace in self.BCset.iterate_coarse_scale_BCs():
            self.fine_scale_problem.eliminate_coarse_scale_constraints( insidefunc,CS_subspace )
        self.fine_scale_problem.K.mat().assemble()

    def compute_full_scale_solution(self,reassembleK=False,reassembleF=False,**kwargs):
        """
        After all (bi)linear forms have been set, this function solves the full-scale problem 
        and returns the solution vector. Under the hood, it assembles all the matrices and 
        takes care of the boundary conditions.
        """
        if reassembleF or not self.full_scale_problem.Fisinit:
            # Apply bcs only if the bilinear form is not build afterwards:
            apply_bc = (reassembleK or not self.full_scale_problem.Kisinit) 
            self.build_linear_form_full_scale_problem(apply_boundary_conditions=apply_bc)
        if reassembleK or not self.full_scale_problem.Kisinit:
            self.build_bilinear_form_full_scale_problem()
        self.full_scale_problem.solve(**kwargs)
        return self.full_scale_problem.U

    def compute_fine_scale_Greens_function(self,loc,subspace=0,reassembleK=False,**kwargs):
        """
        After all (bi)linear forms have been set, this function solves for the fine-scale
        Green's function of `subspace` at location `loc`, and returns the solution vector. 
        Under the hood, it assembles all the matrices and takes care of the boundary conditions.
        """
        if reassembleK or not self.fine_scale_problem.Kisinit:
            self.build_bilinear_form_fine_scale_problem(reassembleK=reassembleK)
        glob_dof = self.connector.get_global_dof_from_loc(loc,subspace=subspace)
        self.fine_scale_problem.initialize_F()
        self.fine_scale_problem.F.vec().setValue(glob_dof,1)
        self.fine_scale_problem.F.vec().assemble()
        self.fine_scale_problem.solve(**kwargs)
        return self.fine_scale_problem.U
    
    def compute_fine_scale_closure_function(self,loc=None,dof=None,reassembleK=False,**kwargs):
        """
        After all (bi)linear forms have been set, this function solves for the fine-scale closure
        function of `dof` or the dof nearesr location `loc`, and returns the solution vector. 
        Under the hood, it assembles all the matrices and takes care of the boundary conditions.
        """
        if reassembleK or not self.fine_scale_problem.Kisinit:
            self.build_bilinear_form_fine_scale_problem(reassembleK=reassembleK)
        if dof is None:
            subspace = self.fine_scale_problem.Se_spaces[0]
            dof = self.connector.get_coarse_dof_from_loc( loc, subspace=subspace )
        self.fine_scale_problem.assemble_F_scale_interaction( dof )
        self.fine_scale_problem.solve(**kwargs)
        return self.fine_scale_problem.U

    def compute_projection(self, U=None, recomputeU=False,reassembleK=False,reassembleF=False, solver="mumps"):
        """
        After all (bi)linear forms have been set, this function computes the projection of U
        (or the previously computed full-scale solution, or a re-computed full-scale solution),
        and returns the solution as a FEniCS Function.
        """
        # Obtain the data vector
        if type(U) is type(None):
            if type(self.full_scale_problem.U) is type(None) or recomputeU:
                print("Computing the full solution first")
                U = self.compute_full_scale_solution(reassembleK=reassembleK,reassembleF=reassembleF,solver=solver)
            else:
                U = self.full_scale_problem.U

        # Matrix/vector sizes
        CS_size = self.connector.coarse_space.dim()
        FS_size = self.full_scale_problem.full_size
        
        # Assemble the LHS matrix
        u = TrialFunction(self.connector.coarse_space)
        v = TestFunction(self.connector.coarse_space)
        a_p = self.Ph(u,v)
        M_p = as_backend_type( assemble(a_p) )

        # Build the RHS vector
        V_p = Function( self.connector.coarse_space ).vector()
        for v_dof in range(CS_size):
            projector_row = FS_size + v_dof
            row_nz,row_vals = self.fine_scale_problem.K.mat().getRow(projector_row)
            if projector_row in row_nz:
                # This coarse-scale constrained was removed 
                continue
            V_p[v_dof] = float( np.asmatrix(row_vals)*np.asmatrix(U[row_nz]).T ) 
        V_p = self.connector.to_mixed_coarse_scale_space( V_p )

        # Apply BCs
        for bc in self.BCset.iterate_FEniCS():
            bc.apply(M_p,V_p)
        
        # Return problem and store as FEniCS Function
        sol_p = Function( self.connector.coarse_space )
        solve( M_p, sol_p.vector(), V_p , solver)
        return sol_p

    def compute_coarse_scale_solution(self, include_scale_interaction=True,solver="mumps"):
        """
        After all (bi)linear forms have been set, this function computes the coarse-scale solution.
        If `include_scale_interaction` is set, the computation makes use of  the fine-scale closure 
        function approach to eliminating the remaining scale interaction. If not, it will simply 
        yield the "naive" finite element solution. It returns the solution as a FEniCS Function.
        """
        # Matrix/vector sizes
        CS_size = self.connector.coarse_space.dim()
        FS_size = self.full_scale_problem.full_size
        
        # Assemble the coarse-scale/coarse-scale system
        u = TrialFunction(self.connector.coarse_space)
        v = TestFunction(self.connector.coarse_space)
        a_h = self.Kh(u,v)
        f_h = self.Fh(v)
        K_h = as_backend_type( assemble(a_h) )
        F_h = as_backend_type( assemble(f_h) )

        # Add the scale interaction
        if include_scale_interaction:
            K_s = np.zeros( (CS_size,CS_size) )
            F_s = np.zeros( CS_size )
            B_on_u_set = []

            # Precompute the B(u^h, . ) vectors for all u^h basis functions
            for u_dof in range(CS_size):
                self.fine_scale_problem.assemble_F_scale_interaction(u_dof, S_set=self.S_set)
                B_on_u = self.fine_scale_problem.F.copy()
                B_on_u_set.append( np.mat( B_on_u[:FS_size] ) )

            # Compute the B(u^h, g ) and L( g ) additions
            for v_dof in range(CS_size):
                # Compute fine-scale closure function
                Ug = self.compute_fine_scale_closure_function(dof=v_dof,solver=solver)
                Ug = np.mat( Ug[:FS_size] )

                F_s[v_dof] = float( Ug * np.mat( self.full_scale_problem.F ).T )

                for u_dof in range(CS_size):
                    K_s[v_dof,u_dof] = float( Ug*B_on_u_set[u_dof].T )

            # Reshuffle K_s and F_s to fit the mixed formulation dof pattern
            K_s_ = np.zeros( (CS_size,CS_size) )
            for col in range(CS_size):
                K_s_[:,col] = self.connector.to_mixed_coarse_scale_space( K_s[:,col].copy() )
            for row in range(CS_size):
                K_s[row,:] = self.connector.to_mixed_coarse_scale_space( K_s_[row,:] )
            F_s = self.connector.to_mixed_coarse_scale_space(F_s) 

            # Add to FE matrix
            K_h.mat().setOption(PETSc.Mat.Option.NEW_NONZERO_ALLOCATION_ERR, False)
            K_h.mat().setValues(range(CS_size),range(CS_size), K_s, addv=PETSc.InsertMode.ADD)
            K_h.mat().assemble()
            F_h[:] += F_s[:]

        # Apply boundary conditions
        for bc in self.BCset.iterate_FEniCS():
            bc.apply(K_h,F_h)
        
        # Return problem and store as FEniCS Function
        sol_p = Function( self.connector.coarse_space )
        solve( K_h, sol_p.vector(), F_h , solver)
        return sol_p
