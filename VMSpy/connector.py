import os, sys
from dolfin import *
import numpy as np
from functools import lru_cache

from ufl.finiteelement import elementlist
from .dofnumbering import *
from .element import *
from .functionspaces import *
from .verboseprint import *


def CONFIRM_IMPORT_CONNECTOR():
    return "confirmed"

class Connector:
    """
    Together with the iterator and the connector, this class represents one of the three system-level
    interfaces of the framework (this is probably the intermediate level).

    It offers fine-scale access on the coarse-scale mesh. It has iterate_element functionality,
    and understands the fines-scale connectivity between the elements through
    - get_map_all_dofs
    - get_map_internal_boundary_dofs
    - get_map_opposing_boundary_dofs
    - get_map_opposing_face_dofs

    Further functionality include interaction with the solution vector as getting pointvalues,
    getting the per-element solution vector and exporting the solution vector,
    and getting coarse-scale and fine-scale dofs from locations or from indicator functions.
    
    To create a new type of connector, the following methods need to be overloaden:

    - get_element: the element object corresponding to the element_nr
    - get_element_type: element_nr -> element_type (corresponding to the element class)
    - get_face_opposite: get the (opposite_element_nr,opposite_face_nr) for the given (element_nr,face_nr)
    - get_element_number_from_location: determine the element_nr for the given point
    - get_relative_location: determine the location of the point relative to element_nr
    """
    def __init__(self):
        self.elements = [None]
        self.total_elements = 0
        self.dimension = 0
        self.coarse_spaces = [] # Collection of FEniCS spaces: for dof-numbering
        self.coarse_space = None # The FEniCS mixed space of the above set: for weak forms

    @property
    def W(self):
        return self.get_element(0).W
    
    @property
    def bdy_spaces(self):
        return self.get_element(0).bdy_spaces
    
    @property
    def W_size(self):
        return self.get_element(0).W_size

    def iterate_elements(self):
        """
        Usable as:
        for (element_nr,element,element_type) in iterate_elements
        """
        for element_nr in range(self.total_elements):
            element = self.get_element(element_nr)
            element_type = self.get_element_type(element_nr)
            yield (element_nr,element,element_type)
        
    def get_face_opposite(self,element_nr,face_nr):
        """
        Return element_nr and face_nr of the element opposite to face_nr of the given element_nr. 
        Returns (-1,-1) if face is a domain boundary face.
        """
        return (-1,-1)
    
    def get_element(self,element_nr):
        """
        Defaults to the only element type there is. Needs to be overloaded if the mesh
        consists of different element objects (e.g., differently stanted trianlges).
        """ 
        return self.elements[0]

    def get_element_type(self, element_nr):
        """
        Returns a type code (internal, boundary, corner, etc) for the given element_nr.
        This code lets with the Element object know how to disect the boundary into
        domain and interface faces.
        """
        return 0

    def create_local_spaces(self,element_types,bdy_spaces=[],refinement=4):
        """
        Forwards the call to all element objects. That, in turn, sets their instance
        variable W to the fine-scale space, and then calls their set_space(W,bdy_spaces).
        """
        els = [ el for (el,_) in element_types ]
        Ps = [ P for (_,P) in element_types ]
        for element in self.elements:
            element.create_local_spaces(els,Ps,bdy_spaces,refinement)

    def create_coarse_spaces(self,element_types):
        """
        Forwards the call to all element objects. That, in turn, sets their instance
        variable Bs and Bs_dof_locs. Additionally, the connector stores a coarse-scale mesh
        for each of the element types for later dofnumber of the coarse-scale constraints,
        and a mixed coarse-scale function space for later weak formulations, and a mapping
        between these two representations.
        """
        els = [ el for (el,_) in element_types ]
        Ps = [ P for (_,P) in element_types ]

        # Forwarding call to elements
        for element in self.elements:
            element.generate_coarse_scale_basis_functions(els,Ps)

        # Producing individual CS spaces
        self.coarse_spaces = []
        for el,P in zip(els,Ps):
            self.coarse_spaces.append(FunctionSpace(self.mesh_coarse,el,P))

        # Producing mixed CS space
        self.coarse_space = get_fine_space(els,Ps,self.mesh_coarse)

    def to_mixed_coarse_scale_space(self,Uh):
        """
        Takes a solution vector `Uh` that is the concatenation of solution vectors of
        spaces in self.coarse_spaces[0],... and returns the solution vector that
        corresponds to the mixed space self.coarse_space.
        """
        if self.coarse_space.num_sub_spaces() == 0:
            uh = Uh[:]
            if 'Discontinuous' in self.coarse_space.element().signature():
                uh = self._reshuffle_to_DG(uh)
            v = Function(self.coarse_space)
            v.vector()[:] = uh
            return v.vector()
        sol = Function(self.coarse_space)
        offset = 0
        for i,V in enumerate(self.coarse_spaces):
            v = Function(V)
            uh = Uh[offset:offset+V.dim()]
            if 'Discontinuous' in V.element().signature():
                uh = self._reshuffle_to_DG(uh,i)
            v.vector()[:] = uh
            assign(sol.sub(i), v)
            offset += V.dim()
        return sol.vector()
    
    def _reshuffle_to_DG(self,v,i=0):
        """
        Takes a numpy array of a DG solution per the coarse-scale numbering (per element)
        and produces a numpy array as a solution vecor of the coarse-scale space i (which 
        is assumed to be a DG space).
        """
        # Output vector
        v_out = np.zeros( v.shape )

        # Loop over all FEniCS elements of the space
        for c in cells(self.coarse_spaces[i].mesh()):
            # Get the vertex info for this FEniCS element
            c_dofs = self.coarse_spaces[i].dofmap().cell_dofs(c.index())
            c_locs = self.coarse_spaces[i].tabulate_dof_coordinates()[c_dofs]

            # Own element corresponding to this cell
            el_nr = self.get_element_number_from_location(c.midpoint())
            el = self.get_element(el_nr)
            el_loc = self.get_element_location(el_nr)

            # Own element vertex info
            el_B_locs = np.asarray(  [ (loc + el_loc).array()[:self.dimension] for loc in el.Bs_dof_locs[i]]  )
            el_dofs = [*range(el_nr*len(el_B_locs),(el_nr+1)*len(el_B_locs))] 

            # Find the overlapping dofs for all dofs in the cell
            for c_loc,c_dof in zip(c_locs,c_dofs):
                ind = ( abs( el_B_locs-c_loc )<1E-5 ).all(axis=1).nonzero()[0][0]
                el_dof = el_dofs[ind]
                v_out[c_dof] = v[el_dof]
        return v_out

    def from_mixed_coarse_scale_space(self,Uh):
        """
        Takes a solution vector `Uh` that corresponds to the mixed space self.coarse_space
        and returns the solution vector that is the concatenation of solution vectors
        of spaces in self.coarse_spaces[0],... .
        """
        # Not fit for DG spaces
        if self.coarse_space.num_sub_spaces() == 0:
            return Uh
        v = Function(self.coarse_space)
        v.vector()[:] = Uh
        sols = v.split()
        solvecs = []
        for i,V in enumerate(self.coarse_spaces):
            v = Function(V)
            assign(v, sols[i])
            solvecs.append(v.vector()[:])
        vec_concat = np.hstack( solvecs )
        sol = Function(self.coarse_space)
        sol.vector()[:] = vec_concat[:]
        return sol.vector()
    
    def get_map_coarse_dof(self,element_nr,CS_local_dof_nr,CS_space_nr=0):
        """
        Returns the row in the VMS iterator's stiffness matrix corresponding to
        the `CS_local_dof_nr` of the `element_nr` element of subspace `CS_space_nr`.
        For DG spaces this is based on simple counting, for other spaces this is
        based on the FEniCS structure of the corresponding coarse-scale mesh.
        """
        base_offset = self.total_elements*self.W_size
        for cs in range(CS_space_nr):
            base_offset += self.coarse_spaces[cs].dim()
        coarse_space = self.coarse_spaces[CS_space_nr]
        if 'Discontinuous' in coarse_space.element().signature():
            # Simplified counting for DG, to avoid different basis functions with the same coordinate
            cs_global_dof_nr = int(coarse_space.dim()/self.total_elements*element_nr + CS_local_dof_nr)
        else:
            # Based on the location of the coarse-scale basis function, and the dof of that
            # location in the stored coarse-scale mesh
            dofloc_local = self.get_element(element_nr).Bs_dof_locs[CS_space_nr][CS_local_dof_nr]
            elementloc = self.get_element_location(element_nr)
            dofloc = (elementloc + dofloc_local).array()[:self.dimension]
            doflocs = coarse_space.tabulate_dof_coordinates()
            cs_global_dof_nr = ( abs(doflocs-dofloc)<1E-5 ).all(axis=1).nonzero()[0][0]
        return base_offset+cs_global_dof_nr
                
    def get_map_all_dofs(self,element_nr):
        """
        Returns the integer offset along the diagonal to where the internal stiffness matrix
        corresponding to element_nr is placed.
        """
        row_offset = element_nr*self.W_size
        return row_offset

    def get_map_opposing_face_dofs(self,element_nr,face_nr,space_nr=-1):
        """
        Map of the local space_nr dofs on the face_nr of the element_nr to the rows/cols of their 
        neighbours in the global matrix. Usable with assembly.place_rows( Ke, K, *map ) or place_columns.
        Returns: internal_local_dofs, external_global_dofs, row_offset
        """
        element = self.get_element(element_nr)
        row_offset = element_nr*self.W_size
        opp_element_nr,opp_face_nr = self.get_face_opposite(element_nr,face_nr)
        if opp_element_nr == -1:
            return np.array([],dtype=np.int32),np.array([],dtype=np.int32),0
        opp_element = self.get_element(opp_element_nr)
        internal_local_dofs = element.get_internal_face_local_dofs(face_nr,space_nr)
        external_global_dofs = opp_element.get_internal_face_local_dofs(opp_face_nr,space_nr) + opp_element_nr*self.W_size
        return internal_local_dofs, external_global_dofs, row_offset

    def reconstruct_function_in_element(self,U,element_nr):
        """
        Map of the local bdy_space dofs of the element_nr-th element to the rows/cols of their neighbours
        in the global matrix. Usable with assembly.place_rows( Ke, K, *map ) or place_columns.
        Returns: internal_local_dofs, external_global_dofs, row_offset
        """
        element = self.get_element(element_nr)
        compression_map = get_compression_map(element.W,element.bdy_spaces)
        u = Function(element.W)
        u.vector()[compression_map] = U[element_nr*self.W_size : (element_nr+1)*self.W_size ] 
        return u

    def get_coarse_dof_from_loc(self,loc,subspace=0):
        """
        Returns the row in the VMS iterator's stiffness matrix corresponding to
        the coarse-scale dof of `subspace`, closest to `loc`.
        """
        offset = self.total_elements*self.W_size
        for Wcoarse in self.coarse_spaces[:subspace]:
            offset += Wcoarse.dim()
        element_nr = self.get_element_number_from_location(Point(loc))
        element = self.get_element(element_nr)
        Bs_locs = element.Bs_dof_locs[subspace]
        rel_loc = self.get_relative_location(element_nr,Point(loc))
        disps = [Bs_loc-rel_loc for Bs_loc in Bs_locs]
        dists2 = [ sum(disp.array()**2) for disp in disps ]
        Bi = np.argmin(dists2)
        return self.get_map_coarse_dof(element_nr,Bi,CS_space_nr=subspace)-offset

    def get_global_dof_from_loc(self,loc,subspace=0):
        """
        Returns the row in the VMS iterator's stiffness matrix corresponding to
        the fine-scale dof of `subspace`, closest to `loc`.
        """
        element_nr = self.get_element_number_from_location(Point(loc))
        element = self.get_element(element_nr)
        W = element.W
        rel_loc = self.get_relative_location(element_nr,Point(loc)).array()[:self.dimension]
        if W.num_sub_spaces() > 0:
            # Rather convoluted approach, necessary for subspaces when "tabulate_dof_coordinates" is
            # not implemented for other subspaces (e.g., BDM2/DG1)
            doftracker = Function(W)
            doftracker.vector()[:] = np.arange( len(doftracker.vector()) )
            V = W.sub(subspace).collapse()
            doftracker2 = Function(V)
            doftracker2.interpolate( doftracker.split()[subspace] )
            doflocs = V.tabulate_dof_coordinates()
            dist = np.array( [ sum((doflocs[i,:]-rel_loc)**2) for i in range(len(doflocs))] )
            dof = dist.argmin()
            W_dof_nr = int( doftracker2.vector()[dof] )
            comp_map =  get_compression_map(W, bdy_spaces=self.bdy_spaces)
            dof_nr = int( np.where( comp_map == W_dof_nr )[0] )
        else:
            doflocs = W.tabulate_dof_coordinates()
            dof_nrs = W.dofmap().dofs()
            dist = np.array( [ sum((doflocs[i,:]-rel_loc)**2) for i in range(len(doflocs))] )
            dof = dist.argmin()
            dof_nr = dof_nrs[dof]
        offset = element_nr*self.W_size
        return offset+dof_nr

    def get_global_dofs_locs_from_indicator(self,insidefunc,subspace=0,on_boundary=True):
        """
        Returns an array of row in the VMS iterator's stiffness matrix for which the
        dofs are from `subspace` and satisfy the `insidefunc` criterion. insidefunc must
        be able to act on arrays to produce a True/False mask. If on_boundary set to True,
        if will only flag dofs in boundary elements (! but not necessarily only dofs on
        the boundary !).
        """
        dofs = []
        locs = []
        for element_nr,element,element_type in self.iterate_elements():
            if on_boundary and element_type==0:
                # Speed-up, plus avoids corner nodes of non-boundary 
                # elements from being flagged. Not fool-proof.
                continue
            dof_offset = element_nr*self.W_size
            element_offset = self.get_element_location(element_nr).array()[:self.dimension]            
            local_dofs, local_locs  = get_local_dofs_and_locs(element.W,subspace,self.bdy_spaces)
            global_dofs,global_locs = local_dofs+dof_offset,local_locs+element_offset
            mask = insidefunc( global_locs )
            dofs.append( global_dofs[mask] ); locs.append( global_locs[mask] )
        dofs = np.hstack(dofs); locs = np.vstack(locs)
        return dofs,locs
                                  
    def get_pointvalue(self,U,loc,subspace=0):
        """
        Gets the value of the solution correponding to `subspace` of the solutionvector U
        at the location `loc`.
        """
        element_nr = self.get_element_number_from_location(loc)
        rel_loc = self.get_relative_location(element_nr,loc)
        sol = self.reconstruct_function_in_element(U,element_nr)
        if self.W.num_sub_spaces() > 0:
            sol = sol.split()[subspace]
        return sol(rel_loc)

    def get_element_location(self,element_nr):
        """
        Get the location of the local origin of the local refence frame of element_nr.
        """
        zero = Point([0]*self.dimension)
        return zero - self.get_relative_location(element_nr,zero)

    def exportSolution(self,U, name, subspace=-1, fieldname="u"):
        """
        Export the solution vector U as a pvd file by partitioning into the FEniCS
        Functions on each element, exporting those, and linking them together.

        subspace: -1 means export all
        fieldname is only appended when subspace > -1
        """
        try:
            os.mkdir(name)
        except:
            vprint("Did not create directory")
        subname = "_%s"%fieldname if subspace > -1 else ""
        with open(name+subname+".pvd",'w') as vtkfile:
            vtkfile.write("""<?xml version="1.0"?>
<VTKFile type="Collection" version="0.1">
  <Collection>
""")
            for element_nr in range(self.total_elements):
                sol = self.reconstruct_function_in_element(U,element_nr)
                if subspace > -1:
                    sol = sol.split()[subspace]
                sol.rename(fieldname,fieldname)
                el_loc = self.get_element_location(element_nr)
                W = sol.function_space()
                mesh = W.mesh()
                mesh.translate(el_loc)
                File(name+"/"+fieldname+str(element_nr)+".pvd") << sol
                mesh.translate( Point([0]*self.dimension) - el_loc)
                os.remove(name+"/"+fieldname+str(element_nr)+".pvd")
                vtkfile.write("""    <DataSet timestep="0" part="0" file="%s/%s%i000000.vtu" />
"""%(name.split('/')[-1],fieldname,element_nr))
            vtkfile.write("""  </Collection>
</VTKFile>""")

    def exportCoarseScaleSolution(self, Uh, name, subspace=-1,fieldname="u", representation="mixed"):
        """
        Export the solution vector in the coarse-scale space as a pvd file. The 
        coarse-scale solution vector is assumed to correspond to self.coarse_space.
        If, instead, it is a concatenation of vectors from self.coarse_spaces[0],...,
        then `representation` must be changed to 'set'.

        subspace: -1 means export all
        fieldname is only appended when subspace > -1
        representation: "mixed" if Uh relates to self.coarse_space
                        "set" if Uh relates to self.coarse_spaces[0],...
        """
        if representation=="set":
            Uh = self.to_mixed_coarse_scale_space(Uh)
        subname = "_%s"%fieldname if subspace > -1 else ""
        sol = Function(self.coarse_space)
        sol.vector()[:] = Uh
        if subspace > -1:
            sol = sol.split()[subspace]
        File(name+subname+".pvd") << sol
            

class Connector_1D(Connector):
    """
    Connector for a uniform 1D grid of elements.
    """
    def __init__(self,width,total_elements):
        Connector.__init__(self)
        self.dimension = 1
        self.width = width
        self.total_elements = total_elements
        self.mesh_coarse = IntervalMesh(total_elements, 0, width)
        self.elements = [Element_1D(width/total_elements)]
    
    def get_element_type(self, element_nr):
        """
        Returns a type code: 0=internal, 1=left, 2=right, 3=both, for the given element_nr.
        This code lets with the Element object know how to disect the boundary into
        domain and interface faces.
        """
        if self.total_elements == 1: return 3 # Left and right boundary
        if element_nr == 0: return 1 # Left boundary
        if element_nr == self.total_elements-1: return 2 # Left and right boundary
        return 0 # Internal element

    def get_face_opposite(self,element_nr,face_nr):
        """
        Return element_nr and face_nr of the element opposite to face_nr of the given element_nr. 
        Returns (-1,-1) if face is a domain boundary face.
        """
        if (element_nr == 0 and face_nr == 0) or \
           (element_nr == self.total_elements-1 and face_nr == 1):
            return (-1,-1)
        return element_nr-1+2*face_nr , 1-face_nr
    
    def get_element_number_from_location(self,loc):
        """
        Returns the `element_nr` corresponding to the element that houses `loc`.
        """
        x = loc.array()[0]
        el_width = self.width/self.total_elements
        return min( int(x/el_width), self.total_elements-1)
    
    def get_relative_location(self,element_nr,loc):
        """
        Returns the location of `loc` in the local coordinate frame of `element_nr`.
        """
        x = loc.array()[0]
        x_el = element_nr*self.width/self.total_elements
        return Point( x-x_el )

    def get_1d_solution(self,U,subspace=0,refinement=16):
        """
        Returns X,Y arrays with solutions of `subspace` of the solutionvector U.
        `refinement` sets the number of points in each element.
        """
        X = []
        Y = []
        h = self.width/self.total_elements
        for element_nr in range(self.total_elements):
            el_offset = self.get_element_location(element_nr).array()[0]
            sol = self.reconstruct_function_in_element(U,element_nr)
            if self.W.num_sub_spaces() > 0:
                sol = sol.split()[subspace]
            xs = np.linspace(0,h,refinement)
            ys = [sol(x) for x in xs]
            X.append([None]);X.append(xs+el_offset)
            Y.append([None]);Y.append(ys)
        return np.hstack(X),np.hstack(Y)
        
class Connector_2D_triangular(Connector):
    """
    Connector for a uniform 2D grid of /-slanted triangles.
    """
    def __init__(self,width,triangles_x):
        Connector.__init__(self)
        self.dimension = 2
        self.width = width
        self.triangles_x = triangles_x
        self.total_elements = 2*triangles_x**2
        h = width/triangles_x
        self.mesh_coarse = RectangleMesh(Point(0,0), Point(width,width), triangles_x,triangles_x)
        self.elements = [ Element_2D_triangle(h, "lower") , Element_2D_triangle(h, "upper") ]
        
    def get_element(self,element_nr):
        """
        Connector_2D_triangular consists of two different element objects for the two
        slanted triangles. This function returns the object for element_nr.
        """ 
        lu = (element_nr // self.triangles_x) % 2
        return self.elements[lu]

    def get_element_type(self, element_nr):
        """
        Returns a type code 0=internal, 1=vertical, 2=horizontal, 3=corner, for the given element_nr.
        This code lets with the Element object know how to disect the boundary into
        domain and interface faces.
        """
        lu = (element_nr // self.triangles_x) % 2 # 0: lower, 1: upper
        if element_nr < self.triangles_x:
            # Bottom row, lower element
            if element_nr+1 == self.triangles_x: return 3 # corner
            return 2
        if element_nr >= self.total_elements-self.triangles_x:
            # top row, upper element
            if element_nr == self.total_elements-self.triangles_x: return 3 # corner
            return 2
        if element_nr / self.triangles_x == element_nr // self.triangles_x and lu:
            # left column, upper element
            return 1
        if (element_nr+1)/self.triangles_x == (element_nr+1)//self.triangles_x and not lu:
            # right column, lower element
            return 1
        return 0
    
    def get_face_opposite(self,element_nr,face_nr):
        """
        Return element_nr and face_nr of the element opposite to face_nr of the given element_nr. 
        Returns (-1,-1) if face is a domain boundary face.
        """
        eltype = self.get_element_type(element_nr)
        if (face_nr == 0 and (eltype == 1 or eltype==3)) or \
           (face_nr == 1 and (eltype == 2 or eltype==3)):
            return (-1,-1)
        lu = (element_nr // self.triangles_x) % 2 # 0: lower, 1: upper
        if lu == 0:
            # lower elements:
            if face_nr == 2:
                # diagonal edge
                opposite_element_nr = element_nr + self.triangles_x
            if face_nr == 0:
                # right edge
                opposite_element_nr = element_nr + self.triangles_x+1
            if face_nr == 1:
                # lower edge
                opposite_element_nr = element_nr - self.triangles_x
        else:
            # upper elements
            if face_nr == 2:
                # diagonal edge
                opposite_element_nr = element_nr - self.triangles_x
            if face_nr == 0:
                # left edge
                opposite_element_nr = element_nr - self.triangles_x-1
            if face_nr == 1:
                # upper edge
                opposite_element_nr = element_nr + self.triangles_x
        return (opposite_element_nr,face_nr)

    def get_element_number_from_location(self,loc):
        """
        Returns the `element_nr` corresponding to the element that houses `loc`.
        """
        x,y,_ = loc.array()
        el_width = self.width/self.triangles_x
        row = min( int(y/el_width), self.triangles_x-1)
        col = min( int(x/el_width), self.triangles_x-1)
        rel_x,rel_y = x-col*el_width, y-row*el_width
        ul = 0 if rel_x > rel_y else 1 # 0 lower, 1 upper
        return row*self.triangles_x*2 + ul*self.triangles_x + col
    
    def get_relative_location(self,element_nr,loc):
        """
        Returns the location of `loc` in the local coordinate frame of `element_nr`.
        """
        x,y,_ = loc.array()
        el_width = self.width/self.triangles_x
        row = int( element_nr/(2*self.triangles_x) )
        col = (element_nr - row*2*self.triangles_x) % self.triangles_x
        x_el,y_el = col*el_width,row*el_width
        return Point(  x-x_el, y-y_el  )
    
