import os, sys
from dolfin import *
import numpy as np

def CONFIRM_IMPORT_NODENUMBERING():
    return "confirmed"

def get_full_size(elements, V, bdy_spaces = [], Bs = []):
    """
    Returns the size of the complete stiffness matrix, including 'elements' number
    of 'V' fine-scale spaces (without interior nodes for spaces in bdy_spaces),
    and constraint equations for each B in Bs.
    """
    element_size = get_element_size(V, bdy_spaces, Bs)
    return elements*element_size

def get_element_size(V, bdy_spaces=[], Bs=[]):
    """
    Returns the total size of all dofs in the 'V' fine-scale spaces (without interior
    nodes for spaces in bdy_spaces) plus the number of B's in Bs (list of lists of FEniCS
    coarse-scale basis Functions.
    """
    comp_map = get_compression_map(V,bdy_spaces)
    size_fine = len(comp_map)
    size_coarse = sum([len(B) for B in Bs])
    return size_fine+size_coarse
    
def get_compression_map(V,bdy_spaces=[],subspaces=None):
    """
    Returns a list of indexes corresponding to dofs in (the 'subspaces' in) V. Excluding
    the interior dofs for all spaces in bdy_spaces.
    Useable as A_c[i,j] = A_f[map(i),map(j)] for i and j until len(map).
    """
    comp_map_bdy = get_bdy_compression_map(V,bdy_spaces)
    if subspaces == None:
        return comp_map_bdy
    
    if type(subspaces) == int:
        subspaces = [subspaces]
    subspace_dofs = []
    for subspace in subspaces:
        subspace_dofs.append( V.sub(subspace).dofmap().dofs() )
    subspace_dofs = np.hstack( subspace_dofs  )
    mask = np.isin(subspace_dofs, comp_map_bdy)
    return subspace_dofs[mask]

def get_bdy_compression_map(V,bdy_spaces=[]):
    """
    Return a dof map that takes all the volumetric dofs out of the boundaryspaces
    Useable as A_c[i,j] = A_f[map(i),map(j)] for i and j until len(map).
    """
    if type(bdy_spaces) == int:
        bdy_spaces = [bdy_spaces]

    if V.num_sub_spaces()==0:
        if bdy_spaces != []:
            return extract_boundary_dofs(V)
        else:
            return np.array( V.dofmap().dofs() ,dtype=np.int32)
    
    dofmaps = []
    for s in range(V.num_sub_spaces()):
        if s in bdy_spaces:
            dofs_s = extract_boundary_dofs(V,sub=s)
        else:
            dofs_s = np.array( V.sub(s).dofmap().dofs() ,dtype=np.int32)
        dofmaps.append( dofs_s )
    fulldofmap = np.hstack( dofmaps )
    fulldofmap = np.sort( fulldofmap )
    return fulldofmap

def extract_boundary_dofs(V,sub=-1,bdy_indicator = lambda x,b: b):
    """
    Return a list of dof indices of V for which subspace 'sub' have nodes
    indicated by 'bdy_indicator'.
    Useable as A_c[i,j] = A_f[map(i),map(j)] for i and j until len(map).
    """
    mesh = V.mesh()
    Vsub = V
    if sub != -1:
        Vsub = V.sub(sub)
    try:
        # Scalars
        bc = DirichletBC(Vsub, Constant(1.0), bdy_indicator)
        u = Function(V)
        bc.apply(u.vector())
        doflist = np.where(u.vector()[:]!=0.0)[0]
    except:
        # Vectors
        bc1 = DirichletBC(Vsub, Constant((0,1)), bdy_indicator) #Needs to split for BDM
        bc2 = DirichletBC(Vsub, Constant((1,0)), bdy_indicator)
        u = Function(V)
        v = Function(V)
        bc1.apply(u.vector())
        bc2.apply(v.vector())
        doflist = np.where(u.vector()[:]**2+v.vector()[:]**2 != 0.0)[0]
    return doflist

def get_subspace_local_dofs(V,subspace,bdy_spaces=[]):
    """
    Returns the array of indices of 'subspace' local to the compressed matrix.
    Compressed in the sense of having removed the interior nodes of the bdy_spaces from V.
    """
    all_dofs = get_bdy_compression_map(V,bdy_spaces)
    subspace_dofs = np.array( V.sub(subspace).dofmap().dofs() )
    mask = np.isin(all_dofs,subspace_dofs)
    return np.array(  np.where( mask == True )[0]  ,dtype=np.int32)

def get_local_dofs_and_locs(W,subspace=0,bdy_spaces=None):
    """
    Returns a tuple of the array of indices of 'subspace' local to the compressed matrix,
    and an array of the coordinates of these dofs.
    Compressed in the sense of having removed the interior nodes of the bdy_spaces from V.
    """
    if W.num_sub_spaces() > 1:
        comp_map = get_compression_map(W,bdy_spaces,subspace)
        dof_locs = W.tabulate_dof_coordinates()[comp_map]
        local_dofs = get_subspace_local_dofs(W,subspace,bdy_spaces)
    else:
        dof_locs = W.tabulate_dof_coordinates()
        local_dofs = W.dofmap().dofs()
    return local_dofs,dof_locs

