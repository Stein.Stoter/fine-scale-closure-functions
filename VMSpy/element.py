import os, sys
from dolfin import *
import numpy as np
from functools import lru_cache
from .functionspaces import get_fine_space_triangle,get_fine_space_one_dimensional
from .functionspaces import get_coarse_space_triangle,get_coarse_space_one_dimensional,get_coarse_space_dof_locs
from .integrations import integrate_fine_scale_term
from .dofnumbering import *
from .verboseprint import *

def CONFIRM_IMPORT_ELEMENT():
    return "confirmed"

class Element:
    """
    Together with the iterator and the connector, this class represents one of the three system-level
    interfaces of the framework (although probably the lowest-level one of the three).

    Elements hold the coarse and fine-scale function spaces. They have functionality for integrating
    weak forms based on those spaces on either the full element (inc. domain boundary faces and
    element interface faces) or on particular faces. They know their own boundaries as ds measures on
    the fine-scale mesh, and the dofs of bdy_spaces on those boundaries.

    To create a new type of element, the following methods need to be overloaden:
    - __init__: must set the total_faces of the element.
    - create_local_spaces: this set the fine-scale space W and calls Element.set_space.
    - generate_coarse_scale_basis_functions: this set the list of lists of coarse-scale basis
        functions Bs and their locations Bs_dof_locs.
    - disect_boundaries: for a given "element_type" passed by the connector this returns two tuples
        with face numbers. One for those on the domain boundary and one for the internal faces.
    """
    def __init__(self):
        self.total_faces = 0
        self.face_definitions = [] # Indicator functions
        self.ds_i = None
        self.W = None
        self.W_size = 0
        self.bdy_spaces = []
        self.Bs = [] # Coarse-scale basis functions on W
        self.Bs_dof_locs = [] # Local location of Bs dofs. Required to find an appropriate dof number.
        
    def set_space(self,W,bdy_spaces=[]):
        """
        Sets the instance variables W, W_size and bdy_spaces for the given fine-scale space W
        and the given bdy_spaces. Calls set_ds afterwards to update the ds_i measure.
        """
        self.W = W
        self.W_size = get_element_size(W,bdy_spaces)
        self.bdy_spaces = bdy_spaces
        self.set_ds()
        
    def set_ds(self):
        """
        Sets the instance variable ds_i to the FEniCS ds-measure for the self.W function space.
        Every ds_i[i] corresponds to a face of the element, per the defined face numbering.
        """
        mesh = self.W.mesh()
        faces = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
        faces.set_all(self.total_faces+1)
        for f,face_def in enumerate(self.face_definitions):
            class Face(SubDomain):
                def inside(self, x, b):
                    return face_def(x,b)
            Face().mark(faces, f)
        self.ds_i = Measure('ds', domain=mesh, subdomain_data=faces)
        
    @lru_cache(maxsize=None)
    def get_internal_face_local_dofs(self,face_nr,space_nr=-1):
        """
        Returns dofnumbers of the space_nr-th subspace of W on the face_nr-th face. To ensure that 
        dofs on connectable faces have equal ordering of dofs, it will try to order these based on
        the distance to the origin. A warning is printed when this doesn't work (for example for
        higher order BDM elements), and will simply give the FEniCS native ordering.
        """
        if space_nr == -1:
            space_nr = self.bdy_spaces[0]
        all_dofs = get_compression_map(self.W,self.bdy_spaces)
        bdy_dofs = extract_boundary_dofs(self.W, space_nr,\
                                     self.face_definitions[face_nr])
        bdy_dofs_loc = np.array(np.in1d(all_dofs, bdy_dofs).nonzero()[0],dtype=np.int32)
        try:
            bdy_dof_coords = self.W.tabulate_dof_coordinates()[bdy_dofs]
            dist = np.array( [sum(bdy_dof_coords[d]**2) for d in range(len(bdy_dofs)) ] )
            argsort = np.argsort(dist)
            return  bdy_dofs_loc[argsort]
        except:
            print("Warning: internal face dofs are not sorted on location due to exception.")
            print("Most likely, tabulate_dof_coordinates is not implemented for this element.")
            return  bdy_dofs_loc

    @lru_cache(maxsize=None)
    def integrate_on_face(self,weak_form_ds,face_nr):
        """
        Integrates the weak_form_ds on face_nr and returns the stiffness matrix as a FEniCS PETScMatrix.
        """
        ds = self.ds_i(face_nr)
        weak_form = lambda u,v,ds=ds : weak_form_ds(u,v,ds)
        Ke = integrate_fine_scale_term(weak_form,self.W,self.bdy_spaces)
        return Ke

    @lru_cache(maxsize=None)
    def integrate_full_element(self,weak_form_dsdI,element_type):
        """
        Integrates the weak_form_dsdI on the element, with domain and interface boundaries corresponding
        to the element_type. Returns the stiffness matrix as a FEniCS PETScMatrix.
        """
        ds,dI = self.get_integration_boundaries(element_type)
        weak_form = lambda u,v,ds=ds,dI=dI : weak_form_dsdI(u,v,ds,dI)
        Ke = integrate_fine_scale_term(weak_form,self.W,self.bdy_spaces)
        return Ke

    def get_integration_boundaries(self,element_type):
        """
        Returns FEniCS measures ds and dI for the domain boundary and the internal faces,
        for the given element_type.
        """
        ds_nrs,dI_nrs = self.disect_boundaries(element_type)
        ds = self.sum_ds([self.ds_i(i) for i in ds_nrs]) if len(ds_nrs)>0 else self.ds_i(self.total_faces+1)
        dI = self.sum_ds([self.ds_i(i) for i in dI_nrs]) if len(dI_nrs)>0 else self.ds_i(self.total_faces+1)
        return ds,dI

    def sum_ds(self,ds_list):
        """
        Returns the union of ds measures given in the ds_list. For some reason this doesn't work with
        sime list contraction, hence a dedicated function for convenience.
        """
        ds = ds_list[0]
        for ds_ in ds_list[1:]:
            ds = ds + ds_
        return ds

    def create_local_spaces(self,element_types,Ps,bdy_spaces=[],refinement=4):
        """
        Needs to be overloaden to set the instance variable W to the fine-scale space, and then calls
        self.set_space(W,bdy_spaces).
        """
        pass
        
    def generate_coarse_scale_basis_functions(self,element_types,Ps):
        """
        Needs to be overloaden to set the coarse-scale instance variables Bs (list of lists of functions on W)
        and Bs_dof_locs (list of lists of Points).
        """
        pass

    def disect_boundaries(self,element_type):
        """
        Needs to be overloaden. Returns two tuples: one with the face numbers of the domain boundaries
        and one with the face numbers of the internal boundaries.
        """
        return (),tuple(range(self.total_faces))


class Element_1D(Element):
    def __init__(self,h):
        Element.__init__(self)
        self.h = h
        self.total_faces = 2
        self.face_definitions = [lambda x,b: x[0]<1E-6, lambda x,b: x[0]>h-1E-6] # left, right

    def create_local_spaces(self,element_types,Ps,bdy_spaces=[],refinement=4):
        """
        Sets the instance variable W to the fine-scale space, and then calls self.set_space(W,bdy_spaces).
        """
        W = get_fine_space_one_dimensional(self.h,element_types,Ps,refinement=refinement)
        self.set_space(W,bdy_spaces)
        
    def generate_coarse_scale_basis_functions(self,element_types,Ps):
        """
        Set the coarse-scale instance variables Bs (list of lists of functions on W) and Bs_dof_locs
        (list of lists of Points).
        """
        self.Bs,self.Bs_dof_locs = get_coarse_space_one_dimensional(self.h,element_types,Ps,self.W)

    def disect_boundaries(self,element_type):
        """
        Returns two tuples: one with the face numbers of the domain boundaries and one with the face numbers
        of the internal boundaries.
        """
        if element_type == 0: return (),(0,1) # Internal element
        if element_type == 1: return (0,),(1,)  # Left boundary
        if element_type == 2: return (1,),(0,)  # Right boundary
        if element_type == 3: return (0,1),() # Left and right boundary

class Element_2D_triangle(Element):
    def __init__(self,h,lu):
        Element.__init__(self)
        self.h = h
        self.lu = lu
        self.total_faces = 3
        if lu == "upper":
            # Left, top, diagonal
            self.face_definitions = [lambda x,b: x[0]<1E-6, \
                                     lambda x,b: x[1]>h-1E-6,
                                     lambda x,b: x[1]-x[0]<1E-6]
        if lu == "lower":
            # right, bottom, diagonal
            self.face_definitions = [lambda x,b: x[0]>h-1E-6, \
                                     lambda x,b: x[1]<1E-6,
                                     lambda x,b: x[1]-x[0]>-1E-6]

    def create_local_spaces(self,element_types,Ps,bdy_spaces=[],refinement=2):
        """
        Sets the instance variable W to the fine-scale space, and then calls self.set_space(W,bdy_spaces).
        """
        W = get_fine_space_triangle(self.h,element_types,Ps,which=self.lu,refinement=refinement)
        self.set_space(W,bdy_spaces)
        
    def generate_coarse_scale_basis_functions(self,element_types,Ps):
        """
        Set the coarse-scale instance variables Bs (list of lists of functions on W) and Bs_dof_locs
        (list of lists of Points).
        """
        self.Bs,self.Bs_dof_locs = get_coarse_space_triangle(self.h,element_types,Ps,self.W,which=self.lu)

    def disect_boundaries(self,element_type):
        """
        Returns two tuples: one with the face numbers of the domain boundaries and one with the face numbers
        of the internal boundaries.
        """
        if element_type == 0: return (),(0,1,2) # Internal element
        if element_type == 1: return (0,),(1,2)  # Vertical boundary
        if element_type == 2: return (1,),(0,2)  # Horizontal boundary
        if element_type == 3: return (0,1),(2,)  # Corner element
