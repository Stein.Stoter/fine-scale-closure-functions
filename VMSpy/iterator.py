from dolfin import *
import numpy as np
from petsc4py import PETSc
from multipledispatch import dispatch
from . import *

def CONFIRM_IMPORT_ITERATOR():
    return "confirmed"

class Iterator:
    """ Automatic assembling of block-based system of equations. """
    def __init__(self,connector=None):
        self.connector = connector
        
        # Bilinear form iteration data
        self.Kes = []
        self.assemble_functions = []
        self.mappings = []
        
        # Linear form iteration data
        self.Fes = []
        self.assemble_functions_force = []
        self.mappings_force = []
        
        self.__full_size = 0
        self.__total_elements = -1
        
        self.default_row_nz = 25
        self.__K = None
        self.Kisinit = False
        self.__F = None
        self.Fisinit = False
        self.U = None
        
    @property
    def K(self):
        if type(self.__K)==type(None): self.initialize_K()
        return self.__K
    
    @property
    def F(self):
        if type(self.__F)==type(None): self.initialize_F()
        return self.__F

    @property
    def total_elements(self):
        if self.__total_elements != -1:
            return self.__total_elements
        return self.connector.total_elements
        
    @total_elements.setter
    def total_elements(self,total_elements):
        self.__total_elements = total_elements

    @property
    def W(self):
        return self.connector.W
    
    @property
    def bdy_spaces(self):
        return self.connector.bdy_spaces
    
    @property
    def W_size(self):
        return self.connector.W_size
    
    @property
    def full_size(self):
        if self.__full_size == 0:
            self.__full_size = get_full_size(self.total_elements,self.W,self.bdy_spaces)
        return self.__full_size

    @full_size.setter
    def full_size(self,full_size):
        self.__full_size = full_size

    def initialize_K(self):
        """
        Creates an empty (unassembled) PETScMatrix of size self.full_size (automatically
        determined based on W_size and total_elements). The matrix has a sparsity pattern
        as given by self.getSparsityPattern. Also set the Kisinit flag to True.
        """
        nnz_per_row = self.getSparsityPattern()
        self.__K = create_empty_matrix(self.full_size, preallocation_nnz = nnz_per_row)
        self.Kisinit = True

    def getSparsityPattern(self):
        """
        Sparsity pattern for the preallocation of self.K. The basif implementation sets 
        each row to self.default_row_nz.
        """
        return self.default_row_nz

    def assemble_K(self,empty=True,assemble=True):
        """
        Loops over the given Kes with the corresponding assemble_functions and mappings
        and performs the associated operations. Before that it initilizes the PETScMatrix
        and afterwards it performs PETSc "assembly" if set to True (should only be done 
        once in the entire process of building the matrix).
        
        Before calling, the Ke, assemble_functions and mappings lists have to be populated:
        Kes : list with functions that are given an element number and return a PETScMatrix.
        assemble_functions_force : functions that are given the get_Fe function, the F vector,
            a total_elements number and a mapping function and perform the assembly. Typical
            candidates are those from FSCF.assembly:
                -   assemble_matrices
                -   assemble_submatrices
                -   assemble_rows
                -   assemble_columns
        mappings_force : list with functions that take the element number and produce
            the type of mapping that the assembly_function requires. Typical candidates are
                -   lambda n: n*W_size
                -   lambda n: from_block(n),to_block(n) (both as 2d-tuple interables of integers)
                -   lambda n: from_rows(n),to_rows(n),col_offset(n)
                -   lambda n: from_cols(n),to_cols(n),row_offset(n)
        """
        if empty==True: self.initialize_K()
        for get_Ke,assemble_function,mapping in zip(self.Kes,self.assemble_functions,self.mappings):
            assemble_function( get_Ke, self.K, self.total_elements, mapping,assemble=False)
        if assemble:        
            self.K.mat().assemble()

    def initialize_F(self):
        """
        Creates an empty PETScVector of size full_size, determined from W_size and total_elements.
        Also sets the Fisinit flag to True.
        """
        self.__F = create_empty_vector(self.full_size)
        self.Fisinit = True
            
    def assemble_F(self,empty=True):
        """
        Loops over the given Fes with the corresponding assemble_functions_force and mappings_force
        and performs the associated operations. Before that it initilizes the PETScVector.

        Before calling, the Fe, assemble_functions_force and mappings_force lists have to be populated:
        Fes : list with functions that are given an element number and return a PETScVector.
        assemble_functions_force : functions that are given the get_Fe function, the F vector,
            a total_elements number and a mapping function and perform the assembly. The main
            candidate from FSCF.assembly is:
            -   assemble_vectors
        mappings_force : list with functions that take the element number and produce
            the type of mapping that the assembly_function requires. Typical example:
            -   lambda n: n*W_size
        """
        if empty==True: self.initialize_F()
        for get_Fe,assemble_function,mapping in zip(self.Fes,self.assemble_functions_force,self.mappings_force):
            assemble_function( get_Fe, self.F, self.total_elements, mapping)
            
    def apply_boundary_condition(self,insidefunc,valuefun = lambda x: np.zeros(len(x)), subspace = 0,on_boundary=True):
        """
        Removes the values from the rows with dofs in subspace and flagged by insidefunc,
        and sets the diagonal to 1. The force  vector is set to valuefunc(dof location).
        insidefunc and valuefun should take arrays of locations.
        
        This operation may require new allocations (on diagonal) and requires an assembly
        operation. For optimal performance, it is called only once and called on a
        non-assembled matrix.

        insidefunc : function array locations -> array bools
        valuefun: optional function array locations -> array floats (defaults to zero's)
        subspace: optional int
        on_boundary: determine if only boundary elements are tested (is not a requirement on the dof)
        """
        dofs,locs = self.connector.get_global_dofs_locs_from_indicator(insidefunc,subspace,on_boundary=on_boundary)
        vals = valuefun(locs)
        apply_boundary_condition(self.K,dofs)    
        self.F.vec().setValues(dofs,vals)
        self.F.vec().assemble()

    def solve(self,solver="mumps"):
        """
        Solves the system Ku=F and stores U internally. Assembles K and F beforehand only 
        if the Kisinit resp. Fisinit flags are still False. 
        """
        if not self.Kisinit:
            self.assemble_K()
        if not self.Kisinit:
            self.assemble_F()
        self.U = solve_system(self.K,self.F,solver)
        
class ElementIterator(Iterator):
    """
    Together with the Connector and Element classes, this class forms the backbone of the system. 
    It allows the user to specify element-level weak forms, potentially involving boundary integrals
    on domain or interface faces, and specify the assembly routine ("full_element", "per_internal_face_row")
    or "per_internal_face_column").
    """
    def __init__(self,inp):
        if type(inp) is VMSIterator:
            self.__init__VMSIterator(inp)
        else:
            Iterator.__init__(self,inp)
            # Bilinear form iteration data
            self.element_routines = []
            
    def __init__VMSIterator(self,vms_iterator):
        """
        Copies over the connector, Kes and element_routines from the given vms_iterator. If 
        its Kisinit flag is set, then it will also copy over the (assumed) assembled matrix.
        """
        Iterator.__init__(self,None)
        # Copy assembly data
        self.connector = vms_iterator.connector
        self.Kes = vms_iterator.Kes
        self.element_routines = vms_iterator.element_routines
        # Copy matrix data if set
        if vms_iterator.Kisinit:
            self.initialize_K()
            full_range = range(self.full_size)
            block = [np.asarray(full_range, np.int32),np.asarray(full_range, np.int32)]
            place_submatrix(vms_iterator.K,self.K,block,block,assemble=False)
        
    def assemble_K(self,empty=True,assemble=True):
        """
        Overloaded from Iterator base class. Specific for element-based assembly.

        Loops over the the elements. In each element, it loops over the given Kes and
        corresponding element_routines (either "full_element", "per_internal_face_row" or 
        "per_internal_face_column") and performs the associated operations. Before that 
        it initilizes the PETScMatrix and afterwards it performs PETSc "assembly" if set 
        to True (should only be done once in the entire process of building the matrix).
        
        Before calling, the Kes and element_routines lists have to be populated:
        Kes : list with functions that take u,v,ds,dI for trial function, test function, 
            domain boundary and interface boundary, and return a PETScMatrix.
        element_routines : list with strings. "full_element": copies over the full matrix
            to the relevant block in the stiffness matrix. "per_internal_face_row": loops over
            all faces, recomputes the matrix for that face, copies all the face-dofs to the
            equivalent face-dofs of the neighboring element row-wise (trial is internal but 
            test is external). "per_internal_face_column": same thing but column-wise (test is
            internal but trial external).
        """
        if empty==True: self.initialize_K()
        for element_data in self.connector.iterate_elements():
            for WF,routine in zip(self.Kes,self.element_routines):
                if routine=="full_element":
                    self._assemble_K_full_element(*element_data, WF,place_block_diagonal_matrix)
                if routine=="per_internal_face_row":
                    self._assemble_K_per_internal_face(*element_data, WF,place_rows)
                if routine=="per_internal_face_column":
                    self._assemble_K_per_internal_face(*element_data, WF,place_columns)
        if assemble:        
            self.K.mat().assemble()

    def _assemble_K_full_element(self, element_nr,element,element_type, WF,assemble_function):
        vprint("element:",element_nr, element_type)
        Ke = element.integrate_full_element(WF,element_type)
        mapping = self.connector.get_map_all_dofs(element_nr)
        assemble_function(Ke,self.K,mapping,assemble=False)

    def _assemble_K_per_internal_face(self, element_nr,element,element_type, WF,assemble_function):
        for face_nr in range(element.total_faces):
            Ke = element.integrate_on_face(WF,face_nr)
            mapping = self.connector.get_map_opposing_face_dofs(element_nr,face_nr)
            assemble_function(Ke,self.K,*mapping,assemble=False)

class VMSIterator(ElementIterator):
    """
    This is the subclass of ElementIterator that focuses on VMS (fine-scale adjoint) problems.
    In addition to the usual element integration set-up, it adds functionality for integrating
    coarse-scale constraints (Ce's) as per-row/column additions to the stiffness matrix, and
    for integrating dof-dependent scale-interaction linear forms (Se's).
    """
    @dispatch(Connector)
    def __init__(self,connector):
        ElementIterator.__init__(self,connector)
        self.__init_Ces()
        
    @dispatch(ElementIterator)
    def __init__(self,iterator):
        """
        Copies over the connector, Kes and element_routines from the given iterator. If its 
        Kisinit flag is set, then it will also copy over the (assumed) assembled matrix.
        """
        Iterator.__init__(self)
        # Copy assembly data
        self.connector = iterator.connector
        self.Kes = iterator.Kes
        self.element_routines = iterator.element_routines
        self.__init_Ces()
        self.__init_Ses()
        # Copy matrix data if set
        if iterator.Kisinit:
            self.initialize_K()
            place_block_diagonal_matrix(iterator.K,self.K,assemble=False)

    def __init_Ces(self):
        # Constraint equation iteration data
        self.__Ce_spaces = []
        self.Ces = []
        self.Ce_element_routines = []
        self.__full_size_CS = 0

    def __init_Ses(self):
        # Scale interaction iteration data
        self.__Se_spaces = []
        self.Ses = []
        self.Se_element_routines = []
        
    @property
    def full_size(self):
        # Overloaded; full_size now also incudes the number coarse-scale basis functions
        # in self.connector's coarse-scale space, as these will be coarse-scale constraints.
        if self.__full_size_CS == 0:
            full_size = get_full_size(self.total_elements,self.W,self.bdy_spaces)
            for CS in self.connector.coarse_spaces:
                full_size += CS.dim()
            self.__full_size_CS = full_size
        return self.__full_size_CS

    @full_size.setter
    def full_size(self,full_size):
        self.__full_size_CS = full_size

    @property
    def Ce_spaces(self):
        if len(self.connector.coarse_spaces) == 1:
            self.__Ce_spaces = [0]*len(self.Ces)
        return self.__Ce_spaces

    @Ce_spaces.setter
    def Ce_spaces(self,Ce_spaces):
        self.__Ce_spaces = Ce_spaces

    @property
    def Se_spaces(self):
        if len(self.connector.coarse_spaces) == 1:
            self.__Se_spaces = [0]*len(self.Ses)
        return self.__Se_spaces

    @Se_spaces.setter
    def Se_spaces(self,Se_spaces):
        self.__Se_spaces = Se_spaces

    def getSparsityPattern(self):
        """
        Overloaded: this is the sparsity pattern for the preallocation of the stiffness matrix.
        Given that the rows and columns corresponding to the coarse-scale constraints are quite
        densely filled, this needs to be taken into account.
        """
        nnz_def = ElementIterator.getSparsityPattern(self)
        if nnz_def is None:
            return nnz_def
        nnz_per_row = np.ones(self.full_size, dtype=np.int32)*nnz_def   
        offset = self.total_elements*self.W_size
        for cs_space in self.connector.coarse_spaces:
            approx_nnz_cols = int( cs_space.dim()/self.connector.total_elements+2.5) * (self.connector.dimension+1)
            nnz_per_row += approx_nnz_cols

            newoffset = offset+cs_space.dim()
            nnz_fact_constraints = 3*self.connector.dimension
            nnz_per_row[offset:newoffset] = min(offset/2,nnz_fact_constraints*self.W_size)
            offset = newoffset
        return nnz_per_row
        
    def assemble_K_coarse_scale_constraints(self,empty=False,assemble=True):
        """
        Element-based assembly of the coarse-scale constraint equations: the rows and columns
        of the saddle-point problem.

        Loops over the the elements. In each element, it loops over the given Ces and
        corresponding element_routines (either "full_element" or "per_internal_face") 
        and performs the associated operations. Before that it initilizes the PETScMatrix 
        and afterwards it performs PETSc "assembly" if set to True (should only be done
        once in the entire process of building the matrix).
        
        Before calling, the Ces, Ce_element_routines and Ce_spaces lists have to be populated:
        Ces : list with functions that take coarse_basis,vq,ds,dI as Function, test function, 
            domain boundary and interface boundary, and return a PETScVector.
        Ce_element_routines : list with strings. "full_element": copies over the full matrix
            to the relevant block in the stiffness matrix. "per_internal_face": loops over
            all faces, recomputes the matrix for that face, copies all the face-dofs to the
            equivalent face-dofs of the neighboring element.
        Ce_spaces : list with integers that denote the subspace to which the coarse-scale
            basis function corresponds. If this list is kept empty, it will default to a list
            of zeros.
        """
        if empty==True: self.initialize_K()
        for element_data in self.connector.iterate_elements():
            for get_Ce,CS_space_nr,routine in zip(self.Ces,self.Ce_spaces,self.Ce_element_routines):
                if routine=="full_element":
                    self._assemble_K_coarse_scale_constraints_full_element(*element_data, get_Ce,CS_space_nr)
                if routine=="per_internal_face":
                    self._assemble_K_coarse_scale_constraints_per_internal_face(*element_data, get_Ce,CS_space_nr)
        if assemble:
            self.K.mat().assemble()
            
    Ce_wrapper_cache_full_element = {}
    def _assemble_K_coarse_scale_constraints_full_element(self,element_nr,element,element_type, get_Ce,CS_space_nr):
        vprint("element:",element_nr, element_type)
        for Bi,B in enumerate(element.Bs[CS_space_nr]):
            # Manual cache to facilitate the element integration cache:
            if (get_Ce,B) in self.Ce_wrapper_cache_full_element:
                Ce_wrapper = self.Ce_wrapper_cache_full_element[(get_Ce,B)]
            else:
                Ce_wrapper = lambda ul,vq,ds,dI,B=B: get_Ce(B,vq,ds,dI)
                self.Ce_wrapper_cache_full_element[(get_Ce,B)] = Ce_wrapper
            Ce_wrapper = lambda ul,vq,ds,dI,B=B: get_Ce(B,vq,ds,dI)
                
            Ce = element.integrate_full_element(Ce_wrapper,element_type)
            CS_dof = self.connector.get_map_coarse_dof(element_nr,Bi,CS_space_nr=CS_space_nr)
            offset = self.connector.get_map_all_dofs(element_nr)
            place_vector_in_matrix(Ce,self.K,CS_dof,offset,assemble=False)
            place_vector_in_matrix(Ce,self.K,offset,CS_dof,transpose=True,assemble=False)

    Ce_wrapper_cache_internal_face = {}
    def _assemble_K_coarse_scale_constraints_per_internal_face(self,element_nr,element,element_type, get_Ce,CS_space_nr):
        for face_nr in range(element.total_faces):
            for Bi,B in enumerate(element.Bs[CS_space_nr]):
                # Manual cache to facilitate the element integration cache:
                if (get_Ce,B) in self.Ce_wrapper_cache_internal_face:
                    Ce_wrapper = self.Ce_wrapper_cache_internal_face[(get_Ce,B)]
                else:
                    Ce_wrapper = lambda ul,vq,dI,B=B: get_Ce(B,vq,dI)
                    self.Ce_wrapper_cache_internal_face[(get_Ce,B)] = Ce_wrapper
                Ce_wrapper = lambda ul,vq,dI,B=B: get_Ce(B,vq,dI)
                    
                Ce = element.integrate_on_face(Ce_wrapper,face_nr)
                CS_dof = self.connector.get_map_coarse_dof(element_nr,Bi,CS_space_nr=CS_space_nr)
                loc_face_dofs = []; opp_face_dofs = []
                for space_nr in range(self.W.num_sub_spaces()):
                    loc,opp,_ = self.connector.get_map_opposing_face_dofs(element_nr,face_nr,space_nr)
                    loc_face_dofs.append(loc); opp_face_dofs.append(opp)
                loc_face_dofs = np.hstack( loc_face_dofs ); opp_face_dofs = np.hstack( opp_face_dofs )
                place_vector_in_matrix(Ce[loc_face_dofs],self.K,CS_dof,opp_face_dofs,assemble=False)
                place_vector_in_matrix(Ce[loc_face_dofs],self.K,opp_face_dofs,CS_dof,transpose=True,assemble=False)

    def assemble_F_scale_interaction(self,CS_dof,empty=True, S_set=None):
        """
        Assembly of the scale-interaction vector for a given dof. `CS_dof` is the global
        representation of the dof (row in the fine-scale stiffness matrix minus offset).

        Loops over the the elements. In each element, it checks if the given dof has support
        in the element. If so, it assembles the presupplied linear forms (Se's) for which dof
        is in `Se_spaces` based on the corresponding element_routines (either "full_element" 
        or "per_internal_face"). Before that it initilizes the PETScVector and afterwards it 
        performs PETSc "assembly".
        
        Before calling, the Ses, Se_element_routines and Se_spaces lists have to be populated:
        Ses : list with functions that take coarse_basis,vq,ds,dI as Function, test function, 
            domain boundary and interface boundary, and return a PETScVector.
        Se_element_routines : list with strings. "full_element": copies over the full vector
            to the relevant rows in the global vector. "per_internal_face": loops over all
            faces, recomputes the matrix for that face, copies all the face-dofs to the
            equivalent face-dofs of the neighboring element.
        Se_spaces : list with integers that denote the subspace to which the coarse-scale
            basis function corresponds. If this list is kept empty, it will default to a list
            of zeros. Most likely, all values should be the same.

        Alternatively, the above lists can be passed as a tuple to S_set.
        """
        if empty==True: self.initialize_F()
        S_set = [self.Ses,self.Se_element_routines,self.Se_spaces] if type(S_set)==type(None) else S_set
        for WF,routine,space_nr in zip(*S_set):
            # Can still be improved, to cancel the search if CS_dof does not correspond to space_nr
            base_offset = self.total_elements*self.W_size
            for element_nr,element,element_type in self.connector.iterate_elements():
                for Bi,B in enumerate(element.Bs[space_nr]):
                    B_global_dof = self.connector.get_map_coarse_dof(element_nr,Bi,CS_space_nr=space_nr)
                    if B_global_dof-base_offset == CS_dof:
                        if routine == "full_element":
                            self._assemble_F_scale_interaction_full_element(element_nr,element,element_type, WF,B)
                        elif routine == "per_internal_face":
                            self._assemble_F_scale_interaction_per_internal_face(element_nr,element,element_type, WF,B)
        self.F.vec().assemble()
                    
    def _assemble_F_scale_interaction_full_element(self,element_nr,element,element_type, WF,B):
        Fe_wrapper = lambda ul,vq,ds,dI,B=B: WF(B,vq,ds,dI)
        Fe = element.integrate_full_element(Fe_wrapper,element_type)
        offset = self.connector.get_map_all_dofs(element_nr)
        place_vector(Fe,self.F,offset, assemble=False)
        
    def _assemble_F_scale_interaction_per_internal_face(self,element_nr,element,element_type, WF,B):
        for face_nr in range(element.total_faces):
            Fe_wrapper = lambda ul,vq,dI,B=B: WF(B,vq,dI)
            Fe = element.integrate_on_face(Fe_wrapper,face_nr)
            loc_face_dofs = []; opp_face_dofs = []
            for space_nr in range(self.W.num_sub_spaces()):
                loc,opp,_ = self.connector.get_map_opposing_face_dofs(element_nr,face_nr,space_nr)
                loc_face_dofs.append(loc); opp_face_dofs.append(opp)
            loc_face_dofs = np.hstack( loc_face_dofs ); opp_face_dofs = np.hstack( opp_face_dofs )
            place_vector(Fe[loc_face_dofs],self.F,opp_face_dofs, assemble=False)
                    
    def eliminate_coarse_scale_constraints(self,insidefunc,CS_space_nr = 0):
        """
        Removes the rows correseponding to the coarse-scale dofs of `CS_space_nr` that are 
        in `insidefunc`, and sets a 1 on the diagonal. This is required for coarse-scale
        formulations that involve strong BCs on the coarse-scale space (which also eliminates
        the corresponding constraint equation). Most non-DG methods would require this.
        """
        base_offset = self.total_elements*self.W_size
        for cs in range(CS_space_nr):
            base_offset += self.connector.coarse_spaces[cs].dim()
        for i,loc in enumerate(self.connector.coarse_spaces[CS_space_nr].tabulate_dof_coordinates()):
            if insidefunc(loc):
                apply_boundary_condition(self.K,i+base_offset)
