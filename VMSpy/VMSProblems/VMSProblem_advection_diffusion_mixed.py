from ..vmsproblem import *
from dolfin import *

"""
Solves a Dirichlet boundary mixed formulation of the Poisson problem:

(kappa^-1 tau, sig ) - (kappa^-1 tau, a phi ) - (div(tau), phi) + (w, div(sig)) + (avg(l) , jump(tau)) = (f,tau)
(avg(q), jump(tau)) = 0
(jump(q) , jump(l)) = 0

by separating into elements. The fields u and l^in are element local,
and all coupling occurs through l^\pm and q^\pm. Per element:

(kappa^-1 tau, sig ) - (kappa^-1 tau, a phi ) - (div(tau), phi) + (w, div(sig))
+ (0.5*l^in , tau^in dot n^in) + (0.5*q^in , tau^in n^in)
(q^in , l^in) 
+ (0.5*l^out , tau^in n^in) + (0.5*q^out , tau^in n^in)
- (q^out , l^in)
= (f,v)_K
"""

def getVMSProblem(connector,a_vec,kappa, force=None,BCset=BCSet(), \
                    projector="MM", DG_parameters = (Constant(0),Constant(0),Constant(0),Constant(0)),\
                    scale_interaction_mode="FE"):
    """
    Returns the VMSProblem for a mixed form of an advection-diffusion problem. Requires:
    - connector: the Connector object with BDM-DG-BDM fine-scale spaces
         and BDM-DG coarse-scale spaces.
    - a_vec: the constant advective vector
    - kappa: the constant diffusion parameter
    - projector: "L2", "MM" or "LDG"
    And optional key word arguments:
    - DG_parameters: tuple for (eta,eta_bdy,beta,C)
    - force: the weak form function for the full-scale problem: force(sul,tvq,ds,dI,**kwargs).
    - scale_interaction_mode: "FE" or "CS" for Finite Element or Coarse-Scale.
         In FE, the scale interaction is only the remaining advective term
         In CS, the scale interaction is all fine-scale terms.
    """
    eta,eta_bdy,beta,C = DG_parameters
    nn   = Constant((1,)) if connector.dimension == 1 else Constant((1,0.5))
    unit = Constant((1,)) if connector.dimension == 1 else Constant(1)
    unit_x = unit
    unit_y = unit
    if projector == "LDG" and connector.dimension == 2:
        unit_x = Constant((1,0))
        unit_y = Constant((0,1))
    
    # Fine scale bilinear form
    def weak_form_internal(ul,vq,ds,dI):
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        sig,phi,l = split(ul); sig=sig*unit; l=inner(l*unit,n_tilde)
        tau,w,q = split(vq); tau=tau*unit; q=inner(q*unit,n_tilde)
        return inner(kappa**(-1)*tau, sig )*dx - inner(kappa**(-1)*tau, a_vec*phi )*dx \
               - (div(tau) * phi)*dx - (w * div(sig))*dx \
               + 0.5*l*inner(n,tau)*dI + 0.5*q*inner(n,sig)*dI \
               + l*q*dI \
               + l*q*ds
    def weak_form_cross_lmp(ul,vq,dI):
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        sig,phi,l = split(ul); sig=sig*unit; l=inner(l*unit,n_tilde)
        tau,w,q = split(vq); tau=tau*unit; q=inner(q*unit,n_tilde)
        return 0.5*l*inner(n,tau)*dI + 0.5*q*inner(n,sig)*dI
    def weak_form_lmp_jump_ext(ul,vq,dI):
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        sig,phi,l = split(ul); sig=sig*unit; l=inner(l*unit,n_tilde)
        tau,w,q = split(vq); tau=tau*unit; q=inner(q*unit,n_tilde)
        return -l*q*dI
    weak_forms_full_scale = WeakFormSet()
    weak_forms_full_scale.add_routine(weak_form_internal,"full_element")
    weak_forms_full_scale.add_routine(weak_form_cross_lmp,"per_internal_face_column")
    weak_forms_full_scale.add_routine(weak_form_cross_lmp,"per_internal_face_row")
    weak_forms_full_scale.add_routine(weak_form_lmp_jump_ext,"per_internal_face_column")

    # Fine scale linear form
    def weak_form_body_force(ul,vq,ds,dI,*args,**kwargs):
        tau,v,q = split(vq)
        return -Constant(1)*v*dx
    if not type(force)==type(None):
        weak_form_body_force = force
    def getFe(el_nr,connector):
        element = connector.get_element(el_nr)
        element_loc = connector.get_element_location(el_nr)
        element_type = connector.get_element_type(el_nr)
        L = lambda ul,vq,ds,dI,ex=element_loc,e=element: weak_form_body_force(ul,vq,ds,dI,ex,e)
        return element.integrate_full_element(L,element_type)
    weak_forms_force = WeakFormSet()
    weak_forms_force.add_routine(getFe,"")

    # Projector constraints on the fine-scale space
    def L2_constraint_tau(coarse_basis_tau,vq,ds,dI):
        tau,w,q = split(vq)
        tau=tau*unit; coarse_basis_tau = coarse_basis_tau*unit
        return inner(coarse_basis_tau,tau) * dx
    def L2_constraint_v(coarse_basis_v,vq,ds,dI):
        tau,w,q = split(vq)
        tau=tau*unit
        return coarse_basis_v * w * dx
    def MM_constraint_tau(coarse_basis_tau,vq,ds,dI):
        tau,w,q = split(vq)
        tau=tau*unit; coarse_basis_tau = coarse_basis_tau*unit
        return inner(kappa**(-1)*coarse_basis_tau, tau )*dx \
               - (div(coarse_basis_tau) * w)*dx
    def MM_constraint_v(coarse_basis_v,vq,ds,dI):
        tau,w,q = split(vq)
        tau=tau*unit
        return - (coarse_basis_v * div(tau))*dx
    def LDG_constraint_tau( coarse_basis_tau,vq,ds,dI,unit_cs=unit_x ):
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde); coarse_basis_tau=coarse_basis_tau*unit_cs
        return inner(kappa**(-1)*coarse_basis_tau, tau )*dx \
               - (div(coarse_basis_tau) * w)*dx \
               + 0.5*q*inner(n,coarse_basis_tau)*dI \
               + inner(n,coarse_basis_tau)*inner(beta*n_tilde,n*q)*dI \
               + kappa**-1*C*inner(n,coarse_basis_tau)*inner(n,tau)*dI
    LDG_constraint_tau_y = lambda cb_tau,vq,ds,dI,uy=unit_y : LDG_constraint_tau( cb_tau,vq,ds,dI,unit_cs=uy )
    def LDG_constraint_tau_ext(coarse_basis_tau,vq,dI,unit_cs=unit_x ):
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde); coarse_basis_tau=coarse_basis_tau*unit_cs
        return 0.5*q*inner(n,coarse_basis_tau)*dI \
               - inner(n,coarse_basis_tau)*inner(beta*n_tilde,n*q)*dI \
               - kappa**-1*C*inner(n,coarse_basis_tau)*inner(n,tau)*dI
    LDG_constraint_tau_ext_y = lambda cb_tau,vq,dI,uy=unit_y : LDG_constraint_tau_ext( cb_tau,vq,dI,unit_cs=uy )
    def LDG_constraint_v(coarse_basis_v,vq,ds,dI):
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde)
        return -(coarse_basis_v * div(tau))*dx \
               + 0.5*coarse_basis_v*inner(n,tau)*dI \
               + inner(beta*n_tilde,n*coarse_basis_v)*inner(n,tau)*dI \
               + kappa*eta*coarse_basis_v*q*dI \
               + kappa*eta_bdy*coarse_basis_v*q*ds
    def LDG_constraint_v_ext(coarse_basis_v,vq,dI):
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde)
        return - 0.5*coarse_basis_v*inner(n,tau)*dI \
               - inner(beta*n_tilde,n*coarse_basis_v)*inner(n,tau)*dI\
               - kappa*eta*coarse_basis_v*q*dI
    constraint_dict = { "L2" : [ (L2_constraint_tau,"full_element",0), (L2_constraint_v,"full_element",1) ], \
                        "MM" : [ (MM_constraint_tau,"full_element",0), (MM_constraint_v,"full_element",1) ], \
                        "LDG": [ (LDG_constraint_tau,"full_element",0),(LDG_constraint_v,"full_element",1), \
               (LDG_constraint_tau_ext,"per_internal_face",0),(LDG_constraint_v_ext,"per_internal_face",1) ] }
    if connector.dimension == 2:
        constraint_dict["LDG"].append(  (LDG_constraint_tau_y,"full_element",2)   )
        constraint_dict["LDG"].append(  (LDG_constraint_tau_ext_y,"per_internal_face",2)   )
    weak_forms_constraints = WeakFormSet()
    for WF,routine,space_nr in constraint_dict[projector]:
        weak_forms_constraints.add_routine( WF,routine,space_nr )

    # Scale interaction term
    def S_full_tau(coarse_basis_tau,vq,ds,dI):
        # Full scale interaction term, without manual elimination
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde); coarse_basis_tau = coarse_basis_tau*unit
        return -(   inner(kappa**(-1)*coarse_basis_tau, tau )*dx \
               - inner(kappa**(-1)*coarse_basis_tau, a_vec*w )*dx \
               - (div(coarse_basis_tau) * w)*dx \
               + 0.5*q*inner(n,coarse_basis_tau)*dI   )
    def S_ext_tau(coarse_basis_tau,vq,dI):
        # Full scale interaction term, without manual elimination
        tau,w,q = split(vq)
        n = FacetNormal( vq.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        tau=tau*unit; q=inner(q*unit,n_tilde); coarse_basis_tau = coarse_basis_tau*unit
        return -(   0.5*q*inner(n,coarse_basis_tau)*dI    )
    def S_full_w(coarse_basis_v,vq,ds,dI):
        # Full scale interaction term, without manual elimination
        tau,w,q = split(vq)
        tau=tau*unit
        n = FacetNormal( vq.function_space().mesh() )
        return -(   - (coarse_basis_v * div(tau))*dx \
               + 0.5*coarse_basis_v*inner(n,tau)*dI \
               + coarse_basis_v*q*dI \
               + coarse_basis_v*q*ds  )
    def S_ext_w(coarse_basis_v,vq,dI):
        # Full scale interaction term, without manual elimination
        tau,w,q = split(vq); tau=tau*unit
        n = FacetNormal( vq.function_space().mesh() )
        return -(   -0.5*coarse_basis_v*inner(n,tau)*dI \
                    - coarse_basis_v*q*dI   )
    def S_adv(coarse_basis_tau,vq,ds,dI, unit_cs=unit_x):
        # Advective scale interaction term, after manual elimination of the diffusive terms
        tau,w,q = split(vq)
        tau=tau*unit; coarse_basis_tau = coarse_basis_tau*unit_cs
        return -(   -inner(kappa**(-1)*coarse_basis_tau, a_vec*w )*dx   )
    scale_interaction_dict = { "FE" : [ (S_adv,"full_element",0) ], \
                               "CS" : [ (S_full_tau,"full_element",0),(S_full_w,"full_element",1), \
                                (S_ext_tau,"per_internal_face",0),(S_ext_w,"per_internal_face",1)] }
    weak_forms_scale_interaction = WeakFormSet()
    for WF,routine,space_nr in scale_interaction_dict[scale_interaction_mode]:
        weak_forms_scale_interaction.add_routine( WF,routine,space_nr )

    # Scale interaction term acting on coarse-scale trial
    def B_sig_g(coarse_basis_sig,hgl,ds,dI):
        # B( sig^h , g' )
        # coarse_basis is the trial function, hgl is the testfunction (fine-scale closure function)
        h,g,l = split(hgl)
        n = FacetNormal( hgl.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        h=h*unit; l=inner(l*unit,n_tilde); coarse_basis_sig = coarse_basis_sig*unit
        return inner(kappa**(-1)*coarse_basis_sig, h )*dx \
               - (div(coarse_basis_sig) * g)*dx \
               + 0.5*l*inner(n,coarse_basis_sig)*dI
    def B_sig_g_ext(coarse_basis_sig,hgl,dI):
        # B( sig^h , g' )
        # coarse_basis is the trial function, hgl is the testfunction (fine-scale closure function)
        h,g,l = split(hgl)
        n = FacetNormal( hgl.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        h=h*unit; l=inner(l*unit,n_tilde); coarse_basis_sig = coarse_basis_sig*unit
        return 0.5*l*inner(n,coarse_basis_sig)*dI
    def B_u_g(coarse_basis_u,hgl,ds,dI):
        # B( u^h , g' )
        # coarse_basis is the trial function, hgl is the testfunction (fine-scale closure function)
        h,g,l = split(hgl)
        n = FacetNormal( hgl.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        h=h*unit; l=inner(l*unit,n_tilde)
        return - inner(kappa**(-1)*h, a_vec*coarse_basis_u )*dx \
               - (coarse_basis_u * div(h))*dx \
               + 0.5*coarse_basis_u*inner(n,h)*dI \
               + coarse_basis_u*l*dI \
               + coarse_basis_u*l*ds
    def B_u_g_ext(coarse_basis_u,hgl,dI):
        # B( u^h , g' )
        # coarse_basis is the trial function, hgl is the testfunction (fine-scale closure function)
        h,g,l = split(hgl)
        n = FacetNormal( hgl.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        h=h*unit; l=inner(l*unit,n_tilde)
        return -0.5*coarse_basis_u*inner(n,h)*dI \
               - coarse_basis_u*l*dI
    weak_forms_full_scale_interaction = WeakFormSet()
    weak_forms_full_scale_interaction.add_routine( B_sig_g,"full_element",0 )
    weak_forms_full_scale_interaction.add_routine( B_sig_g_ext,"per_internal_face",0 )
    weak_forms_full_scale_interaction.add_routine( B_u_g,"full_element",1 )
    weak_forms_full_scale_interaction.add_routine( B_u_g_ext,"per_internal_face",1 )

    # Projector constraints on the coarse-scale space
    def L2_constraint_coarse_scale(u,v):
        try:
            sigx,phi,sigy = split(u); sig = as_vector([sigx,sigy])
            taux,w,tauy = split(v); tau = as_vector([taux,tauy])
        except:
            sig,phi = split(u); sig=sig*unit
            tau,w = split(v); tau=tau*unit
        return inner(sig,tau)*dx + phi*w*dx
    def MM_constraint_coarse_scale(u,v):
        sig,phi = split(u); sig=sig*unit
        tau,w = split(v); tau=tau*unit
        n = FacetNormal( v.function_space().mesh() )
        return kappa**(-1)*inner(tau, sig)*dx \
            - div(tau) * phi * dx - w * div(sig) * dx
    def LDG_constraint_coarse_scale(u,v):
        try:
            sigx,phi,sigy = split(u); sig = as_vector([sigx,sigy])
            taux,w,tauy = split(v); tau = as_vector([taux,tauy])
        except:
            sig,phi = split(u); sig=sig*unit
            tau,w = split(v); tau=tau*unit
        n = FacetNormal( v.function_space().mesh() )
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        return kappa**(-1)*inner(tau, sig)*dx \
            - div(tau) * phi * dx - w * div(sig) * dx \
            + jump(tau,n)*avg(phi)*dS + jump(sig,n)*avg(w)*dS \
            + jump(tau,n)*inner(avg(beta*n_tilde),jump(phi,n))*dS + jump(sig,n)*inner(avg(beta*n_tilde),jump(w,n))*dS \
            + kappa**-1*C*jump(tau,n)*jump(sig,n)*dS + kappa*eta*inner(jump(phi,n),jump(w,n))*dS \
            + kappa*eta_bdy*phi*w*ds
    projector_coarse_scale_dict = { "L2" : L2_constraint_coarse_scale, \
                                    "MM" : MM_constraint_coarse_scale, \
                                    "LDG": LDG_constraint_coarse_scale}
    weak_forms_projector_coarse_scale = projector_coarse_scale_dict[projector]

    # Finite element weak formulation without scale interaction
    def weak_form_coarse_scale(u,v):
        try:
            sigx,phi,sigy = split(u); sig = as_vector([sigx,sigy])
            taux,w,tauy = split(v); tau = as_vector([taux,tauy])
        except:
            sig,phi = split(u); sig=sig*unit
            tau,w = split(v); tau=tau*unit
        n = FacetNormal(v.function_space().mesh())
        n_tilde = n*abs( inner(n,nn) )/inner(n,nn)
        a_CS = kappa**(-1)*inner(tau, sig)*dx - kappa**(-1)*inner(a_vec*phi,tau)*dx \
            - div(tau) * phi * dx - w * div(sig) * dx \
            + jump(tau,n)*avg(phi)*dS + jump(sig,n)*avg(w)*dS 
        if scale_interaction_mode == "FE":
            a_CS += jump(tau,n)*inner(avg(beta*n_tilde),jump(phi,n))*dS + jump(sig,n)*inner(avg(beta*n_tilde),jump(w,n))*dS \
                    + kappa**-1*C*jump(tau,n)*jump(sig,n)*dS + kappa*eta*inner(jump(phi,n),jump(w,n))*dS \
                    + kappa*eta_bdy*phi*w*ds
        else:
            a_CS += inner(jump(phi,n),jump(w,n))*dS + phi*w*ds
        return  a_CS
    def weak_form_coarse_scale_force(v):
        try:
            taux,w,tauy = split(v); tau = as_vector([taux,tauy])
        except:
            tau,w = split(v); tau=tau*unit
        f = Expression("( 2*(x[0]) - (x[1]) < 0  ) ? 1 : -1",degree=0)
        return -w*dx

    # VMS problem
    VMS_problem = VMSProblem(connector)
    VMS_problem.set_bilinear_form_full_scale(weak_forms_full_scale)
    VMS_problem.set_linear_form_full_scale(weak_forms_force)
    VMS_problem.set_bilinear_form_constraints(weak_forms_constraints)
    VMS_problem.set_linear_form_scale_interaction(weak_forms_scale_interaction)
    VMS_problem.set_bilinear_form_projector_coarse_scales(weak_forms_projector_coarse_scale)
    VMS_problem.set_bilinear_form_coarse_scale(weak_form_coarse_scale)
    VMS_problem.set_linear_form_coarse_scale(weak_form_coarse_scale_force)
    VMS_problem.set_bilinear_form_full_scale_interaction(weak_forms_full_scale_interaction)
    VMS_problem.set_boundary_condition(BCset)

    VMS_problem.build_all()

    return VMS_problem
    
