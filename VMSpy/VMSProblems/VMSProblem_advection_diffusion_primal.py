from ..vmsproblem import *
from dolfin import *

"""
Solves:

(a v, grad u )+(kappa grad u, grad v) + (avg(l) n_tilde , jump(v)) = (f,v)
(avg(q) n_tilde, jump(u)) = 0
(jump(q) , jump(l)) = 0

by separating into elements. The fields u and v are always internal,
in the third term l is chosen internal. All coupling then occurs through l^\pm and q^\pm. Per element:

(a grad u , v )_K + (kappa grad u, grad v)_K
+ (0.5*l^in n_tilde, v^in n^in) + (0.5*q^in n_tilde, u^in n^in)
(q^in , l^in) 
+ (0.5*l^out n_tilde, v^in n^in) + (0.5*q^out n_tilde, u^in n^in)
- (q^out , l^in)
= (f,v)_K
"""

def getVMSProblem(connector,a_vec,kappa, force=None,BCset=BCSet(), \
                    projector="L2", DG_parameters = (Constant(0),Constant(0)), scale_interaction_mode="FE"):
    """
    Returns the VMSProblem for a primal form of an advection-diffusion problem. Requires:
    - connector: the Connector object
    - a_vec: the constant advective vector
    - kappa: the constant diffusion parameter
    - projector: "L2", "H10", "Nitsche" or "IP"
    And optional key word arguments:
    - DG_parameters: tuple for (eta,eta_bdy)
    - force: the weak form function for the full-scale problem: force(ul,vq,ds,dI,**kwargs).
    - scale_interaction_mode: "FE" or "CS" for Finite Element or Coarse-Scale.
         In FE, the scale interaction is only the remaining advective term
         In CS, the scale interaction is all fine-scale terms.
    """
    eta,eta_bdy = DG_parameters
    nn = Constant((1,)) if connector.dimension == 1 else Constant((1,0.5))
    
    # Fine scale bilinear form
    def weak_form_internal(ul,vq,ds,dI):
        n = FacetNormal( ul.function_space().mesh() )
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        u,l = split(ul)
        v,q = split(vq)
        return inner( a_vec*v, grad(u) )*dx + inner( kappa*grad(u), grad(v) )*dx + \
               u*q*ds + l*v*ds \
               + 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI \
               + l*q*dI
    def weak_form_cross_lmp(ul,vq,dI):
        n = FacetNormal( ul.function_space().mesh() )
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        u,l = split(ul)
        v,q = split(vq)
        return 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI
    def weak_form_lmp_jump_ext(ul,vq,dI):
        u,l = split(ul)
        v,q = split(vq)
        return -l*q*dI
    weak_forms_full_scale = WeakFormSet()
    weak_forms_full_scale.add_routine(weak_form_internal,"full_element")
    weak_forms_full_scale.add_routine(weak_form_cross_lmp,"per_internal_face_column")
    weak_forms_full_scale.add_routine(weak_form_cross_lmp,"per_internal_face_row")
    weak_forms_full_scale.add_routine(weak_form_lmp_jump_ext,"per_internal_face_column")

    # Fine scale linear form
    def weak_form_body_force(ul,vq,ds,dI):
        v,q = split(vq)
        return Constant(1)*v*dx
    if not type(force)==type(None):
        weak_form_body_force = force
    def getFe(el_nr,connector):
        element = connector.get_element(el_nr)
        element_loc = connector.get_element_location(el_nr)
        element_type = connector.get_element_type(el_nr)
        L = lambda ul,vq,ds,dI,ex=element_loc,e=element: weak_form_body_force(ul,vq,ds,dI,ex,e)
        return element.integrate_full_element(L,element_type)
    weak_forms_force = WeakFormSet()
    weak_forms_force.add_routine(getFe,"")

    # Projector constraints on the fine-scale space
    def L2_constraint(coarse_basis,vq,ds,dI):
        v,q = split(vq)
        return v*coarse_basis*dx
    def H10_constraint(coarse_basis,vq,ds,dI):
        v,q = split(vq)
        return inner(grad(v),grad(coarse_basis))*dx
    def Nitsche_constraint(coarse_basis,vq,ds,dI):
        n = FacetNormal(vq.function_space().mesh())
        v,q = split(vq)
        return inner(grad(v),grad(coarse_basis))*dx \
               + q/kappa*coarse_basis*ds - inner(grad(coarse_basis),n)*v*ds \
               + eta_bdy*v*coarse_basis*ds
    def IP_constraint_int(coarse_basis,vq,ds,dI):
        n = FacetNormal(vq.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        v,q = split(vq)
        return inner(grad(v),grad(coarse_basis))*dx \
               + 0.5*q/kappa*inner(n,n_tilde)*coarse_basis*dI - 0.5*inner(grad(coarse_basis),n_tilde)*inner(n_tilde,n)*v*dI \
               + eta*v*coarse_basis*dI \
               + q/kappa*coarse_basis*ds - inner(grad(coarse_basis),n)*v*ds \
               + eta_bdy*v*coarse_basis*ds
    def IP_constraint_ext(coarse_basis,vq,dI):
        n = FacetNormal(vq.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        v,q = split(vq)
        return 0.5*q/kappa*inner(n,n_tilde)*coarse_basis*dI + 0.5*inner(grad(coarse_basis),n_tilde)*inner(n_tilde,n)*v*dI \
               - eta*v*coarse_basis*dI
    constraint_dict = { "L2" : [ (L2_constraint,"full_element") ], \
                        "H10" : [ (H10_constraint,"full_element") ], \
                        "Nitsche" : [ (Nitsche_constraint,"full_element") ], \
                        "IP" : [ (IP_constraint_int,"full_element"), (IP_constraint_ext,"per_internal_face") ] }
    weak_forms_constraints = WeakFormSet()
    for WF,routine in constraint_dict[projector]:
        weak_forms_constraints.add_routine( WF,routine )

    # Scale interaction term
    def S_full(coarse_basis,vq,ds,dI):
        # Full scale interaction term, without manual elimination
        # coarse_basis was the coarse-scale testfunction, vq was the fine-scale trial (now fine-scale test)
        n = FacetNormal(vq.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        v,q = split(vq)
        return - (  inner(a_vec*coarse_basis,grad(v))*dx+inner(kappa*grad(v),grad(coarse_basis))*dx \
               + 0.5*q*inner(n_tilde,n)*coarse_basis*dI - 0.5*inner(kappa*grad(coarse_basis),n_tilde)*inner(n_tilde,n)*v*dI \
               + q*coarse_basis*ds - inner(kappa*grad(coarse_basis),n)*v*ds   )
    def S_full_ext(coarse_basis,vq,dI):
        # Full scale interaction term, without manual elimination
        # coarse_basis was the coarse-scale testfunction, vq was the fine-scale trial (now fine-scale test)
        n = FacetNormal(vq.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        v,q = split(vq)
        return -(  0.5*q*inner(n_tilde,n)*coarse_basis*dI + 0.5*inner(kappa*grad(coarse_basis),n_tilde)*inner(n_tilde,n)*v*dI  )
    def S_adv(coarse_basis,vq,ds,dI):
        # Advective scale interaction term, after manual elimination of the diffusive terms
        # coarse_basis was the coarse-scale testfunction, vq was the fine-scale trial (now fine-scale test)
        v,q = split(vq)
        return -inner(a_vec*coarse_basis,grad(v))*dx
    scale_interaction_dict = { "FE" : [ (S_adv,"full_element") ], \
                               "CS" : [ (S_full,"full_element"),(S_full_ext,"per_internal_face") ] }
    weak_forms_scale_interaction = WeakFormSet()
    for WF,routine in scale_interaction_dict[scale_interaction_mode]:
        weak_forms_scale_interaction.add_routine( WF,routine )

    # Scale interaction term acting on coarse-scale trial
    def B_u_g(coarse_basis,gl,ds,dI):
        # B( u^h , g' )
        # coarse_basis is the trial function, gl is the testfunction (fine-scale closure function)
        n = FacetNormal(gl.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        g,l = split(gl)
        return inner(a_vec*g,grad(coarse_basis))*dx+inner(kappa*grad(g),grad(coarse_basis))*dx \
               + 0.5*l*inner(n_tilde,n)*coarse_basis*dI - 0.5*inner(kappa*grad(coarse_basis),n_tilde)*inner(n_tilde,n)*g*dI \
               + l*coarse_basis*ds - inner(kappa*grad(coarse_basis),n)*g*ds
    def B_u_g_ext(coarse_basis,gl,dI):
        # B( u^h , g' )
        # coarse_basis is the trial function, gl is the testfunction (fine-scale closure function)
        n = FacetNormal(gl.function_space().mesh())
        n_tilde = n*abs(inner(n,nn))/inner(n,nn)
        g,l = split(gl)
        return 0.5*l*inner(n_tilde,n)*coarse_basis*dI + 0.5*inner(kappa*grad(coarse_basis),n_tilde)*inner(n_tilde,n)*g*dI 
    weak_forms_full_scale_interaction = WeakFormSet()
    weak_forms_full_scale_interaction.add_routine( B_u_g,"full_element" )
    weak_forms_full_scale_interaction.add_routine( B_u_g_ext,"per_internal_face" )

    # Projector constraints on the coarse-scale space
    def L2_constraint_coarse_scale(u,v):
        return u*v*dx
    def H10_constraint_coarse_scale(u,v):
        return inner(grad(u),grad(v))*dx
    def Nitsche_constraint_coarse_scale(u,v):
        n = FacetNormal(v.function_space().mesh())
        return inner(grad(u),grad(v))*dx \
               - inner(grad(v),n)*u*ds - inner(grad(u),n)*v*ds \
               + eta_bdy*v*u*ds
    def IP_constraint_coarse_scale(u,v):
        n = FacetNormal(v.function_space().mesh())
        return inner(grad(u),grad(v))*dx \
               - inner(jump(u,n),avg(grad(v)))*dS - inner(jump(v,n),avg(grad(u)))*dS \
               + eta*inner(jump(u,n),jump(v,n))*dS \
               - inner(grad(v),n)*u*ds - inner(grad(u),n)*v*ds \
               + eta_bdy*v*u*ds
    projector_coarse_scale_dict = { "L2" : L2_constraint_coarse_scale, \
                                    "H10" : H10_constraint_coarse_scale, \
                                    "Nitsche" : Nitsche_constraint_coarse_scale, \
                                    "IP" : IP_constraint_coarse_scale }
    weak_forms_projector_coarse_scale = projector_coarse_scale_dict[projector]

    # Finite element weak formulation without scale interaction
    def weak_form_coarse_scale(u,v):
        n = FacetNormal(v.function_space().mesh())
        a_CS = inner(a_vec*v,grad(u))*dx+inner(kappa*grad(u),grad(v))*dx \
                 - inner(avg(kappa*grad(u)),nn)*inner(jump(v,n),nn)*dS - inner(avg(kappa*grad(v)),nn)*inner(jump(u,n),nn)*dS \
                 - inner(kappa*grad(u),n)*v*ds - inner(kappa*grad(v),n)*u*ds
        if scale_interaction_mode == "FE":
            a_CS += kappa*eta*inner(jump(u,n),jump(v,n))*dS + kappa*eta_bdy*u*v*ds
        return a_CS
    def weak_form_coarse_scale_force(v):
        return Constant(1)*v*dx

    # VMS problem
    VMS_problem = VMSProblem(connector)
    VMS_problem.set_bilinear_form_full_scale(weak_forms_full_scale)
    VMS_problem.set_linear_form_full_scale(weak_forms_force)
    VMS_problem.set_bilinear_form_constraints(weak_forms_constraints)
    VMS_problem.set_linear_form_scale_interaction(weak_forms_scale_interaction)
    VMS_problem.set_bilinear_form_projector_coarse_scales(weak_forms_projector_coarse_scale)
    VMS_problem.set_bilinear_form_coarse_scale(weak_form_coarse_scale)
    VMS_problem.set_linear_form_coarse_scale(weak_form_coarse_scale_force)
    VMS_problem.set_bilinear_form_full_scale_interaction(weak_forms_full_scale_interaction)
    VMS_problem.set_boundary_condition(BCset)

    VMS_problem.build_all()

    return VMS_problem
    
