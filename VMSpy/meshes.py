from dolfin import *
from mshr import *

def CONFIRM_IMPORT_MESHES():
    return "confirmed"

def get_unit_triangular_points(h,which='lower'):
    """
    Returns a list of dolfin 'Points' of upper or lower /-slanted
    triangle of size h.
    """
    if which=="lower":
        return [Point(0,0),Point(h,0),Point(h,h)]
    if which=="upper":
        return [Point(0,0),Point(h,h),Point(0,h)]

def get_triangular_domain(pts):
    """
    Returns a msh 'Polygon' defined by the list of dolfin 'Points'.
    """
    return Polygon(pts)

def get_unit_triangle_mesh_from_pts(pts):
    """
    Returns 1-element mesh of triangle defined by list of 'Points'.
    """
    mesh = UnitTriangleMesh.create()
    for i,pt in enumerate(pts):
        mesh.coordinates()[i] = pt.array()[:2]
    return mesh
    
def get_triangle_mesh_from_pts(pts,refinement=1):
    """
    Returns uniformly 2^refinement mesh of triangle defined by list
    of dolfin 'Points'.
    """
    mesh = get_unit_triangle_mesh_from_pts(pts)
    for ref in range(refinement):
        mesh = refine(mesh)
    return mesh
    
def get_triangle_mesh(h,which='lower',refinement=1):
    """
    Returns uniformly 2^refinement mesh of upper or lower /-slanted triangle.
    """
    pts = get_unit_triangular_points(h,which)
    mesh = get_unit_triangle_mesh_from_pts(pts)
    for ref in range(refinement):
        mesh = refine(mesh)
    return mesh

def get_one_dimensional_mesh(width, refinement=1):
    """
    Returns 1D width-wide uniform mesh with refinement-elements.
    """
    els = 1 if refinement == 0 else refinement
    return IntervalMesh(els, 0, width)
