from dolfin import *
import numpy as np
from .verboseprint import *
from petsc4py import PETSc
import resource

def CONFIRM_IMPORT_ASSEMBLY():
    return "confirmed"

def solve_system(A,b,solver="mumps"):
    """
    Returns the PETScVector solution vector to the system Ax=b.
    """
    x = create_empty_vector(b.size())
    solve(A,x,b,solver)
    return x

def create_empty_matrix(size, preallocation_nnz=None):
    """
    Returns a dolfin PETScMatrix of (size x size). Uses default 'setUp'
    preallocation and assembly if 'preallocation_nnz == None'. Other
    options are:

    setPreallocationNNz: int  to set the average nonzeros per row
    setPreallocationNNz: (int,int)  to set the average nonzeros per row for diagonal/offdiagonal sections
    setPreallocationNNz: array int  to set the nonzeros per specific row
    setPreallocationNNz: (array int,array int) to sets the nonzeros per specific row for diagonal/offdiagonal sections
    """
    A = PETSc.Mat()
    A.create()
    A.setSizes(size)
    A.setType('aij')
    
    if type(preallocation_nnz) == type(None):
        A.setUp()
        A.assemble()
    else:
        A.setPreallocationNNZ(preallocation_nnz)
        A.setOption(PETSc.Mat.Option.NEW_NONZERO_ALLOCATION_ERR, False)
        
    return PETScMatrix(A)

def create_empty_vector(size):
    """
    Returns a dolfin PETScVector of (size x size).
    """
    b = PETSc.Vec().create(comm=PETSc.COMM_WORLD)
    b.setSizes(size)
    b.setUp()
    b.assemble()
    return PETScVector(b)

def apply_boundary_condition(A,dofs):
    """
    Removes the values from the rows in dofs, and sets the diagonal to 1.
    This operation may require new allocations (on diagonal) and requires
    assembly operations. For optimal performance, it is called only once
    and called on a non-assembled matrix.

    A : PETScMatrix
    dofs: int (unadvices) or array ints
    """
    if type(dofs) == int:
        dofs = [dofs]
    size = A.size(0)
    for row in dofs:
        # Dummy operation, to make sure (row,row) is allocated before first assembly
        A.mat().setValue(row,row,1)
    A.mat().assemble() # Required for zeroRows
    A.mat().zeroRows(dofs) # Also sets diagonal to 1

def place_block_diagonal_matrix(Me,M,mapping=0, assemble = True):
    """
    Places a matrix 'Me' into 'M' at the diagonal index 'mapping'.
    Matrix 'M' is assembled (in a PETSc sense) if 'assemble == True'.
    
    Me: PETScMatrix
    M: PETScMatrix
    mapping: int
    assemble: optional Boolean
    
    Returns: None
    """
    Msize = M.size(0)
    rows_ind,cols,vals = Me.mat().getValuesCSR()
    cols_to = cols + mapping
    rows_ind_to = np.ones(Msize+1,dtype=np.int32)*len(vals)
    rows_ind_to[0:mapping] = 0
    rows_ind_to[mapping:mapping+len(rows_ind)-1] = rows_ind[0:-1]
    M.mat().setValuesCSR(rows_ind_to, cols_to, vals, addv=PETSc.InsertMode.ADD)
    if assemble:
        M.mat().assemble()
        
def assemble_matrices(get_Me,M,repeats,get_mapping, assemble = True):
    """
    Repeats the operation of placing a matrix 'Me' into 'M' 'repeats' many
    times at a diagonal index 'get_mapping(i)'. 'get_Me' can either be the
    matrix 'Me' itself or functions that output 'Me' for the repeat-index.
    Matrix 'M' is subsequently assembled (in a PETSc sense) after all
    placements if 'assemble == True'.
    
    get_Me: PETScMatrix or function(int i)->PETScMatrix
    M: PETScMatrix
    repeats: int
    get_mapping: function(int i)->int
    assemble: optional Boolean
    
    Returns: None
    """
    if type(get_Me) != type(lambda x: x):
        get_Me = lambda x, Me=get_Me: Me
    for el in range(repeats):
        Me = get_Me(el)
        mapping = get_mapping(el)
        place_block_diagonal_matrix(Me,M,mapping, assemble = False)
    if assemble:
        M.mat().assemble()

def place_submatrix(Me,M,from_block,to_block, assemble = True):
    """
    Places a submatrix out of 'Me' into 'M'. The rows and columns of the
    submatrix are those in from_blocks[0] and from_blocks[1] respectively.
    Matrix 'M' is assembled (in a PETSc sense) if 'assemble == True'.
    
    Me: PETScMatrix
    M: PETScMatrix
    from_block: 2d-tuple interables of integers (or np.int32)
    to_block: 2d-tuple interables of integers (or np.int32)
    assemble: optional Boolean
    
    Returns: None
    """
##    #Seems more efficient but can't get to work:
##    fromrows = PETSc.IS().createGeneral(from_block[0], Me.mat().getComm())
##    fromcols = PETSc.IS().createGeneral(from_block[1], Me.mat().getComm())
##    Me_sub = Me.mat().getLocalSubMatrix(fromrows,fromcols)
##    torows = PETSc.IS().createGeneral(to_block[0])
##    tocols = PETSc.IS().createGeneral(to_block[1])
##    M_sub = Me.mat().getLocalSubMatrix(torows,tocols)
##    rows_ind,cols,vals = Me_sub.getValuesCSR()
##    M_sub.setValuesCSR(rows_ind, cols, vals, addv=PETSc.InsertMode.ADD)

    # Second idea that avoids getValues on zero entries, but is slow due to the where and in1d:
    # for i,r in enumerate(from_block[0]):
    #     nz,vals = Me.mat().getRow(r)
    #     val_inds = np.where( np.in1d(nz, from_block[1]) )[0]
    #     col_inds = np.where( np.in1d(from_block[1], nz) )[0]
    #     M.mat().setValues( to_block[0][i],to_block[1][col_inds],vals[val_inds], addv=PETSc.InsertMode.ADD)
        
    # Currently very crude implementation of sparse submatrix copying
    vals = Me.mat().getValues(from_block[0],from_block[1])
    for i,col in enumerate(to_block[1]):
        place_vector_in_matrix(vals[:,i],M, col,to_block[0], assemble=False)
    if assemble:
        M.mat().assemble()
        
def assemble_submatrices(get_Me,M,repeats,get_mapping, assemble = True):
    """
    Repeats the operation of places a submatrix 'Me' into 'M' 'repeats'
    many times. The from_block that defines the submatrix and the to_block
    are obtained from get_mapping(i). 'get_Me' can either be the matrix
    'Me' itself or functions that output 'Me' for the repeat-index.
    Matrix 'M' is subsequently assembled (in a PETSc sense) after all
    placements if 'assemble == True'.
    
    get_Me: PETScMatrix or function(int i)->PETScMatrix
    M: PETScMatrix
    repeats: int
    get_mapping: function(int i)-> (from_block[0],from_block[1]),(to_block[0],to_block[1])
    assemble: optional Boolean
    
    Returns: None
    """
    if type(get_Me) != type(lambda x: x):
        get_Me = lambda x, Me=get_Me: Me
    for el in range(repeats):
        Me = get_Me(el)
        from_block,to_block = get_mapping(el)
        place_submatrix(Me,M,from_block,to_block, assemble = False)
    if assemble:
        M.mat().assemble()
        
def place_rows(Me,M,from_rows,to_rows, col_offset, assemble = True):
    """
    Places rows in from_rows of 'Me' into the rows in to_rows of 'M',
    offset by 'col_offset' columns. This is a wrapper of 'place_submatrix'.
    Matrix 'M' is assembled (in a PETSc sense) if 'assemble == True'.
    
    Me: PETScMatrix
    M: PETScMatrix
    from_rows: int or np.ndarray
    to_rows: int or np.ndarray
    col_offset: int
    assemble: optional Boolean
    
    Returns: None
    """
    if (not type(from_rows) == np.ndarray) or (not type(to_rows) == np.ndarray):
        from_rows = np.array([from_rows],dtype=np.int32)
        to_rows = np.array([to_rows],dtype=np.int32)
    full_size = np.arange(Me.size(0),dtype=np.int32)
    place_submatrix(Me,M,(from_rows,full_size),(to_rows,full_size+col_offset),assemble=assemble)
    
def assemble_rows(get_Me,M,repeats,get_mapping, assemble = True):
    """
    Repeats the operation of placing rows of 'Me' into 'M' 'repeats'
    many times. The 'from_rows', 'to_rows' and 'col_offset' are obtained
    from 'get_mapping(i)'. 'get_Me' can either be the matrix 'Me'
    itself or functions that output 'Me' for the repeat-index.
    Matrix 'M' is subsequently assembled (in a PETSc sense) after all
    placements if 'assemble == True'.
    
    get_Me: PETScMatrix or function(int i)->PETScMatrix
    M: PETScMatrix
    repeats: int
    get_mapping: function(int i)-> (np.ndarray/int from_rows, np.ndarray/int to_rows, int col_offset)
    assemble: optional Boolean
    
    Returns: None
    """
    if type(get_Me) != type(lambda x: x):
        get_Me = lambda x, Me=get_Me: Me
    for el in range(repeats):
        Me = get_Me(el)
        from_rows,to_rows,col_offset = get_mapping(el)
        place_rows(Me,M,from_rows,to_rows,col_offset, assemble = False)
    if assemble:
        M.mat().assemble()
    
def place_columns(Me,M,from_cols,to_cols,row_offset, assemble = True):
    """
    Places columns in 'from_cols' of 'Me' into the columns in 'to_cols' of 'M',
    offset by 'row_offset' columns. This is a wrapper of 'place_submatrix'.
    Matrix 'M' is assembled (in a PETSc sense) if assemble == True.
    
    Me: PETScMatrix
    M: PETScMatrix
    from_cols: int or np.ndarray
    to_cols: int or np.ndarray
    row_offset: int
    assemble: optional Boolean
    
    Returns: None
    """
    if (not type(from_cols) == np.ndarray) or (not type(to_cols) == np.ndarray):
        from_cols = np.array(from_cols)
        to_cols = np.array(to_cols)
    full_size = np.arange(Me.size(0),dtype=np.int32)
    place_submatrix(Me,M,(full_size,from_cols),(full_size+row_offset,to_cols),assemble=assemble)
    
def assemble_columns(get_Me,M,repeats,get_mapping, assemble = True):
    """
    Repeats the operation of placing columns of 'Me' into 'M' 'repeats'
    many times. The 'from_cols', 'to_cols' and 'row_offset' are obtained
    from 'get_mapping(i)'. 'get_Me' can either be the matrix 'Me' itself
    or functions that output 'Me' for the repeat-index.
    Matrix 'M' is subsequently assembled (in a PETSc sense) after all
    placements if 'assemble == True'.
    
    get_Me: PETScMatrix or function(int i)->PETScMatrix
    M: PETScMatrix
    repeats: int
    get_mapping: function(int i)-> (np.ndarray/int from_cols, np.ndarray/int to_cols, int row_offset)
    assemble: optional Boolean
    
    Returns: None
    """
    if type(get_Me) != type(lambda x: x):
        get_Me = lambda x, Me=get_Me: Me
    for el in range(repeats):
        Me = get_Me(el)
        from_cols,to_cols,row_offset = get_mapping(el)
        place_columns(Me,M,from_cols,to_cols,row_offset, assemble = False)
    if assemble:
        M.mat().assemble()
    
def place_vector_in_matrix(Ve,M,to_col,to_row,transpose=False, assemble = True):
    """
    Places the PETScVector Ve in 'M' at the location (to_col,to_row). Either as
    a column vector ('transpose==False') or as a row vector ('transpose==True').
    Matrix 'M' is assembled (in a PETSc sense) if 'assemble == True'.
    
    Ve: PETScVector
    M: PETScMatrix
    to_col: int
    to_row: int
    transpose: Boolean that dictates column (False) or row (True) placement
    assemble: optional Boolean
    
    Returns: None
    """
    to_col = np.asarray( to_col, np.int32)
    to_row = np.asarray( to_row, np.int32)
    
    nonzeros = np.nonzero(Ve[:])[0]
    vals = Ve[nonzeros]
    
    if transpose:
        if to_col.size == 1:
            size_array = np.arange(len(Ve[:]),dtype=np.int32)
            to_col = size_array + to_col
        to_col = to_col[nonzeros]
    else:
        if to_row.size == 1:
            size_array = np.arange(len(Ve[:]),dtype=np.int32)
            to_row = size_array + to_row
        to_row = to_row[nonzeros]
    M.mat().setValues(to_row,to_col, vals, addv=PETSc.InsertMode.ADD)
    if assemble:
        M.mat().assemble()
        
def place_vector(Ve,V,mapping=0, assemble = True):
    """
    Places 'Ve' into 'V' at the location 'mapping'.
    Vector 'V' is assembled (in a PETSc sense) if 'assemble == True'.
    
    Ve: PETScVector
    V: PETScVector
    mapping: int
    assemble: optional Boolean
    
    Returns: None
    """
    nonz = np.asarray( np.nonzero(Ve[:])[0] ,dtype=np.int32)
    if len(nonz)==1:
        nonz = nonz[0] # To avoid error message
    nonzvals = Ve[nonz]
    if np.asarray(mapping).size > 1:
        nonz = mapping[nonz]
    else:
        nonz += mapping
    V.vec().setValues(nonz, nonzvals, addv=PETSc.InsertMode.ADD)
    if assemble:
        V.vec().assemble()

def assemble_vectors(get_Ve,V,repeats,get_mapping, assemble = True):
    """
    Repeats the operation of placing 'Ve' into 'V' 'repeats' many times.
    The 'mapping' offset isobtained from 'get_mapping(i)'. 'get_Ve' can
    either be the vector 'Ve' itself or a function that output 'Ve' for
    the repeat-index.
    Vector 'V' is subsequently assembled (in a PETSc sense) after all
    placements if 'assemble == True'.
    
    get_Ve: PETScVector or function(int i)->PETScVector
    V: PETScVector
    repeats: int
    get_mapping: function(int i)-> int
    assemble: optional Boolean
    
    Returns: None
    """
    if type(get_Ve) != type(lambda x: x):
        get_Ve = lambda x, Ve=get_Ve: Ve
    for el in range(repeats):
        Ve = get_Ve(el)
        mapping = get_mapping(el)
        place_vector(Ve,V,mapping, assemble = False)
    if assemble:
        V.vec().assemble()
