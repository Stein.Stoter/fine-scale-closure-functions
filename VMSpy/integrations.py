from dolfin import *
import numpy as np
from scipy import sparse
from .dofnumbering import get_compression_map
from .assembly import create_empty_matrix
from petsc4py import PETSc

def CONFIRM_IMPORT_INTEGRATIONS():
    return "confirmed"

def integrate_fine_scale_term(weak_form,W,bdy_spaces = []):
    """
    Returns the PETScMatrix of the weak_form acting on W, after removing the
    interior nodes from the subspaces in bdy_spaces.
    """
    u = TrialFunction(W)
    v = TestFunction(W)
    M = assemble( weak_form(u,v) )
    if not bdy_spaces == []:
        comp_map = get_compression_map(W,bdy_spaces)
        if type(M) == Matrix:
            M = compress_matrix(M,comp_map)
        else:
            M = compress_vector(M,comp_map)
    return as_backend_type(M)

def integrate_constraint_terms(weak_form,coarse_bases,W, bdy_spaces = []):
    """
    Returns the list of PETScVectors of the weak_form acting on WxB, for
    each B in the coarse_bases list of FEniCS Functions.
    """
    if not bdy_spaces == []:
        comp_map = get_compression_map(W,bdy_spaces)
    v = TestFunction(W)
    C = []
    for B in coarse_bases:
        Vec = as_backend_type( assemble( weak_form(v,B) ) )
        if not bdy_spaces == []:
            Vec = compress_vector(Vec,comp_map)
        C.append(Vec)
    return C

def integrate_subspace(weak_form,W,subspaces, bdy_spaces = []):
    """
    Returns the PETScMatrix of the weak_form acting on W, after removing all
    dofs not from spaces in 'subspaces' and from all interior nodes from the
    subspaces in bdy_spaces.
    """
    M = integrate_fine_scale_term(weak_form,W)
    comp_map = get_compression_map(W,bdy_spaces,subspaces)
    M = compress_matrix(M,comp_map)
    return as_backend_type(M)

def compress_matrix(M,comp_map,comp_map_y=None):
    """
    Returns the 'comp_map x comp_map_y' indices of matrix 'M'.
    Retains sparsity of the submatrix.
    """
    if type(comp_map_y)==type(None): comp_map_y = comp_map
    Mcom = sparse.csr_matrix( M.array()[np.ix_(comp_map,comp_map_y)] )
    Mcom.eliminate_zeros() #precaution
    I = Mcom.indptr
    J = Mcom.indices
    D = Mcom.data
    shape = Mcom.shape
    M_ = PETSc.Mat().createAIJWithArrays(shape,(I,J,D))
    return PETScMatrix(M_)

def compress_vector(V,comp_map):
    """
    Returns the 'comp_map' indices of vector 'V'.
    """
    Vcom = V[comp_map]
    return Vcom

