from dolfin import *
from .meshes import *

def CONFIRM_IMPORT_FUNCTIONSPACES():
    return "confirmed"

def get_fine_space_triangle(h,element_types,Ps,which="lower",refinement=1):
    """
    Returns FEniCS mixed FunctionSpace for the fine scales for a regular triangle of size 'h'.
    Fine-scale space constructed on O( 2^refinement ) elements.

    h: float
    element_types: list of strings (e.g. ["DG","BDM","CG"])
    Ps: list of ints
    which: "upper" or "lower" for upper or lower /-slanted triangle.
    refinement: int for 2^refinement elements
    """
    mesh = get_triangle_mesh(h,which, refinement)
    return get_fine_space(element_types,Ps,mesh)

def get_coarse_space_triangle(h,element_types,Ps,W_fine,which="lower"):
    """
    Returns FEniCS a tuple of coarse-scale basis functions and their locations for a
    regular triangle of size 'h'.

    h: float
    element_types: list of strings (e.g. ["DG","BDM","CG"])
    Ps: list of ints
    W_fine: FEniCS mixed function space on which coarse scale are represented.
    which: "upper" or "lower" for upper or lower /-slanted triangle.

    returns:
    Bs: list of lists of basis functions for each element_type. Basis functions are
    FEniCS Functions interpolated on the relevant W_file space
    Bs_locs: list of lists of Points.
    """
    mesh_fine = W_fine.mesh()
    mesh_coarse = get_triangle_mesh(h,which, 0)
    Bs = get_coarse_space(element_types,Ps,mesh_coarse,mesh_fine)
    if "BDM" in element_types and which=="upper":
        ind = element_types.index("BDM")
        if Ps[ind]>1:
            print("Warning: pos/neg correction not implemented")
        else:
            # Tested in test_connector
            Bs[ind][0].vector()[:] *= -1
            Bs[ind][1].vector()[:] *= -1
    Bs_locs = get_coarse_space_dof_locs(element_types,Ps,mesh_coarse)
    return Bs,Bs_locs 

def get_fine_space_one_dimensional(h,element_types,Ps,refinement=1):
    """
    Returns FEniCS mixed FunctionSpace for the fine scales for a one-dimensional element
    of size 'h'. Fine-scale space constructed on 'refinement' elements.

    h: float
    element_types: list of strings (e.g. ["DG","CG"])
    Ps: list of ints
    refinement: int
    """
    mesh = get_one_dimensional_mesh(h, refinement)
    return get_fine_space(element_types,Ps,mesh)

def get_coarse_space_one_dimensional(h,element_types,Ps,W_fine):
    """
    Returns a tuple of coarse-scale FEniCS basis functions and their locations for a
    one-dimensional element of size 'h'.

    h: float
    element_types: list of strings (e.g. ["DG","CG"])
    Ps: list of ints
    W_fine: FEniCS mixed function space on which coarse scale are represented.

    returns:
    Bs: list of lists of basis functions for each element_type. Basis functions are
    FEniCS Functions interpolated on the relevant W_file space
    Bs_locs: list of lists of Points.
    """
    mesh_fine = W_fine.mesh()
    mesh_coarse = get_one_dimensional_mesh(h, 0)
    Bs = get_coarse_space(element_types,Ps,mesh_coarse,mesh_fine)
    Bs_locs = get_coarse_space_dof_locs(element_types,Ps,mesh_coarse)
    return Bs,Bs_locs

def get_fine_space(element_types,Ps,mesh_fine):
    """
    Returns the FEniCS mixed fine-scale spaces for the element_types and polynomial orders on the given mesh.
    """
    elements = [ FiniteElement(eltype, mesh_fine.ufl_cell(), P) for (eltype,P) in zip(element_types,Ps) ]
    if len(elements)>1:
        E = MixedElement(*elements)
    else:
        E = elements[0]
    W = FunctionSpace(mesh_fine,E)
    return W

def get_coarse_space(element_types,Ps,mesh_coarse,mesh_fine):
    """
    Returns the list of lists of FEniCS basis Functions for the element_types and
    polynomial orders on the given coarse mesh, interpolated on the fine mesh.
    """
    elements = [ FiniteElement(eltype, mesh_coarse.ufl_cell(), P) for (eltype,P) in zip(element_types,Ps) ]
    B_int = []
    for (eltype,P) in zip(element_types,Ps):
        B = get_individual_basis_functions(mesh_coarse,eltype,P)
        V_fine = FunctionSpace(mesh_fine,eltype,P)
        B_int.append( interpolate_coarse_functions_on_fine_space(B,V_fine) )
    return B_int

def get_coarse_space_dof_locs(element_types,Ps,mesh_coarse):
    """
    Returns the list of lists of Points of the FEniCS basis Functions for the element_types and
    polynomial orders on the given coarse mesh.
    """
    elements = [ FiniteElement(eltype, mesh_coarse.ufl_cell(), P) for (eltype,P) in zip(element_types,Ps) ]
    B_locs = []
    for (eltype,P) in zip(element_types,Ps):
        B = get_individual_basis_functions(mesh_coarse,eltype,P)
        B_locs.append( [Point(loc) for loc in B[0].function_space().tabulate_dof_coordinates() ] )
    return B_locs

def get_individual_basis_functions(mesh,eltype,P):
    """
    Returns the list of lists of FEniCS basis functions of the 'eltype' with polynomial order 'P' on 'mesh'.
    """
    V_coarse = FunctionSpace(mesh,eltype,P)
    B = []
    for i in range(V_coarse.dim()):
        b = Function(V_coarse)
        b.vector()[i] = 1
        B.append( b )
    return B

def interpolate_coarse_functions_on_fine_space(B,V_fine):
    """
    Returns the set of FEniCS Functions in the list B, interpolated on V_fine.
    """
    B_int = []
    for b in B:
        b_int = Function(V_fine)
        b_int.interpolate(b)
        B_int.append( b_int )
    return B_int
