# Fine-scale closure functions

FEniCS based variational multicale (VMS) finite element code.
Mainly aimed at performing coarse-scale / fine-scale decompositions, e.g. for computing fine-scale Green's and fine-scale closure functions.

## ToDo:

### Major

-  Bug-fix for 2D compute_coarse_scale.
-  Create v1.0.0 tag

### Medium

- Add additional elements and meshes
  -  Square mesh
  -  Cubic mesh
  -  \\-slanted triangular mesh
  -  Cross-shaped triangular mesh
