import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.dofnumbering import *
from VMSpy.functionspaces import *
from VMSpy.meshes import get_triangle_mesh
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np

class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_NODENUMBERING(),'confirmed')

class DofNumberingTest(unittest.TestCase):  
    def test_full_size(self):
        h = 0.3
        element_types = ["CG","DG","CG"]
        Ps_fine = [1,0,1]
        Ps_coarse = [1,0,1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=5)
        (B_CG,B_DG,B_l),_ = get_coarse_space_one_dimensional(h,element_types,Ps_coarse,W)
        bdy_spaces = [2]
        size = get_full_size(2, W,bdy_spaces,[B_DG])
        self.assertEqual(size, 2*(6 + 5 + 2 + 1) )

        h = 0.3
        element_types = ["CG","DG"]
        Ps_fine = [1,0]
        Ps_coarse = [1,0]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=0)
        Bs,_ = get_coarse_space_triangle(h,element_types,Ps_coarse,W)
        bdy_spaces = []
        size = get_full_size(2, W,bdy_spaces,Bs)
        self.assertEqual(size, 2*2*(3+1) )
	
    def test_extract_boundary_dofs(self):
        h = 0.1
        mesh_coarse = get_triangle_mesh(h,"upper",0)
        V_coarse_DG = FunctionSpace(mesh_coarse,'DG',0)
        b_dofs_DG = extract_boundary_dofs(V_coarse_DG)
        self.assertEqual( len(b_dofs_DG),  0 )
        
        V_coarse_CG = FunctionSpace(mesh_coarse,'CG',1)
        b_dofs_CG = extract_boundary_dofs(V_coarse_CG)
        self.assertEqual( len(b_dofs_CG),  3 )
        
        mesh_fine = get_triangle_mesh(h,"upper",2) # 3*2^2 boundary facets
        V_fine = FunctionSpace(mesh_fine,'CG',1)
        File("./Output/meshfine.pvd") << mesh_fine
        b_dofs_fine = extract_boundary_dofs(V_fine)
        self.assertEqual( len(b_dofs_fine),  12 )
        
        S_fine = VectorFunctionSpace(mesh_fine,'CG',1)
        b_dofs_S_fine = extract_boundary_dofs(S_fine)
        self.assertEqual( len(b_dofs_S_fine),  24 )
    
        BDM_coarse = FunctionSpace(mesh_coarse,'BDM',1)
        b_dofs_BDM_coarse = extract_boundary_dofs(BDM_coarse)
        self.assertEqual( len(b_dofs_BDM_coarse),  6 )

    def test_compression_map(self):
        h = 0.1
        mesh_coarse = get_triangle_mesh(h,"upper",0)
        V_coarse_DG = FunctionSpace(mesh_coarse,'DG',0)
        compression_map1 = get_compression_map(V_coarse_DG,bdy_spaces=[0])
        self.assertEqual( len(compression_map1) , 0 )
        
        V_coarse_CG = FunctionSpace(mesh_coarse,'CG',1)
        compression_map2 = get_compression_map(V_coarse_CG,bdy_spaces=[0])
        self.assertTrue( np.array_equal(compression_map2, np.array([0,1,2])) )
        
        V_coarse_CG_2 = FunctionSpace(mesh_coarse,'CG',3)
        compression_map3 = get_compression_map(V_coarse_CG_2,bdy_spaces=[0])
        self.assertTrue( len(compression_map3)==9 )

        element_types = ["DG","CG","CG"]
        Ps_fine = [0,1,3]
        Ps_coarse = [0,1,3]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=0)
        Bs,_ = get_coarse_space_triangle(h,element_types,Ps_coarse,W)
        compression_map4 = get_compression_map(W,2)
        self.assertTrue( len(compression_map4) < W.dim() )
        self.assertEqual( len(compression_map4) , 13 )
        for i in range(13):
            #14th dof removed
            self.assertTrue( i in compression_map4 )

        element_types = ["CG","CG"]
        Ps_fine = [1,1]
        Ps_coarse = [1,1]
        W1D = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=3)
        B1Ds,_ = get_coarse_space_one_dimensional(h,element_types,Ps_coarse,W)
        compression_map5 = get_compression_map(W1D,bdy_spaces=[0])
        self.assertEqual(len(compression_map5) , 6 )

    def test_subspace_dofs(self):
        h = 0.3
        element_types = ["CG","DG","CG"]
        Ps_fine = [1,0,1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=5)
        bdy_spaces = [2]

        dofs_CG  = get_subspace_local_dofs(W,0,bdy_spaces)
        dofs_DG  = get_subspace_local_dofs(W,1,bdy_spaces)
        dofs_LMP = get_subspace_local_dofs(W,2,bdy_spaces)

        self.assertEqual( len(dofs_CG) + len(dofs_DG) + len(dofs_LMP) , get_element_size(W,bdy_spaces) )
        self.assertEqual( sum(dofs_CG) + sum(dofs_DG) + sum(dofs_LMP) , sum(range(get_element_size(W,bdy_spaces))) )

    def test_get_local_dofs_and_locs(self):
        h = 0.3
        element_types = ["CG","DG","CG"]
        Ps_fine = [1,0,1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=5)
        bdy_spaces = [2]

        dofs_CG,locs_CG  = get_local_dofs_and_locs(W,0,bdy_spaces)
        dofs_DG,locs_DG  = get_local_dofs_and_locs(W,1,bdy_spaces)
        dofs_LMP,locs_LMP = get_local_dofs_and_locs(W,2,bdy_spaces)

        self.assertEqual( len(dofs_CG) + len(dofs_DG) + len(dofs_LMP) , get_element_size(W,bdy_spaces) )
        self.assertEqual( locs_LMP[0] , h )
        self.assertEqual( locs_LMP[1] , 0 )

if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
