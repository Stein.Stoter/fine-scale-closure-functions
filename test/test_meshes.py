import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.meshes import *
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np

class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_MESHES(),'confirmed')

class LineElement(unittest.TestCase):
    def test_one_dimensional_mesh(self):
        h = 0.3
        mesh = get_one_dimensional_mesh(h)
        self.assertTrue( 0 in [float(pt) for pt in mesh.coordinates()]   )
        self.assertTrue( h in [float(pt) for pt in mesh.coordinates()]   )
        self.assertAlmostEqual( calc_area(mesh), h )
        self.assertEqual( len(mesh.cells()), 1   )
        
        mesh_5els = get_one_dimensional_mesh(h,5)
        self.assertEqual( len(mesh_5els.cells()), 5   )

class TriangularElement(unittest.TestCase):
    def test_get_unit_triangular_points(self):
        h = 0.1
        pts_lower = get_unit_triangular_points(h,'lower')
        self.assertEqual([(pt.x(),pt.y()) for pt in pts_lower], \
                         [(0.0,0.0),(h,0.0),(h,h)] )
        pts_upper = get_unit_triangular_points(2*h,'upper')
        self.assertEqual([(pt.x(),pt.y()) for pt in pts_upper], \
                         [(0.0,0.0),(2*h,2*h),(0.0,2*h)] )
        
    def test_triangular_domains(self):
        h = 0.1
        pts_lower = get_unit_triangular_points(h,'lower')
        domain_lower = get_triangular_domain(pts_lower)
        self.assertTrue(  domain_lower.inside( Point(0.5*h,0.25*h)  )  )
        pts_upper = get_unit_triangular_points(h,'upper')
        domain_upper = get_triangular_domain(pts_upper)
        self.assertTrue(  domain_upper.inside( Point(0.25*h,0.5*h)  )  )
        
        # Boundaries are not inside:
        self.assertFalse( domain_lower.inside( Point(0.5*h,0)  )  )
        self.assertFalse( domain_upper.inside( Point(h,h)  )  )

    def test_unit_triangle_mesh_from_pts(self):
        h = 0.1
        pts_lower = get_unit_triangular_points(h,'lower')
        mesh = get_unit_triangle_mesh_from_pts(pts_lower)
        self.assertTrue( [h,0] in [list(pt) for pt in mesh.coordinates()]   )
        self.assertEqual( len(mesh.cells()), 1 )

        # Check areas:
        pts = [Point(1,1),Point(1,5),Point(-1,1)] # Area of 4
        mesh_coarse_disp = get_unit_triangle_mesh_from_pts(pts)
        self.assertAlmostEqual( calc_area(mesh_coarse_disp) , 4 )

    def test_triangle_mesh(self):
        global mesh
        h = 0.1
        mesh = get_triangle_mesh(h,"upper")
        self.assertTrue( [0,h] in [list(pt) for pt in mesh.coordinates()]   )
        self.assertAlmostEqual( calc_area(mesh), 0.005 )


def calc_area(mesh):
    V = FunctionSpace(mesh,'CG',1)
    v = Function(V)
    v.interpolate(Constant(1))
    return( assemble(v*dx) )
	

if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
