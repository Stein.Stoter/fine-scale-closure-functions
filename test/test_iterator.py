import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.iterator import *
from VMSpy.connector import Connector_1D
from VMSpy.dofnumbering import get_element_size
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np

class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_ITERATOR(),'confirmed')

class IteratorTest(unittest.TestCase):
    def test_assemble(self):
        # Set up problem
        h = 0.3
        element_types = ["CG","DG"]
        Ps_fine = [1,0]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=2)
        bdy_spaces = [0]
        W_size = get_element_size(W,bdy_spaces)
        total_elements = 4
        def weak_formK(uu,vv):
            u_cg,u_dg = split(uu)
            v_cg,v_dg = split(vv)
            return u_cg*v_cg*dx + u_dg*v_dg*dx
        def weak_formF(uu,vv):
            v_cg,v_dg = split(vv)
            return v_dg*dx

        # Manually set the iterator's total number of element and full size
        iterator = Iterator()
        iterator.total_elements = total_elements
        iterator.full_size = W_size*total_elements

        # Assemble the block matrix
        Ke = integrate_fine_scale_term( weak_formK, W, bdy_spaces )
        iterator.Kes = [Ke]
        iterator.assemble_functions = [assemble_matrices]
        iterator.mappings = [lambda n: n*W_size]
        iterator.assemble_K()

        # Assemble the vector
        Fe = integrate_fine_scale_term( weak_formF, W, bdy_spaces )
        Fe_zero = create_empty_vector(Fe.size); 
        iterator.Fes = [ lambda el: Fe if el+1==3 else Fe_zero ]
        iterator.assemble_functions_force = [assemble_vectors]
        iterator.mappings_force = [lambda n: n*W_size]
        iterator.assemble_F()

        # Extract values
        K = iterator.K
        F = iterator.F
        _,_,Kvals = K.mat().getValuesCSR()
        _,_,Kevals = Ke.mat().getValuesCSR()

        self.assertNotEqual( len(Kvals) , total_elements*W_size*W_size ) #Checking sparsity
        self.assertAlmostEqual( sum(sum(K.array())) , sum(sum(Ke.array()))*total_elements )
        self.assertAlmostEqual( sum(F[:]) , 0.5*h*h )
        self.assertAlmostEqual( sum(F[0:2*W_size]**2)+sum(F[3*W_size:]**2) , 0 )

    def test_apply_boundary_condition(self):
        width = 1
        coarse_elements = 3
        fine_elements = 3
        connector = Connector_1D(width,coarse_elements)
        lmp_subspace = 1
        connector.create_local_spaces( [('CG',1),('CG',1)],refinement=fine_elements )
        iterator = Iterator(connector)

        iterator.apply_boundary_condition( lambda x: np.where(x[:,0]>0.5*width)[0] , lambda x: np.ones(len(x)) ,on_boundary=False)
        totdofs = 0.5*coarse_elements*(fine_elements+1)
        self.assertAlmostEqual( sum(sum(iterator.K.array())) , totdofs )
        self.assertAlmostEqual( sum(iterator.F[:]) , totdofs )


class IteratorTestSystem(unittest.TestCase):
    def test_assemble(self):
        # Problem formulation
        x0,x1 = 0,1
        coarse_elements = 3
        fine_elements = 3
        def weak_form_internal(ul,vq):
            # All variables are internal
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return inner(v*Constant((1,)),grad(u))*dx + inner( grad(u), grad(v) )*dx \
                   + 0.5*u*q*inner(n,n_tilde)*ds + 0.5*l*v*inner(n,n_tilde)*ds
        def weak_form_lmp_jump_int(ul,vq):
            # All variables are internal
            u,l = split(ul)
            v,q = split(vq)
            return l*q*ds
        def weak_form_cross_lmp(ul,vq):
            # u,v are internal l,q external
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return 0.5*u*q*inner(n,n_tilde)*ds + 0.5*l*v*inner(n,n_tilde)*ds
        def weak_form_lmp_jump_ext(ul,vq):
            # l is internal q is external
            u,l = split(ul)
            v,q = split(vq)
            return -l*q*ds
        weak_forms = [weak_form_internal, weak_form_lmp_jump_int,\
                      weak_form_cross_lmp, weak_form_cross_lmp,\
                      weak_form_cross_lmp, weak_form_cross_lmp,\
                      weak_form_lmp_jump_ext, weak_form_lmp_jump_ext]

        # Connector
        connector = Connector_1D(x1-x0,coarse_elements)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces,refinement=fine_elements )

        # Iterator
        iterator = Iterator(connector)
        iterator.Kes = [integrate_fine_scale_term( WF , connector.W, connector.bdy_spaces ) for WF in weak_forms]
        iterator.assemble_functions = [assemble_matrices, assemble_matrices,\
                                       assemble_columns, assemble_columns,\
                                       assemble_rows, assemble_rows,\
                                       assemble_columns, assemble_columns ]
        iterator.mappings = [connector.get_map_all_dofs,\
                             connector.get_map_all_dofs,\
                             lambda e: connector.get_map_opposing_face_dofs(e,0),\
                             lambda e: connector.get_map_opposing_face_dofs(e,1),\
                             lambda e: connector.get_map_opposing_face_dofs(e,0),\
                             lambda e: connector.get_map_opposing_face_dofs(e,1),\
                             lambda e: connector.get_map_opposing_face_dofs(e,0),\
                             lambda e: connector.get_map_opposing_face_dofs(e,1)]
        

        # Left and right LMP dof number
        lmp_dofs = get_subspace_local_dofs(iterator.W,lmp_subspace,bdy_spaces)
        lmp_dof_left  = int( lmp_dofs[1] )
        lmp_dof_right = int( lmp_dofs[0] + (coarse_elements-1)*iterator.W_size )
        
        # Assemble stiffness
        iterator.assemble_K()
        iterator.K.mat().setValues(lmp_dof_left, lmp_dof_left, 0)
        iterator.K.mat().setValues(lmp_dof_right, lmp_dof_right, 0)
        iterator.K.mat().assemble()

        # Produce force vector
        iterator.assemble_F()
        bdy_val = 1
        place_vector([0.5*bdy_val],iterator.F,mapping=lmp_dof_right)
        
        # Solve
        it = iterator
        iterator.solve()
        U = iterator.U
        
        # Rearange solution vector into fenics standard
        U_rear = rearrange_to_FEniCS(U,connector)
        
        # Compare with FEniCS computation
        U_FEniCS = solve_adv_dif(x0,x1,len(U_rear)-1)
        self.assertTrue( np.allclose( U_rear[:], U_FEniCS[:] )  )
        
class ElementIteratorTestSystem(unittest.TestCase):
    def test_assemble(self):
        # Problem formulation
        x0,x1 = 0,1
        coarse_elements = 15
        fine_elements = 3
        def weak_form_internal(ul,vq,ds,dI):
            # All variables are internal
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return inner(v*Constant((1,)),grad(u))*dx +inner( grad(u), grad(v) )*dx + \
                   + u*q*ds + l*v*ds \
                   + 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI \
                   + l*q*dI
        def weak_form_cross_lmp(ul,vq,dI):
            # u,v are internal l,q external
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI
        def weak_form_lmp_jump_ext(ul,vq,dI):
            # l is internal q is external
            u,l = split(ul)
            v,q = split(vq)
            return -l*q*dI
        weak_forms = [weak_form_internal,
                      weak_form_cross_lmp,
                      weak_form_cross_lmp,
                      weak_form_lmp_jump_ext]

        # Connector
        connector = Connector_1D(x1-x0,coarse_elements)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces,refinement=fine_elements )

        # Iterator
        iterator = ElementIterator(connector)
        iterator.Kes = weak_forms
        iterator.element_routines = [ "full_element",
                                      "per_internal_face_column",
                                      "per_internal_face_row",
                                      "per_internal_face_column"]
        
        # Assemble stiffness
        iterator.assemble_K()

        # Produce force vector
        iterator.assemble_F()
        lmp_dofs = get_subspace_local_dofs(iterator.W,lmp_subspace,bdy_spaces)
        lmp_dof_left  = int( lmp_dofs[1] )
        lmp_dof_right = int( lmp_dofs[0] + (coarse_elements-1)*iterator.W_size )
        bdy_val = 1
        place_vector([bdy_val],iterator.F,mapping=lmp_dof_right)
        
        # Solve
        iterator.solve()
        U = iterator.U
        
        # Rearange solution vector into fenics standard
        U_rear = rearrange_to_FEniCS(U,connector)
        
        # Compare with FEniCS computation
        U_FEniCS = solve_adv_dif(x0,x1,len(U_rear)-1)
        self.assertTrue( np.allclose( U_rear[:], U_FEniCS[:] )  )

    def test_assemble_K_coarse_scale_constraints(self):
        # Problem formulation
        width = 2.3
        coarse_elements = 3
        fine_elements = 3
        def coarse_constraint(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return v*coarse_basis*dx
        def coarse_constraint_ds(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return v*coarse_basis*ds
        def coarse_constraint_coupled_dI(coarse_basis,vq,dI):
            v,q = split(vq)
            return q*coarse_basis*dI
        
        # Connector
        connector = Connector_2D_triangular(width,coarse_elements)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces,refinement=fine_elements )
        connector.create_coarse_spaces([('DG',0)] )

        # Element internal integrals
        iterator = VMSIterator(connector)
        iterator.Ces = [coarse_constraint,coarse_constraint_ds]
        iterator.Ce_element_routines = ["full_element","full_element"]
        iterator.assemble_K_coarse_scale_constraints()
        full_W_size = iterator.total_elements*iterator.W_size
        self.assertTrue( not np.any(iterator.K.array()[0:full_W_size,0:full_W_size]) )
        self.assertTrue( not np.any(iterator.K.array()[full_W_size:-1,full_W_size:-1]) )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[:,col]) for col in range(full_W_size,iterator.full_size)])  )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[row,:]) for row in range(full_W_size,iterator.full_size)])  )
        self.assertAlmostEqual(sum(sum(iterator.K.array())), 2*width**2 + 2*(4*width))
        
        # Coupling integrals
        iterator.Ces = [coarse_constraint_coupled_dI]
        iterator.Ce_element_routines = ["per_internal_face"]
        iterator.assemble_K_coarse_scale_constraints(empty=True)
        self.assertTrue( not np.any(iterator.K.array()[0:full_W_size,0:full_W_size]) )
        self.assertTrue( not np.any(iterator.K.array()[full_W_size:,full_W_size:]) )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[:,col]) for col in range(full_W_size,iterator.full_size)])  )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[row,:]) for row in range(full_W_size,iterator.full_size)])  )
        self.assertAlmostEqual(sum(sum(iterator.K.array())), 2*2*( 4*width + 3*sqrt(2*width**2) ) )
        
        # For a continuous basis
        iterator.full_size = 0
        connector.create_coarse_spaces([('RT',1),('CG',1)] )
        iterator.Ces = [coarse_constraint,coarse_constraint_ds,coarse_constraint_coupled_dI]
        iterator.Ce_spaces = [1,1,1]
        iterator.Ce_element_routines = ["full_element","full_element","per_internal_face"]
        iterator.assemble_K_coarse_scale_constraints(empty=True)
        full_W_RT_size = iterator.total_elements*iterator.W_size + iterator.connector.coarse_spaces[0].dim()
        self.assertTrue( not np.any(iterator.K.array()[0:full_W_size,0:full_W_RT_size]) )
        self.assertTrue( not np.any(iterator.K.array()[full_W_RT_size:-1,full_W_RT_size:-1]) )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[:,col]) for col in range(full_W_RT_size,iterator.full_size)])  )
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[row,:]) for row in range(full_W_RT_size,iterator.full_size)])  )
        #self.assertAlmostEqual(sum(sum(iterator.K.array())),  2*width**2 + 2*(4*width) \
        #                                                      + 2*2*( 4*width + 3*sqrt(2*width**2) ) )
        
        # Remove constraints by constraining the coarse-scale dof
        def bdy_inside(x):
            return near( x[0], 0 ) or near( x[0], width ) or near( x[1], 0 ) or near( x[1], width )
        def dom_inside(x):
            return True
        iterator.eliminate_coarse_scale_constraints(bdy_inside,CS_space_nr = 0)
        iterator.eliminate_coarse_scale_constraints(dom_inside,CS_space_nr = 1)
        self.assertTrue( np.alltrue( [ np.any(iterator.K.array()[row,:]) for row in range(full_W_RT_size,iterator.full_size)])    )
        self.assertEqual(sum(sum(iterator.K.array()[full_W_size:full_W_RT_size,:])), coarse_elements*4)
        self.assertEqual(sum(sum(iterator.K.array()[full_W_RT_size:,:])), iterator.connector.coarse_spaces[1].dim() )

    def test_contructors(self):
        # Problem formulation
        width = 2.3
        coarse_elements = 3
        fine_elements = 3
        def weak_form_internal(ul,vq, ds,dI):
            u,l = split(ul)
            v,q = split(vq)
            return inner( grad(u), grad(v) )*dx
        def coarse_constraint(coarse_basis,vq, ds,dI):
            v,q = split(vq)
            return v*coarse_basis*dx
        
        # Connector
        connector = Connector_2D_triangular(width,coarse_elements)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces,refinement=fine_elements )
        connector.create_coarse_spaces([('DG',0)] )

        # Element iterator
        iterator = ElementIterator(connector)
        iterator.Kes = [weak_form_internal]
        iterator.element_routines = [ "full_element"]
        iterator.assemble_K()

        # Extend to VMS iterator
        vms_iterator = VMSIterator(iterator)
        vms_iterator.assemble_K()
        vms_iterator.Ces = [coarse_constraint]
        vms_iterator.Ce_element_routines = ["full_element"]
        vms_iterator.assemble_K_coarse_scale_constraints()

        # Element iterator again
        iterator2 = ElementIterator(vms_iterator)
        iterator2.K.mat().assemble()
        
        # Test back and forth constructors:
        self.assertTrue( np.sum(np.sum( abs(iterator.K.array())))> 0.001  ) # Check non-empty
        self.assertTrue( np.allclose( iterator.K.array(), iterator2.K.array()  ))

        
    def test_assemble_F_scale_interaction(self):
        # Problem formulation
        width = 2.3
        coarse_elements = 3
        fine_elements = 3
        def S(coarse_basis,vq,ds,dI):
            v,q = split(vq)
            return v*coarse_basis*dx
        def S_bdy(coarse_basis,vq,dI):
            v,q = split(vq)
            return q*coarse_basis*dI
        
        # Connector
        connector = Connector_2D_triangular(width,coarse_elements)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces,refinement=fine_elements )
        connector.create_coarse_spaces([('CG',1)] )

        # Get dofs from locations
        #locs = connector.coarse_spaces[0].tabulate_dof_coordinates()
        loc1 = np.array([0,2.3]) # Upper left corner dof
        dof1 = connector.get_coarse_dof_from_loc( loc1 ) 
        loc2 = np.array([0,0]) # Lower left corner dof
        dof2 = connector.get_coarse_dof_from_loc( loc2 ) 

        # Element internal integrals
        iterator = VMSIterator(connector)
        iterator.Ses = [S]
        iterator.Se_element_routines = ["full_element"]
        iterator.assemble_F_scale_interaction( dof1 )
        self.assertAlmostEqual( sum(iterator.F[:]) , 1/6*(width/coarse_elements)**2 )     
           
        iterator.assemble_F_scale_interaction( dof2, empty=False )
        self.assertAlmostEqual( sum(iterator.F[:]) , 0.5*(width/coarse_elements)**2 )

        iterator.Ses = [S_bdy]
        iterator.Se_element_routines = ["per_internal_face"]
        iterator.Se_spaces = [0] # Only necessary if multiple spaces exist
        iterator.assemble_F_scale_interaction( dof2 )
        self.assertAlmostEqual( sum(iterator.F[:]) , 2*0.5*np.sqrt(2)*width/coarse_elements ) # 2* due to CG

# Rearange solution vector to FEniCS standard
def rearrange_to_FEniCS(U,connector):
    rearanged = []
    W_size = connector.W_size
    u_dofs_ = get_subspace_local_dofs(connector.W,0,connector.bdy_spaces)
    for el in reversed( range(connector.total_elements) ):
        subvec = U[el*W_size:(el+1)*W_size]
        nodes = u_dofs_[:]
        relevant_dofs = subvec[u_dofs_]
        if el != 0:
            relevant_dofs = relevant_dofs[:-1]
        rearanged.append( relevant_dofs )
    return np.hstack( rearanged )

# Equivalent 1D FEniCS implementation
def solve_adv_dif(x0,x1,els):
    mesh = IntervalMesh(els,x0,x1)
    V = FunctionSpace(mesh,'CG',1)
    u = TrialFunction(V)
    v = TestFunction(V)
    A = inner(v*Constant((1,)),grad(u))*dx + inner( grad(u), grad(v) )*dx
    bc = DirichletBC( V, Expression('(x[0]-%f)/%f'%(x0,x1-x0),degree=1), lambda x,on_bdy : on_bdy )
    u = Function(V)
    solve( A == Constant(0)*v*dx , u, bc )
    return u.vector()

if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
