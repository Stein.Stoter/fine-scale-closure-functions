import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.assembly import *
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np


class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_ASSEMBLY(),'confirmed')


class MatrixOperations(unittest.TestCase):        
    def test_create_empty_matrix(self):
        A = create_empty_matrix((4,4))
        self.assertEqual( A.array().shape, (4,4) )
        self.assertEqual( np.max(A.array()**2), 0 )
        
        b = create_empty_vector(4)
        self.assertEqual( b[:].shape, (4,) )
        self.assertEqual( np.max(b[:]**2), 0 )

    def test_solve_system(self):
        A = create_empty_matrix((1,1))
        A.mat().setValues([0], [0], [0.5])
        A.mat().assemble()
        b = create_empty_vector(1)
        b[:] = 2
        x = solve_system(A,b)        
        self.assertEqual(x[0] , 4)

    def test_place_block_diagonal_matrix(self):
        A = create_empty_matrix((3,3))
        
        A1 = create_empty_matrix((2,2))
        A1.mat().setValues([0,1], [0,1], np.array([[1,2],[3,4]]))
        A1.mat().assemble()
        
        A2 = create_empty_matrix((2,2))
        A2.mat().setValues([0,1], [0,1], np.array([[5,6],[7,8]]))
        A2.mat().assemble()
        
        A3 = create_empty_matrix((2,2))
        A3.mat().setValues([0,1], [0,1], np.array([[9,10],[10,2]]))
        A3.mat().assemble()
        
        place_block_diagonal_matrix(A1,A)
        self.assertTrue(np.allclose(  A.array(), np.array([[1,2,0],[3,4,0],[0,0,0]])  ))
        place_block_diagonal_matrix(A2,A,1)
        self.assertTrue(np.allclose(  A.array(), np.array([[1,2,0],[3,9,6],[0,7,8]])  ))
        
    def test_assemble_matrices(self):
        A = create_empty_matrix((10,10))
        A1 = create_empty_matrix((2,2))
        A1.mat().setValues([0,1], [0,1], np.array([[1,2],[3,1]]))
        A1.mat().assemble()
        elements = 5
        assemble_matrices(A1,A,elements,lambda n: 2*n)
        _,_,nonzeros = A.mat().getValuesCSR()
        Aarr = A.array()
        
        self.assertEqual( np.sum(Aarr), elements*np.sum(A1.array())  )
        self.assertTrue( Aarr[0,0] == Aarr[1,1] == Aarr[9,9] == 1 )
        self.assertTrue( Aarr[0,1] == Aarr[2,3] == Aarr[8,9] == 2 )
        self.assertTrue( Aarr[1,0] == Aarr[3,2] == Aarr[9,8] == 3 )
        self.assertTrue( len(nonzeros) ==  elements*4)
        
        B = create_empty_matrix((8,8))
        B1 = create_empty_matrix((4,4))
        B1.mat().setValuesCSR([ 0, 1, 2, 3, 4 ] ,[ 0, 1, 2, 1 ],  [ 5, 8, 3, 6 ] )
        B1.mat().assemble()
        elements = 2
        assemble_matrices(B1,B,elements,lambda n: 4*n)
        _,_,nonzeros = B.mat().getValuesCSR()

        self.assertEqual( np.sum(B.array()), elements*np.sum(B1.array())  )
        # Test the block sparsity is not retained
        self.assertNotEqual( len(nonzeros) ,  elements*4*4  )
        self.assertEqual( len(nonzeros) ,  elements*4  )

    def test_assemble_vectors(self):
        V = create_empty_vector(10)
        V1 = create_empty_vector(2)
        V1.vec().setValues([0,1], [1,5])
        V1.vec().assemble()
        elements = 5
        assemble_vectors(V1,V,elements,lambda n: 2*n)
        
        self.assertEqual( np.sum(V[:]), elements*np.sum(V1[:])  )
        self.assertTrue( V[0] == V[2] == V[4] == 1 )
        self.assertTrue( V[1] == V[3] == V[9] == 5  )

    def test_place_submatrix(self):
        A = create_empty_matrix((5,5))
        A1 = create_empty_matrix((3,3))
        A1.mat().setValues([0,1,2], [0,1,2], np.array([[1,2,3],[4,5,6],[7,8,9]]))
        A1.mat().assemble()
        
        full_size = np.arange(A1.size(0),dtype=np.int32)
        from_block = ( np.array([0],dtype=np.int32), full_size )
        to_block = ( np.array([0],dtype=np.int32), full_size )
        place_submatrix(A1,A,from_block,to_block)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[1,2,3,0,0],\
             [0,0,0,0,0],\
             [0,0,0,0,0],\
             [0,0,0,0,0],\
             [0,0,0,0,0]]  ) ) )
        
        from_block = ( full_size, np.array([0,1],dtype=np.int32) )
        to_block = ( full_size+2, np.array([1,3],dtype=np.int32) )
        place_submatrix(A1,A,from_block,to_block)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[1,2,3,0,0],\
             [0,0,0,0,0],\
             [0,1,0,2,0],\
             [0,4,0,5,0],\
             [0,7,0,8,0]]  ) ) )
        
    def test_place_rows_place_cols(self):
        A = create_empty_matrix((5,5))
        A1 = create_empty_matrix((3,3))
        A1.mat().setValues([0,1,2], [0,1,2], np.array([[1,2,3],[4,5,6],[7,8,9]]))
        A1.mat().assemble()

        from_row = 0
        to_row = 3
        col_offset = 2
        place_rows(A1,A,from_row,to_row,col_offset)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,0,0,0,0],\
             [0,0,0,0,0],\
             [0,0,0,0,0],\
             [0,0,1,2,3],\
             [0,0,0,0,0]]  ) ) )
        
        from_rows = np.array([0,2],dtype=np.int32)
        to_rows = np.array([3,0],dtype=np.int32)
        col_offset = 1
        place_rows(A1,A,from_rows,to_rows,col_offset)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,7,8,9,0],\
             [0,0,0,0,0],\
             [0,0,0,0,0],\
             [0,1,3,5,3],\
             [0,0,0,0,0]]  ) ) )

        from_cols,to_cols,row_offset = from_rows,to_rows,col_offset
        place_columns(A1,A,from_cols,to_cols,row_offset)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,7,8,9,0],\
             [3,0,0,1,0],\
             [6,0,0,4,0],\
             [9,1,3,12,3],\
             [0,0,0,0,0]]  )))

    def test_place_vector_in_matrix(self):
        A = create_empty_matrix((5,5))
        V1 = create_empty_vector(4)
        V1.vec().setValues([0,1,2,3], [1,0,2,4])
        V1.vec().assemble()

        to_row = 1
        to_col = 2
        place_vector_in_matrix(V1,A,to_col,to_row)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,0,0,0,0],\
             [0,0,1,0,0],\
             [0,0,0,0,0],\
             [0,0,2,0,0],\
             [0,0,4,0,0]]  ) ) )
        
        place_vector_in_matrix(V1,A,to_row,to_col,transpose=True)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,0,0,0,0],\
             [0,0,1,0,0],\
             [0,1,0,2,4],\
             [0,0,2,0,0],\
             [0,0,4,0,0]]  ) ) )

        to_col = 1
        to_rows = [1,5,0,2]
        place_vector_in_matrix(V1,A,to_col,to_rows)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,2,0,0,0],\
             [0,1,1,0,0],\
             [0,5,0,2,4],\
             [0,0,2,0,0],\
             [0,0,4,0,0]]  ) ) )
        
        place_vector_in_matrix(V1,A,to_rows,to_col,transpose=True)
        self.assertTrue( np.allclose( A.array(), np.array( \
            [[0,2,0,0,0],\
             [2,2,5,0,0],\
             [0,5,0,2,4],\
             [0,0,2,0,0],\
             [0,0,4,0,0]]  ) ) )
        

if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
