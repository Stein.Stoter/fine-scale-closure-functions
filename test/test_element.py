import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.element import *
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np


class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_ELEMENT(),'confirmed')

class ElementTest(unittest.TestCase):
    def test_Element1D(self):
        h = 0.3
        element = Element_1D(h)
        bdy_spaces = [0]
        element.create_local_spaces( ['CG','CG'],[1,2],bdy_spaces )

        all_dofs = get_compression_map(element.W,element.bdy_spaces)
        leftdof_loc  = element.get_internal_face_local_dofs(0)
        rightdof_loc = element.get_internal_face_local_dofs(1)
        leftdof  = all_dofs[leftdof_loc]
        rightdof = all_dofs[rightdof_loc]
        
        self.assertEqual( element.W.tabulate_dof_coordinates()[leftdof] , 0 )
        self.assertEqual( element.W.tabulate_dof_coordinates()[rightdof] , h )

        ds0,dI0 = element.get_integration_boundaries(0) # Internal
        ds1,dI1 = element.get_integration_boundaries(1) # Left
        ds2,dI2 = element.get_integration_boundaries(2) # Right
        ds3,dI3 = element.get_integration_boundaries(3) # Double
        
        self.assertEqual( assemble(Constant(1)*ds0) , 0 )
        self.assertEqual( assemble(Constant(1)*dI0) , 2 )
        self.assertEqual( assemble(Constant(1)*ds1) , 1 )
        self.assertEqual( assemble(Constant(1)*dI1) , 1 )
        self.assertEqual( assemble(Constant(1)*ds2) , 1 )
        self.assertEqual( assemble(Constant(1)*dI2) , 1 )
        self.assertEqual( assemble(Constant(1)*ds3) , 2 )
        self.assertEqual( assemble(Constant(1)*dI3) , 0 )
        
        
    def test_Element2D_triangle(self):
        h = 0.3
        element = Element_2D_triangle(h,"upper")
        bdy_spaces = [0]
        element.create_local_spaces( ['CG','CG'],[1,2],bdy_spaces )

        all_dofs = get_compression_map(element.W,element.bdy_spaces)
        coords = element.W.tabulate_dof_coordinates()
        leftdofs_loc = element.get_internal_face_local_dofs(0)
        leftdofs = all_dofs[leftdofs_loc]
        leftcoords = coords[leftdofs]
        topdofs_loc = element.get_internal_face_local_dofs(1)
        topdofs = all_dofs[topdofs_loc]
        topcoords = coords[topdofs]
        sidedofs_loc = element.get_internal_face_local_dofs(2)
        sidedofs = all_dofs[sidedofs_loc]
        sidecoords = coords[sidedofs]
        self.assertTrue( np.allclose(leftcoords[:,0] , 0) )
        self.assertTrue( np.allclose(topcoords[:,1]  , h) )
        self.assertTrue( np.allclose(sidecoords[:,0] - sidecoords[:,1] , 0) )

        neighbor_element = Element_2D_triangle(h,"lower")
        neighbor_element.create_local_spaces( ['CG','CG'],[1,2],bdy_spaces )

        # DOF connectivity neighbors
        neighbor_all_dofs = get_compression_map(neighbor_element.W,neighbor_element.bdy_spaces)
        neighbor_coords = neighbor_element.W.tabulate_dof_coordinates()
        neighbor_rightdofs_loc = neighbor_element.get_internal_face_local_dofs(0)
        neighbor_rightdofs = neighbor_all_dofs[neighbor_rightdofs_loc]
        neighbor_rightcoords = neighbor_coords[neighbor_rightdofs]
        neighbor_bottomdofs_loc = neighbor_element.get_internal_face_local_dofs(1)
        neighbor_bottomdofs = neighbor_all_dofs[neighbor_bottomdofs_loc]
        neighbor_bottomcoords = neighbor_coords[neighbor_bottomdofs]
        neighbor_sidedofs_loc = neighbor_element.get_internal_face_local_dofs(2)
        neighbor_sidedofs = neighbor_all_dofs[neighbor_sidedofs_loc]
        neighbor_sidecoords = neighbor_coords[neighbor_sidedofs]
        self.assertTrue( np.allclose( leftcoords , neighbor_rightcoords+(-h,0) ) )
        self.assertTrue( np.allclose( topcoords , neighbor_bottomcoords+(0,h) ) )
        self.assertTrue( np.allclose( sidecoords , neighbor_sidecoords ) )
        # Also tests the correct dof-ordering of neighboring elements

        # Integration on facets upper element
        ds0,dI0 = element.get_integration_boundaries(0) # Internal
        ds1,dI1 = element.get_integration_boundaries(1) # Left
        ds2,dI2 = element.get_integration_boundaries(2) # Top
        ds3,dI3 = element.get_integration_boundaries(3) # Corner

        n = FacetNormal(element.W)
        nn = inner(Constant((1,-1)),n) # zero divergence
        self.assertAlmostEqual( assemble(Constant(1)*ds0) , 0 )
        self.assertAlmostEqual( assemble(nn*ds0) , 0 )
        self.assertAlmostEqual( assemble(Constant(1)*dI0) , (2+sqrt(2))*h )
        self.assertAlmostEqual( assemble(nn*dI0) , 0 )
        self.assertAlmostEqual( assemble(nn*ds1) , -h )
        self.assertAlmostEqual( assemble(nn*dI1) , h )
        self.assertAlmostEqual( assemble(nn*ds2) , -h )
        self.assertAlmostEqual( assemble(nn*dI2) , h )
        self.assertAlmostEqual( assemble(nn*ds3) , -2*h )
        self.assertAlmostEqual( assemble(nn*dI3) , 2*h )

        # Integration on facets lower element
        element = Element_2D_triangle(h,"lower")
        element.create_local_spaces( ['CG','CG'],[1,2],bdy_spaces )
        ds0,dI0 = element.get_integration_boundaries(0) # Internal
        ds1,dI1 = element.get_integration_boundaries(1) # Right
        ds2,dI2 = element.get_integration_boundaries(2) # Bottom
        ds3,dI3 = element.get_integration_boundaries(3) # Corner
        
        n = FacetNormal(element.W)
        nn = inner(Constant((1,-1)),n)
        self.assertAlmostEqual( assemble(Constant(1)*ds0) , 0 )
        self.assertAlmostEqual( assemble(nn*ds0) , 0 )
        self.assertAlmostEqual( assemble(Constant(1)*dI0) , (2+sqrt(2))*h )
        self.assertAlmostEqual( assemble(nn*dI0) , 0 )
        self.assertAlmostEqual( assemble(nn*ds1) , h )
        self.assertAlmostEqual( assemble(nn*dI1) , -h )
        self.assertAlmostEqual( assemble(nn*ds2) , h )
        self.assertAlmostEqual( assemble(nn*dI2) , -h )
        self.assertAlmostEqual( assemble(nn*ds3) , 2*h )
        self.assertAlmostEqual( assemble(nn*dI3) , -2*h )

        

if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
