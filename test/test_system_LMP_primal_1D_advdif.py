import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from dolfin import *
from VMSpy import *

"""
Solves:

(a grad u , v )+(grad u, grad v) + (avg(l) n_tilde , jump(v)) = (f,v)
(avg(q) n_tilde, jump(u)) = 0
(jump(q) , jump(l)) = 0

by separating into elements. The fields u and l^in are element local,
and all coupling occurs through l^\pm and q^\pm. Per element:

(a grad u , v )_K + (grad u, grad v)_K
+ (0.5*l^in n_tilde, v^in n^in) + (0.5*l^out n_tilde, v^in n^in)
+ (0.5*q^in n_tilde, u^in n^in) + (0.5*q^out n_tilde, u^in n^in)
(q^in , l^in) - (q^out , l^in)
= (f,v)_K

"""

class LMP1DAdvectionDiffusion(unittest.TestCase):
    def test(self):
        ## Solve a 1D advection-diffusion problem by partitioning and assembling
        ## while coupling the patches with double valued Lagrange multipliers.
        ## Compare pointwise to the solution of standard FEniCS implementation.

        # Problem formulation
        x0,x1 = 0,1
        coarse_elements = 3
        fine_elements = 3
        def weak_form_internal(ul,vq):
            # All variables are internal
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return inner(v*Constant((1,)),grad(u))*dx + inner( grad(u), grad(v) )*dx \
                   + 0.5*u*q*inner(n,n_tilde)*ds + 0.5*l*v*inner(n,n_tilde)*ds
        def weak_form_cross_lmp(ul,vq):
            # u,v are internal l,q external
            n = FacetNormal( ul.function_space().mesh() )
            n_tilde = Constant((1,))
            u,l = split(ul)
            v,q = split(vq)
            return 0.5*u*q*inner(n,n_tilde)*ds + 0.5*l*v*inner(n,n_tilde)*ds
        def weak_form_lmp_jump_ext(ul,vq):
            # l is internal q is external
            u,l = split(ul)
            v,q = split(vq)
            return -l*q*ds
        def weak_form_lmp_jump_int(ul,vq):
            # l is internal q is external
            u,l = split(ul)
            v,q = split(vq)
            return l*q*ds

        # Space on domain partitioning
        h = (x1-x0)/coarse_elements
        element_types = ["CG","CG"]
        bdy_spaces = [1]
        Ps_fine = [1,1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=fine_elements)

        # Sizes
        element_size = get_element_size(W,bdy_spaces)
        complete_size = get_full_size(coarse_elements,W,bdy_spaces)

        # Element local dof numbering
        u_dofs_ = get_subspace_local_dofs(W,0,bdy_spaces)
        def el_to_u_dofs(el):
            return u_dofs_+el*element_size
        lmp_dofs_ = get_subspace_local_dofs(W,1,bdy_spaces)
        def el_to_lmp_dofs(el):
            return lmp_dofs_+el*element_size

        # LMP interconnection
        def get_opposing_LMP_dofnumber(el,lr):
            #local_lmpdof 1 is left local_lmpdof 0 is right
            if lr == 'l':
                return el_to_lmp_dofs(el-1)[0]
            if lr == 'r':
                return el_to_lmp_dofs(el+1)[1]

        # Local Lagrange multiplier dofs to neighboring dofs
        def map_Lagrange_multiplier_external(el):
            # Returns: from_col, to_col, row_offset per element.
            from_col = lmp_dofs_ #order: right, left
            to_col_r = get_opposing_LMP_dofnumber(el,'r')
            to_col_l = get_opposing_LMP_dofnumber(el,'l')
            to_col = np.array([to_col_r,to_col_l],dtype=np.int32)
            row_offset = el*element_size
            if el == 0:
                from_col = from_col[0:1]
                to_col = to_col[0:1]
            if el == coarse_elements-1:
                from_col = from_col[1:2]
                to_col = to_col[1:2]
            return from_col,to_col,row_offset
        def map_Lagrange_multiplier_internal(el):
            # Returns: from_col, to_col, row_offset per element.
            from_block = lmp_dofs_ #order: right, left
            to_block = el_to_lmp_dofs(el)
            if el == 0:
                from_block = from_block[0:1]
                to_block = to_block[0:1]
            if el == coarse_elements-1:
                from_block = from_block[1:2]
                to_block = to_block[1:2]
            return (from_block,from_block),(to_block,to_block)

        vprint("Start")
        
        # Fully internal element matrix
        Ke = integrate_fine_scale_term( weak_form_internal , W, bdy_spaces )

        # Inter-element terms
        LMP_c = integrate_fine_scale_term( weak_form_cross_lmp , W, bdy_spaces )

        # Lagrange multiplier jump term
        LMP_j_ext = integrate_fine_scale_term( weak_form_lmp_jump_ext , W, bdy_spaces )

        # Lagrange multiplier jump term
        LMP_j_int = integrate_fine_scale_term( weak_form_lmp_jump_int , W, bdy_spaces )
        
        # Allocate empty matrices
        K = create_empty_matrix((complete_size,complete_size))
        F = create_empty_vector(complete_size)

        # Assemble internal blocks
        assemble_matrices(Ke,K,coarse_elements,  lambda el: el*element_size  )
        assemble_submatrices(LMP_j_int,K,coarse_elements,  map_Lagrange_multiplier_internal  )

        # Move rows and columns of Lagrange multiplier cross term
        assemble_columns(LMP_c,K,coarse_elements,  map_Lagrange_multiplier_external  )
        assemble_rows(LMP_c,K,coarse_elements,  map_Lagrange_multiplier_external  )

        # Move rows and columns of Lagrange multiplier cross term
        assemble_columns(LMP_j_ext,K,coarse_elements,  map_Lagrange_multiplier_external  )
        vprint(K.array())

        # Prescribe boundary condition on LMP contraint
        lmp_bdy_dof = el_to_lmp_dofs(coarse_elements-1)[0]
        bdy_val = 1
        place_vector([0.5*bdy_val],F,mapping=lmp_bdy_dof)
        vprint("Finished assembly")

        # Solve
        U = solve_system(K,F)
        vprint(F[:])
        vprint("Solved")

        # Rearange solution vector into fenics standard
        rearanged = []
        for el in reversed( range(coarse_elements) ):
            subvec = U[el*element_size:(el+1)*element_size]
            nodes = u_dofs_[:]
            relevant_dofs = subvec[u_dofs_]
            if el != 0:
                relevant_dofs = relevant_dofs[:-1]
            rearanged.append( relevant_dofs )
        U_rear = np.hstack( rearanged )

        # Compare with FEniCS computation
        U_FEniCS = solve_adv_dif(x0,x1,len(U_rear)-1)
        self.assertTrue( np.allclose( U_rear[:], U_FEniCS[:] )  )
        

# Quick FEniCS implementation
def solve_adv_dif(x0,x1,els):
    mesh = IntervalMesh(els,x0,x1)
    V = FunctionSpace(mesh,'CG',1)
    u = TrialFunction(V)
    v = TestFunction(V)
    A = inner(v*Constant((1,)),grad(u))*dx + inner( grad(u), grad(v) )*dx
    bc = DirichletBC( V, Expression('(x[0]-%f)/%f'%(x0,x1-x0),degree=1), lambda x,on_bdy : on_bdy )
    u = Function(V)
    solve( A == Constant(0)*v*dx , u, bc )
    return u.vector()


if __name__ == "__main__":
    VERBOSE[0] = False
    unittest.main()
