import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.functionspaces import *
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np


class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_FUNCTIONSPACES(),'confirmed')


class CoarseScaleElement(unittest.TestCase):
    def test_individual_basis_functions(self):
        pts = [Point(1,1),Point(1,5),Point(-1,1)] 
        mesh_coarse = get_triangle_mesh_from_pts(pts,0)
        
        B1 = get_individual_basis_functions(mesh_coarse,'CG',1)
        self.assertEqual( len(B1),3 )
        self.assertAlmostEqual( calc_area(mesh_coarse)/3. , assemble(B1[0]*dx) )
        
        B2 = get_individual_basis_functions(mesh_coarse,'CG',2)
        self.assertEqual( len(B2),6 )

        B3 = get_individual_basis_functions(mesh_coarse,'BDM',2)
        self.assertEqual( len(B3),12 )
        
        mesh1D = get_one_dimensional_mesh(0.3)
        B4 = get_individual_basis_functions(mesh1D,'CG',2)
        self.assertEqual( len(B4),3 )

    def test_interpolate_coarse_functions_on_fine_space(self):
        pts = [Point(1,1),Point(1,5),Point(-1,1)] 
        mesh_coarse = get_triangle_mesh_from_pts(pts,0)
        mesh_fine   = get_triangle_mesh_from_pts(pts, 2)
        V_fine = FunctionSpace(mesh_fine,'CG',1)
        B = get_individual_basis_functions(mesh_coarse,'CG',1)
        B_int = interpolate_coarse_functions_on_fine_space(B,V_fine)
        
        self.assertTrue(  len(B[0].vector()) < len(B_int[0].vector()) )
        self.assertAlmostEqual( sum( [ abs( B[i](pts[j]) - B_int[i](pts[j]) )  \
                                  for i in range(3) for j in range(3) ])  , 0 )
        self.assertAlmostEqual( calc_area(mesh_coarse)/3. , assemble(B_int[1]*dx) )

        B_DG = get_individual_basis_functions(mesh_coarse,'DG',0)
        B_DG_int = interpolate_coarse_functions_on_fine_space(B_DG,V_fine)
        self.assertTrue( np.allclose( B_DG_int[0].vector()[:] , 1)  )


class AllScalesElement(unittest.TestCase):
    def test_all_spaces_triangle(self):
        h = 0.1
        element_types = ["DG","BDM","CG"]
        Ps_fine = [0,1,1]
        Ps_coarse = [1,1,1]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=1)
        Bs,_ = get_coarse_space_triangle(h,element_types,Ps_coarse,W)

        self.assertEqual( W.num_sub_spaces() , 3 )
        self.assertEqual( len(Bs) , 3 )
        self.assertEqual( len(Bs[0]) , 3 )
        self.assertEqual( len(Bs[1]) , 6 )
        self.assertEqual( len(Bs[2]) , 3 )
        self.assertEqual( len(Bs[1][0].vector()[:]) , W.sub(1).dim() )
        # But the coarse-scale DG doesnt fit on the fine-scale, so:
        self.assertNotEqual( len(Bs[0][0].vector()[:]) , W.sub(0).dim() )

    def test_all_spaces_one_dimensional(self):
        h = 0.1
        element_types = ["DG","R","CG"]
        Ps_fine = [0,0,1]
        Ps_coarse = [1,0,1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=1)
        Bs,_ = get_coarse_space_one_dimensional(h,element_types,Ps_coarse,W)

        self.assertEqual( W.num_sub_spaces() , len(Bs) )


def calc_area(mesh):
    V = FunctionSpace(mesh,'CG',1)
    v = Function(V)
    v.interpolate(Constant(1))
    return( assemble(v*dx) )


if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
