import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.vmsproblem import *
from VMSpy.VMSProblems.VMSProblem_advection_diffusion_primal import getVMSProblem as getVMSProblem_primal
from VMSpy.VMSProblems.VMSProblem_advection_diffusion_mixed import getVMSProblem as getVMSProblem_mixed

import matplotlib.pyplot as plt

from dolfin import *
import numpy as np

class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_VMSPROBLEM(),'confirmed')


class VMS_ProblemTest(unittest.TestCase):

    def test_primal_advection_diffusion_FSGF_FSCF(self):
        """
        This tests effectively tests the primal formulation example files.
        """

        # Physical parameters
        width = 1
        a_vec = Constant((1,))
        kappa = Constant(0.1)
        BC = (lambda x : near(x,0) or near(x,1) , lambda x : 0 ) # indicator function, value function.
        # Discretization parameters
        elements_coarse = 5
        elements_fine = 64
        element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
        element_types_coarse = [element_types_fine[0]]

        # Connector
        connector = Connector_1D(width,elements_coarse)
        lmp_subspace = len(element_types_fine)-1 # Last space LMP space
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( element_types_fine, bdy_spaces, refinement=elements_fine)
        connector.create_coarse_spaces(element_types_coarse)
        
        # Boundary condition
        BCset = BCSet(connector,*BC,subspace=-1,CS_subspace=0)

        # Obtain the VMS problem
        VMS_problem = getVMSProblem_primal(connector,a_vec,kappa, BCset=BCset, projector="H10")
        
        # Compute possible solutions
        global U_FSGF,U_FSCF
        U_full = VMS_problem.compute_full_scale_solution()
        U_FSGF = VMS_problem.compute_fine_scale_Greens_function(0.5)
        U_FSCF = VMS_problem.compute_fine_scale_closure_function(0.4)

        # x,y = connector.get_1d_solution(U_FSGF,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()
        
        # x,y = connector.get_1d_solution(U_FSCF,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()

        # plot(u_p)
        # plot(u_cs)
        # plt.show()

        #print(U_full[::5])
        self.assertTrue( np.allclose( U_full[::5] ,\
            np.array( [ 0.19971011,  0.1872495 ,  0.1716673 ,  0.15607891,  0.14048523,
                        0.12488701,  0.10928492,  0.09367951,  0.07807127,  0.06246061,
                        0.04684787,  0.03123336,  0.01561733,  0.        ,  0.39146784,
                        0.37617955,  0.36084255,  0.34546389,  0.33004959,  0.31460481,
                        0.29913396,  0.28364081,  0.26812858,  0.25260004,  0.23705755,
                        0.22150312,  0.20593848, -0.08168949,  0.56875876,  0.55539945,
                        0.54171238,  0.52774496,  0.51353774,  0.49912541,  0.48453766,
                        0.46979985,  0.4549337 ,  0.43995776,  0.42488793,  0.40973779,
                        -0.09752235, 0.6674616 ,  0.66966066,  0.6692812 ,  0.66669623,
                        0.66222482,  0.65613987,  0.6486748 ,  0.64002927,  0.63037405,
                        0.61985521,  0.60859769,  0.59670834,  0.58427857,  0.02764563,
                        0.15224129,  0.25655185,  0.34351187,  0.41563138,  0.47505727,
                        0.52362589,  0.5629079 ,  0.59424676,  0.61879156,  0.63752518,
                        0.65128829,  0.66079995] )   ))
        
        #print(U_FSGF[::5])
        self.assertTrue( np.allclose( U_FSGF[::5] ,\
            np.array( [-5.08000112e-16, -5.53935099e-16, -5.34152203e-16, -5.11023360e-16,
                       -4.83982658e-16, -4.57573085e-16, -4.26539095e-16, -3.90256223e-16,
                       -3.47836708e-16, -2.98242641e-16, -2.39519527e-16, -1.66764746e-16,
                       -3.14601041e-17,  0.00000000e+00, -1.14549478e-16, -1.14655261e-16,
                       -5.55103688e-17, -1.19450421e-16, -1.26431536e-16, -1.36052666e-16,
                       -1.52919550e-16, -1.71608276e-16, -1.99097567e-16, -2.36093862e-16,
                       -1.39659956e-16, -2.98405268e-16, -3.05912574e-16,  4.53785084e-01,
                        4.54842475e-02,  9.86613965e-02,  1.60832573e-01,  2.33518967e-01,
                        3.18499051e-01,  4.17852099e-01,  4.35715536e-01,  3.85760481e-01,
                        3.27356363e-01,  2.59074163e-01,  1.79243168e-01,  8.59100917e-02,
                       -5.46214916e-01, -3.51712762e-16, -4.42303594e-16, -3.40151922e-16,
                       -4.35934965e-16, -4.18699261e-16, -3.46601161e-16, -3.41148548e-16,
                       -3.34820244e-16, -3.19671710e-16, -2.95344787e-16, -2.74790432e-16,
                       -2.06878483e-16, -1.86192571e-16, -3.15476751e-18, -1.57378500e-17,
                       -8.24935310e-17, -9.57226767e-17,  1.57574300e-17, -6.04717446e-17,
                       -7.79440153e-17, -9.84371004e-17, -1.23822814e-16, -1.24045665e-16,
                       -1.67973770e-16, -2.19304164e-16, -2.66075255e-16,  0.00000000e+00,
                        0.00000000e+00] )   ))

        #print(U_FSCF[::5])
        self.assertTrue( np.allclose( U_FSCF[::5] ,\
            np.array( [6.35000141e-17, 4.14814055e-17, 4.12418701e-17, 4.09618212e-17,
                       4.06344068e-17, 3.83459149e-17, 3.57280976e-17, 3.26675201e-17,
                       2.90892971e-17, 2.49058773e-17, 2.02862374e-17, 1.63863684e-17,
                       3.02242766e-17, -1.39924968e-18, 2.11565321e-02, 7.11070260e-02,
                       1.16292272e-01, 1.55906309e-01, 1.89006858e-01, 2.14492273e-01,
                       2.31074581e-01, 2.37247969e-01, 2.31251942e-01, 2.11028248e-01,
                       1.74170512e-01, 1.17865366e-01, 3.88236033e-02, -2.00000000e-01,
                       -5.16576355e-02, -9.88387580e-02, -1.40786240e-01, -1.76614898e-01,
                       -2.05289836e-01, -2.25601121e-01, -2.36134181e-01, -2.35235191e-01,
                       -2.20970613e-01, -1.91079879e-01, -1.42920086e-01, -7.34013267e-02,
                       -2.00000000e-01, 1.24098307e-16, 1.59132483e-16, 1.07129698e-16,
                       1.38651538e-16, 1.22020928e-16, 7.52921353e-17, 5.63304908e-17,
                       3.81080898e-17, 1.55505982e-17, -1.00455858e-17, -3.76924929e-17,
                       -8.03175287e-17, -1.01902691e-16, 1.89286051e-18, 9.98748173e-18,
                       4.18445447e-17, 5.14509387e-17, 5.15146750e-18, 4.29248324e-17,
                       5.40844817e-17, 6.67616937e-17, 8.29199608e-17, 8.99939141e-17,
                       1.10765312e-16, 1.29145785e-16, 1.50125029e-16, -0.00000000e+00,
                    0.00000000e+00] )   ))


    def test_primal_advection_diffusion_coarse_scale_equals_projection_1D(self):
        """
        This tests effectively tests the mixed formulation example files.
        """
        width = 1
        a_vec = Constant((1,))
        kappa = Constant(0.1)
        BC = (lambda x : near(x,0) or near(x,1) , lambda x : 0 ) # indicator function, value function.
        # Discretization parameters
        elements_coarse = 5
        elements_fine = 64
        element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
        element_types_coarse = [element_types_fine[0]]
        # DG parameters
        eta = Constant( 0.1 )
        eta_bdy = Constant( 0.1  )
        DG_params = (eta,eta_bdy)

        # Connector
        connector = Connector_1D(width,elements_coarse)
        lmp_subspace = len(element_types_fine)-1 # Last space LMP space
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( element_types_fine, bdy_spaces, refinement=elements_fine)
        connector.create_coarse_spaces(element_types_coarse)

        # Boundary condition
        BCset = BCSet(connector,*BC,subspace=-1,CS_subspace=0)

        # Asserts
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, BCset=BCset, projector="H10")
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="Nitsche")
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, BCset=BCset, projector="H10",scale_interaction_mode="CS")
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, BCset=BCset, projector="L2",scale_interaction_mode="CS")
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="Nitsche",scale_interaction_mode="CS")

        # IP
        connector.create_coarse_spaces([("DG",1)])
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="IP")
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="IP",scale_interaction_mode="CS")


    def test_primal_advection_diffusion_coarse_scale_equals_projection_2D(self):
        # Physical parameters
        width = 1
        a_mag = 0
        a_vec = Constant( (1/sqrt(5)*a_mag, 2/sqrt(5)*a_mag) )
        kappa = Constant(0.1)
        elements_coarse_x = 3
        elements_fine_refinement = 3
        element_types_fine = [("CG",1),("CG",1)] # Last space LMP space
        # VMS parameters
        h = width/elements_coarse_x
        eta = Constant( 10/h )
        eta_bdy = Constant( 2/h +float(a_mag/kappa(0)) ) # if projector == "Nitsche" or "IP"
        DG_params = (eta,eta_bdy)

        # Connector
        element_types_coarse = [ element_types_fine[0] ]
        lmp_subspace = len(element_types_fine)-1
        connector = Connector_2D_triangular(width,elements_coarse_x)
        connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
        connector.create_coarse_spaces(element_types_coarse)
    
        # iterators
        BCset = BCSet(connector, lambda x : near(x[0],0) or near(x[0],width) or near(x[1],0) or near(x[1],width) , lambda x : 0 ,subspace=-1,CS_subspace=0)
        
        # asserts
        self.assert_coarse_scale_equals_projection(getVMSProblem_primal, \
            connector,a_vec,kappa, BCset=BCset, projector="H10")


    def test_mixed_advection_diffusion_FSGF_FSCF(self):
        """
        This tests effectively tests the mixed formulation example files.
        """
        # Physical parameters
        width = 1
        a_vec = Constant((1,))
        kappa = Constant(0.01)
        # Discretization parameters
        elements_coarse = 5
        elements_fine = 256
        P_coarse = 1 # Order for coarse-scale sigma
        element_types_fine = [("CG",2),("DG",1),("CG",1)] # Last space LMP space # Last space LMP space
        element_types_coarse = [ ("CG",P_coarse), ("DG",P_coarse-1) ]

        # Connector
        connector = Connector_1D(width,elements_coarse)
        lmp_subspace = len(element_types_fine)-1 # Last space LMP space
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( element_types_fine, bdy_spaces, refinement=elements_fine)
        connector.create_coarse_spaces(element_types_coarse)
        
        # Obtain the VMS problem
        VMS_problem = getVMSProblem_mixed(connector,a_vec,kappa, projector="MM")
        
        # Compute possible solutions
        U_full = VMS_problem.compute_full_scale_solution(solver="lu")
        U_FSGF = VMS_problem.compute_fine_scale_Greens_function(0.5,subspace=1,solver="lu")
        U_FSCF = VMS_problem.compute_fine_scale_closure_function(0.6,solver="lu")
        u_p = VMS_problem.compute_projection(solver="lu")
        u_cs = VMS_problem.compute_coarse_scale_solution(solver="lu")
        
        # x,y = connector.get_1d_solution(U_full,subspace=0,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()
        # x,y = connector.get_1d_solution(U_full,subspace=1,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()

        # x,y = connector.get_1d_solution(U_FSGF,subspace=0,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()
        # x,y = connector.get_1d_solution(U_FSGF,subspace=1,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()
        
        # x,y = connector.get_1d_solution(U_FSCF,subspace=0,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()
        # x,y = connector.get_1d_solution(U_FSCF,subspace=1,refinement=elements_fine)
        # plt.plot(x,y,'-g')
        # plt.show()

        # u_p = VMS_problem.compute_projection()
        # plot(u_p)
        # plt.show()
        
        # print(U_full[::500])
        self.assertTrue( np.allclose( U_full[::500] ,\
            np.array( [ 0.19 ,      0.10234375, 0.0046875,  0.30859375, 0.2109375,  0.5025,
                        0.40484375, 0.70835938, 0.61070313, 0.92296467, 0.82578122] )   ))
        
        # print(U_FSGF[::500])
        self.assertTrue( np.allclose( U_FSGF[::500] ,\
            np.array( [ 0.00999092, -0.36226822,  4.09597268, -0.64325388,  1.80265482, -0.27632834,
                        0.01002602,  0.00999092,  0.00999092,  0.87512613, -2.68671225] )   ))

        # print(U_FSCF[::500])
        self.assertTrue( np.allclose( U_FSCF[::500] ,\
            np.array( [ 5.75484912e-14, -2.29687500e+00,  9.34062501e+01, -8.42187501e+00,
                        8.72812501e+01  ,4.37486995e-01  ,6.99137285e-01 ,-4.08195887e-01,
                        -7.70338021e-01 , 2.39062500e+01 ,-7.57031249e+01] )   ))


    def test_mixed_advection_diffusion_coarse_scale_equals_projection_1D(self):
        # Physical parameters
        width = 1
        a_vec = Constant((1,))
        kappa = Constant(0.01)
        # Discretization parameters
        elements_coarse = 5
        elements_fine = 256
        P_coarse = 1 # Order for coarse-scale sigma
        element_types_fine = [("CG",2),("DG",1),("CG",1)] # Last space LMP space # Last space LMP space
        element_types_coarse = [ ("CG",P_coarse), ("DG",P_coarse-1) ]
        # DG parameters
        eta = Constant( 0.1 )
        eta_bdy = Constant( 0.1  )
        beta = Constant( -0.5 )
        C = Constant( 0.01 )
        DG_params = (eta,eta_bdy,beta,C)

        # Connector
        connector = Connector_1D(width,elements_coarse)
        lmp_subspace = len(element_types_fine)-1 # Last space LMP space
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( element_types_fine, bdy_spaces, refinement=elements_fine)
        connector.create_coarse_spaces(element_types_coarse)

        # Asserts
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, projector="MM")
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, projector="MM",scale_interaction_mode="CS")
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, projector="L2",scale_interaction_mode="CS")

        # LDG
        connector.create_coarse_spaces([("DG",1),("DG",1)])
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="LDG")
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, DG_parameters=DG_params, projector="LDG",scale_interaction_mode="CS")


    def test_mixed_advection_diffusion_coarse_scale_equals_projection_2D(self):
        """
        This tests effectively tests the mixed formulation example files.
        """
        
        # Physical parameters
        width = 1
        a_mag = 0
        a_vec = Constant( (1/sqrt(5)*a_mag, 2/sqrt(5)*a_mag) )
        kappa = Constant(0.1)
        elements_coarse_x = 3
        elements_fine_refinement = 3 # Choosing 4 due to higher order 
        element_types_fine = [("BDM",1),("DG",0),("BDM",1)] # Last space LMP space
        P_coarse = 1 # Order for coarse-scale sigma
        # VMS parameters
        h = width/elements_coarse_x
        eta = Constant( 0.001 )
        eta_bdy = Constant( kappa(0)/h )
        beta = Constant( -0.5 )
        C = Constant( 1 )
        DG_params = (eta,eta_bdy,beta,C)

        # Connector
        element_types_coarse = [ ("BDM",P_coarse), ("DG",P_coarse-1) ]
        lmp_subspace = len(element_types_fine)-1
        connector = Connector_2D_triangular(width,elements_coarse_x)
        connector.create_local_spaces( element_types_fine, [lmp_subspace], refinement=elements_fine_refinement)
        connector.create_coarse_spaces(element_types_coarse)

        # Asserts
        self.assert_coarse_scale_equals_projection(getVMSProblem_mixed, \
            connector,a_vec,kappa, projector="MM")
        

    def assert_coarse_scale_equals_projection(self,vms_getter, *args,**kwargs ):
        VMS_problem = vms_getter( *args,**kwargs )
        u_p = VMS_problem.compute_projection(solver="lu")
        u_cs = VMS_problem.compute_coarse_scale_solution(solver="lu")
        self.assertTrue(  np.allclose(u_p.vector()[:],u_cs.vector()[:])  )


if __name__ == "__main__":
    VERBOSE[0]=False
    unittest.main()
