import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy import *
from dolfin import *

class Basic1DAdvectionDiffusion(unittest.TestCase):
    def test(self):
        ## Solve a 1D advection-diffusion problem by partitioning and assembling
        ## and compare to the solution vector of standard FEniCS implementation
        ## NOTE: FEniCS dof numbering in 1D goes backwards (right to left)

        # Problem formulation
        x0,x1 = 0,1
        coarse_elements = 4
        fine_elements = 10
        def weak_form(u,v):
            return inner(v*Constant((1,)),grad(u))*dx + inner( grad(u), grad(v) )*dx

        # Space on domain partitioning
        h = (x1-x0)/coarse_elements
        element_types = ["CG"]
        Ps_fine = [1]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=fine_elements)

        # Sizes
        element_size = get_element_size(W)
        complete_size = get_full_size(coarse_elements,W)-coarse_elements+1

        # Base element matrix
        Ke = integrate_fine_scale_term( weak_form , W )
        
        # Left node Bdy_cond
        Ke_left_bdy = Ke.copy()
        apply_boundary_condition(Ke_left_bdy,element_size-1)
        
        # Right node Bdy_cond
        Ke_right_bdy = Ke.copy()
        apply_boundary_condition(Ke_right_bdy,0)

        # Allocate empty matrices
        K = create_empty_matrix((complete_size,complete_size))
        F = create_empty_vector(complete_size)

        # Assemble
        place_block_diagonal_matrix(Ke_right_bdy,K)
        assemble_matrices(Ke,K,coarse_elements-2,lambda n: (element_size-1)*(coarse_elements-n-2))
        place_block_diagonal_matrix(Ke_left_bdy,K,(element_size-1)*(coarse_elements-1))
        place_vector([1],F,mapping=0)

        # Solve
        U = solve_system(K,F)
        U_FEniCS = solve_adv_dif(x0,x1,complete_size-1, weak_form)

        # Compare
        self.assertTrue( np.allclose( U[:], U_FEniCS[:] )  )
        

# Quick FEniCS implementation
def solve_adv_dif(x0,x1,els, weak_form):
    mesh = IntervalMesh(els,x0,x1)
    V = FunctionSpace(mesh,'CG',1)
    u = TrialFunction(V)
    v = TestFunction(V)
    A = weak_form(u,v)
    bc = DirichletBC( V, Expression('(x[0]-%f)/%f'%(x0,x1-x0),degree=1), lambda x,on_bdy : on_bdy )
    u = Function(V)
    solve( A == Constant(0)*v*dx , u, bc )
    return u.vector()


if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
