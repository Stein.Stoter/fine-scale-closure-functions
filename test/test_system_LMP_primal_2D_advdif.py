import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from dolfin import *
from VMSpy import *

"""
Solves:

(a grad u , v )+(grad u, grad v) + (avg(l) n_tilde , jump(v)) = (f,v)
(avg(q) n_tilde, jump(u)) = 0
(jump(q) , jump(l)) = 0

by separating into elements. The fields u and l^in are element local,
and all coupling occurs through l^\pm and q^\pm. Per element:

(a grad u , v )_K + (grad u, grad v)_K
+ (0.5*l^in n_tilde, v^in n^in) + (0.5*q^in n_tilde, u^in n^in)
(q^in , l^in) 
+ (0.5*l^out n_tilde, v^in n^in) + (0.5*q^out n_tilde, u^in n^in)
- (q^out , l^in)
= (f,v)_K
"""

class LMP_mixed_2D_advdif_Test(unittest.TestCase):
    def test_assemble(self):
        global connector, iterator
        # Problem formulation
        width = 1
        triangles_x = 4
        element_refinement = 4
        def weak_form_internal(ul,vq,ds,dI):
            # All variables are internal
            n = FacetNormal( ul.function_space().mesh() )
            nn = Constant((1,0.5))
            n_tilde = n*abs(inner(n,nn))/inner(n,nn)
            u,l = split(ul)
            v,q = split(vq) #inner(v*Constant((1,0.5)),grad(u))*dx + 
            return inner( grad(u), grad(v) )*dx + \
                   u*q*ds + l*v*ds \
                   + 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI \
                   + l*q*dI
        def weak_form_cross_lmp(ul,vq,dI):
            # u,v are internal l,q external
            n = FacetNormal( ul.function_space().mesh() )
            nn = Constant((1,0.5))
            n_tilde = n*abs(inner(n,nn))/inner(n,nn)
            u,l = split(ul)
            v,q = split(vq)
            return 0.5*u*q*inner(n,n_tilde)*dI + 0.5*l*v*inner(n,n_tilde)*dI
        def weak_form_lmp_jump_ext(ul,vq,dI):
            # l is internal q is external
            u,l = split(ul)
            v,q = split(vq)
            return -l*q*dI
        weak_forms = [weak_form_internal,
                      weak_form_cross_lmp,
                      weak_form_cross_lmp,
                      weak_form_lmp_jump_ext]
        def weak_form_body_force(ul,vq):
            # All variables are internal
            v,q = split(vq)
            return v*dx

        # Connector
        connector = Connector_2D_triangular(width,triangles_x)
        lmp_subspace = 1
        bdy_spaces = [lmp_subspace]
        connector.create_local_spaces( [('CG',1),('CG',1)],bdy_spaces, refinement=element_refinement)

        # Iterator
        iterator = ElementIterator(connector)
        iterator.Kes = weak_forms
        iterator.assemble_functions = [place_block_diagonal_matrix,\
                                       place_columns,\
                                       place_rows,\
                                       place_columns ]
        iterator.element_routines = [ "full_element",
                                      "per_internal_face",
                                      "per_internal_face",
                                      "per_internal_face"]

        # Assemble stiffness
        iterator.assemble_K()

        # Produce force vector
        iterator.Fes = [lambda elnr: integrate_fine_scale_term(weak_form_body_force,connector.get_element(elnr).W,iterator.bdy_spaces)]
        iterator.assemble_functions_force = [assemble_vectors]
        iterator.mappings_force = [connector.get_map_all_dofs]
        iterator.assemble_F()
        
        # Solve
        iterator.solve()
        U = iterator.U

        # Export test, must be verified in Paraview
        name = "./Output/LMP_mixed_2D_advdif"
        connector.exportSolution(U, name, subspace=0)
        connector.exportSolution(U, name, subspace=lmp_subspace, fieldname='l')
        
        # Compare with FEniCS computation
        u_FEniCS = solve_adv_dif(width,triangles_x*(2**element_refinement))
        u_FEniCS.rename('u','u')
        File("./Output/LMP_mixed_2D_advdif_FEniCS.pvd") << u_FEniCS
                

# Quick FEniCS implementation
def solve_adv_dif(width,els_x):
    mesh = RectangleMesh(Point(0,0),Point(width,width),els_x,els_x)
    V = FunctionSpace(mesh,'CG',1)
    u = TrialFunction(V)
    v = TestFunction(V)
    A = inner(v*Constant((1,0.5)),grad(u))*dx + inner( grad(u), grad(v) )*dx
    bc = DirichletBC( V, Constant(0), lambda x,on_bdy : on_bdy )
    u = Function(V)
    solve( A == v*dx , u, bc )
    return u


if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
