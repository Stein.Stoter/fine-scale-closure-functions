import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from VMSpy.integrations import *
from VMSpy.functionspaces import *
from VMSpy.verboseprint import *
from dolfin import *
import numpy as np


class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_INTEGRATIONS(),'confirmed')


class IntegrationsTest(unittest.TestCase):
    def test_integrate_fine_scale_term(self):
        h = 0.1
        element_types = ["BDM","DG"]
        Ps_fine = [1,0]
        W = get_fine_space_triangle(h,element_types,Ps_fine,which="upper",refinement=2)
        
        def weak_form(up,vq):
            u,p = split(up)
            v,q = split(vq)
            return inner(u,v)*dx+p*q*dx
        
        M = integrate_fine_scale_term(weak_form,W)
        self.assertEqual(M.size(0),M.size(1))
        self.assertEqual(M.size(0),W.dim())

        # Symmetric matrix with positive diagonal values
        Ma = M.array()
        self.assertTrue( np.allclose(Ma,Ma.T) )
        for i in range(W.dim()):
            self.assertTrue( Ma[i,i] > 1E-6 )
        
    def test_integrate_constraint_term(self):
        h = 0.1
        element_types = ["DG","CG"]
        Ps_fine = [0,1]
        Ps_coarse = [0,1]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=1)
        (B_DG,B_CG),_ = get_coarse_space_triangle(h,element_types,Ps_coarse,W)
        
        def weak_form(testfunction,coarse_basis):
            v_dg,v_cg = split(testfunction)
            return v_dg*coarse_basis*ds
        
        C_DG = integrate_constraint_terms(weak_form,B_DG,W)
        C_CG = integrate_constraint_terms(weak_form,B_CG,W)
        C_DG_vec  = C_DG[0][:]
        C_CG_vecs = [ vec[:]  for vec in C_CG ]
        # Integration with DG basis equals sum of the integration of three nodal CG bases:
        self.assertTrue( np.allclose(C_DG_vec, sum(C_CG_vecs)) )

    def test_compressed_integration_triangle(self):
        h = 0.3
        element_types = ["CG","DG"]
        Ps_fine = [1,0]
        Ps_coarse = [1,0]
        W = get_fine_space_triangle(h,element_types,Ps_fine,refinement=4)
        (B_CG,B_DG),_ = get_coarse_space_triangle(h,element_types,Ps_coarse,W)
        
        bdy_spaces = [0]
        
        def weak_form(testfunction,coarse_basis):
            v_cg,v_dg = split(testfunction)
            return v_cg*coarse_basis*ds
        
        B = integrate_constraint_terms(weak_form,B_DG,W)[0]
        self.assertAlmostEqual( sum(B[:]) , (2+np.sqrt(2))*h )
        B_compressed = integrate_constraint_terms(weak_form,B_DG,W,bdy_spaces)[0]
        self.assertTrue( len(B_compressed) < len(B) )
        self.assertAlmostEqual( sum(B_compressed[:]) , sum(B[:]) )

    def test_compressed_integration_one_dimensional(self):
        h = 0.3
        element_types = ["CG","DG"]
        Ps_fine = [1,0]
        Ps_coarse = [1,0]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=5)
        (B_CG,B_DG),_ = get_coarse_space_one_dimensional(h,element_types,Ps_coarse,W)
        bdy_spaces = [0]
        
        def weak_form(testfunction,coarse_basis):
            v_cg,v_dg = split(testfunction)
            return v_cg*coarse_basis*ds
        
        BCG0 = integrate_constraint_terms(weak_form,B_CG,W)[0]
        self.assertAlmostEqual( sum(BCG0[:]) , 1 )
        BCG0_compressed = integrate_constraint_terms(weak_form,B_CG,W,bdy_spaces)[0]
        self.assertTrue( len(BCG0_compressed) < len(BCG0) )
        self.assertAlmostEqual( sum(BCG0_compressed[:]) , sum(BCG0[:]) )
        
        BDG0 = integrate_constraint_terms(weak_form,B_DG,W)[0]
        self.assertAlmostEqual( sum(BDG0[:]) , 2 )
        BDG0_compressed = integrate_constraint_terms(weak_form,B_DG,W,bdy_spaces)[0]
        self.assertTrue( len(BDG0_compressed) < len(BDG0) )
        self.assertAlmostEqual( sum(BDG0_compressed[:]) , sum(BDG0[:]) )
        
    def test_compress_matrix(self):
        h = 0.3
        element_types = ["CG","DG"]
        Ps_fine = [1,0]
        Ps_coarse = [1,0]
        W = get_fine_space_one_dimensional(h,element_types,Ps_fine,refinement=5)
        (B_CG,B_DG),_ = get_coarse_space_one_dimensional(h,element_types,Ps_coarse,W)
        bdy_spaces = [0]
        
        def weak_form(uu,vv):
            u_cg,u_dg = split(uu)
            v_cg,v_dg = split(vv)
            return u_cg*v_cg*ds
        
        Mfull = integrate_fine_scale_term(weak_form,W,bdy_spaces)
        self.assertEqual( Mfull.mat().size[0] , 2 + 5 )
        self.assertAlmostEqual( sum(sum(Mfull.array())) ,  2)
        
        Msub = integrate_subspace(weak_form,W,bdy_spaces,bdy_spaces)
        self.assertEqual( Msub.mat().size[0] , 2)
        self.assertAlmostEqual( sum(sum(Msub.array())) ,  2)

        Msub_off = integrate_subspace(weak_form,W,[1],bdy_spaces)
        self.assertEqual( Msub_off.mat().size[0] , 5)
        self.assertAlmostEqual( sum(sum(Msub_off.array())) ,  0)

        
if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
