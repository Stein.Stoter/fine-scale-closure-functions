import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
import numpy as np
from VMSpy.connector import *
from VMSpy.verboseprint import *

# Depracated code that does test deeper functionality
def get_map_internal_boundary_dofs(self,element_nr,bdy_space=0):
    """
    Map of the local bdy_space dofs on internal boundaries of the element_nr-th element to their
    rows/cols in the global matrix. Usable with assembly.place_submatrix( Ke, K, *map ).
    Returns: (internal_local_dofs,internal_local_dofs), (internal_global_dofs,internal_global_dofs)
    """
    element = self.get_element(element_nr)
    row_offset = element_nr*self.W_size
    internal_local_dofs = get_subspace_local_dofs(element.W,element.bdy_spaces[bdy_space],element.bdy_spaces)
    internal_global_dofs = internal_local_dofs + row_offset
    return (internal_local_dofs,internal_local_dofs), (internal_global_dofs,internal_global_dofs)

def get_map_opposing_boundary_dofs(self,element_nr,space_nr=-1):
    """
    Map of the local bdy_space dofs of the element_nr-th element to the rows/cols of their neighbours
    in the global matrix. Usable with assembly.place_rows( Ke, K, *map ) or place_columns.
    Returns: internal_local_dofs, external_global_dofs, row_offset
    """
    element = self.get_element(element_nr)
    row_offset = element_nr*self.W_size
    internal_local_dofs = []
    external_global_dofs = []
    for face_nr in range(element.total_faces):
        i, e, r = self.get_map_opposing_face_dofs(element_nr,face_nr,space_nr)
        internal_local_dofs.append(i)
        external_global_dofs.append(e)
    internal_local_dofs  = np.array( np.hstack( internal_local_dofs ), dtype=np.int32)
    external_global_dofs = np.array( np.hstack( external_global_dofs ) , dtype=np.int32)
    return internal_local_dofs, external_global_dofs, row_offset


class ModuleTest(unittest.TestCase):
    def test_import(self):
        self.assertEqual(CONFIRM_IMPORT_CONNECTOR(),'confirmed')

class ConnectorTest(unittest.TestCase):
    def test_connector1D(self):
        width = 2.5
        elements = 5
        connector = Connector_1D(width,elements)
        connector.create_local_spaces( [('CG',1),('CG',2)],[0] )

        self.assertEqual( connector.get_element_number_from_location(Point(0)) , 0 )
        self.assertEqual( connector.get_element_number_from_location(Point(width)) , elements-1 )

        # Get opposite faces
        self.assertTrue( connector.get_face_opposite(0,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(0,1) == (1,0) )
        self.assertTrue( connector.get_face_opposite(4,0) == (3,1) )
        self.assertTrue( connector.get_face_opposite(4,1) == (-1,-1) ) # Domain boundary

        # Boundary counting is unspecified and gives: right, left
        (el0_int_loc,_),(el0_int_glob,_) = get_map_internal_boundary_dofs(connector,0)
        (el1_int_loc,_),(el1_int_glob,_) = get_map_internal_boundary_dofs(connector,1)
        (el4_int_loc,_),(el4_int_glob,_) = get_map_internal_boundary_dofs(connector,4)
        self.assertTrue( len(el0_int_loc) == len( el0_int_glob) == 2 )
        self.assertTrue( np.allclose(el0_int_loc,el1_int_loc) )
        self.assertTrue( np.allclose(el0_int_loc,el4_int_loc) )
        self.assertFalse( np.allclose(el0_int_glob,el1_int_glob) )
        self.assertFalse( np.allclose(el0_int_glob,el4_int_glob) )

        # Boundary counting: left, right
        el0_opp_loc,el0_opp_glob,_ = get_map_opposing_boundary_dofs(connector,0)
        el1_opp_loc,el1_opp_glob,_ = get_map_opposing_boundary_dofs(connector,1)
        el4_opp_loc,el4_opp_glob,_ = get_map_opposing_boundary_dofs(connector,4)
        self.assertTrue( len(el1_opp_loc) == 2 )
        self.assertTrue( len(el0_opp_loc) == len(el4_opp_loc) == 1 )
        self.assertEqual( el0_opp_glob[0] , el1_int_glob[1] )
        self.assertEqual( el1_opp_glob[0] , el0_int_glob[0] )

        # Get 1D solution
        U = np.arange( connector.W_size * connector.total_elements )
        X,Y = connector.get_1d_solution(U,subspace=1,refinement=1)
        for x,y in zip(X,Y):
            if not x == None:
                self.assertAlmostEqual(  y, connector.get_pointvalue(U,Point(x),subspace=1) )

    def test_connector2D_triangle_faces(self):
        width = 2.5
        triangles_x = 3
        connector = Connector_2D_triangular(width,triangles_x)
        connector.create_local_spaces( [('CG',1),('CG',2)],[0] )

        # Get element types: internal, vertical, horizontal, corner
        self.assertTrue( connector.get_element_type(0) == 2 )
        self.assertTrue( connector.get_element_type(1) == 2 )
        self.assertTrue( connector.get_element_type(2) == 3 )
        self.assertTrue( connector.get_element_type(3) == 1 )
        self.assertTrue( connector.get_element_type(4) == 0 )
        self.assertTrue( connector.get_element_type(5) == 0 )
        self.assertTrue( connector.get_element_type(6) == 0 )
        self.assertTrue( connector.get_element_type(7) == 0 )
        self.assertTrue( connector.get_element_type(8) == 1 )
        self.assertTrue( connector.get_element_type(9) == 1 )
        self.assertTrue( connector.get_element_type(14) == 1 )
        self.assertTrue( connector.get_element_type(15) == 3 )
        self.assertTrue( connector.get_element_type(15) == 3 )
        self.assertTrue( connector.get_element_type(15) == 3 )
        self.assertTrue( connector.get_element_type(17) == 2 )

        # Get opposite faces
        self.assertTrue( connector.get_face_opposite(0,0) == (4,0) )
        self.assertTrue( connector.get_face_opposite(0,1) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(0,2) == (3,2) )
        self.assertTrue( connector.get_face_opposite(2,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(2,1) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(8,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(9,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(14,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(15,0) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(15,1) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(16,1) == (-1,-1) ) # Domain boundary
        self.assertTrue( connector.get_face_opposite(14,1) == (11,1) )
        self.assertTrue( connector.get_face_opposite(14,2) == (17,2) )
        self.assertTrue( connector.get_face_opposite(7,0) == (11,0) )
        self.assertTrue( connector.get_face_opposite(7,1) == (4,1) )
        self.assertTrue( connector.get_face_opposite(7,2) == (10,2) )
        
    def test_connector2D_triangle_dofs(self):
        width = 2.5
        triangles_x = 3
        connector = Connector_2D_triangular(width,triangles_x)
        connector.create_local_spaces( [('CG',1),('CG',2)],[0] )

        # Face dof connectivity
        el7_f0_loc, el7_f0_glob,el7_off  = connector.get_map_opposing_face_dofs(7,0)
        el7_f1_loc, el7_f1_glob,el7_off  = connector.get_map_opposing_face_dofs(7,1)
        el7_f2_loc, el7_f2_glob,el7_off  = connector.get_map_opposing_face_dofs(7,2)
        # Surrounding elements:
        el11_f0_loc,el11_f0_glob,el11_off = connector.get_map_opposing_face_dofs(11,0)
        el4_f1_loc, el4_f1_glob,el4_off  = connector.get_map_opposing_face_dofs(4,1)
        el10_f2_loc,el10_f2_glob,el10_off = connector.get_map_opposing_face_dofs(10,2)
        self.assertTrue( len(el7_f0_loc) == len(el7_f0_glob) == len(el4_f1_glob)  )
        self.assertTrue( np.allclose( el7_f0_glob,el11_f0_loc+el11_off ) ) 
        self.assertTrue( np.allclose( el7_f1_glob,el4_f1_loc+el4_off ) ) 
        self.assertTrue( np.allclose( el7_f2_glob,el10_f2_loc+el10_off ) ) 
        
        # Full boundary dof connectivity
        (el7_int_loc,_), (el7_int_glob,_)  = get_map_internal_boundary_dofs(connector,7)
        el7_opp_loc, el7_opp_glob,_  = get_map_opposing_boundary_dofs(connector,7)
        # Surrounding elements:
        el4_opp_loc, el4_opp_glob,_  = get_map_opposing_boundary_dofs(connector,4)
        el10_opp_loc,el10_opp_glob,_ = get_map_opposing_boundary_dofs(connector,10)
        el11_opp_loc,el11_opp_glob,_ = get_map_opposing_boundary_dofs(connector,11)

        self.assertEqual( len(el7_opp_glob) , len(el7_f0_glob)+len(el7_f1_glob)+len(el7_f2_glob) )
        self.assertTrue( len(el7_int_loc)+3 == len(el7_opp_loc) == len(el10_opp_loc)  ) # double at nodes
        self.assertTrue( np.alltrue( [ d in el7_opp_loc for d in el7_int_loc ]  ) )
        self.assertTrue( np.alltrue( [ d in el4_opp_glob or d in el10_opp_glob or d in el11_opp_glob for d in el7_int_glob ]  ) )

    def test_solution_reconstruction(self):
        width = 2.5
        triangles_x = 3
        connector = Connector_2D_triangular(width,triangles_x)
        connector.create_local_spaces( [('CG',1),('CG',2)],[0] )
        element_nr = 3
        W = connector.W
        W_size = connector.W_size
        U = np.arange( W_size * connector.total_elements )
        sol = connector.reconstruct_function_in_element(U,element_nr)

        self.assertEqual( connector.get_element_number_from_location(Point(0.001,0)) , 0 )
        self.assertEqual( connector.get_element_number_from_location(Point(width-0.001,width)) , triangles_x*triangles_x*2-1 )
    
        self.assertEqual( len(sol.vector()), W.dim() ) 
        self.assertEqual( sum(sol.vector()[:]) , sum(np.arange(element_nr*W_size,(element_nr+1)*W_size)) )

        loc0 = Point( (0.2,0.1) )
        val0 = connector.get_pointvalue(U,loc0,subspace=1)
        _,sol0 = connector.reconstruct_function_in_element(U,0).split()
        loc1 = Point( (width-0.2,width-0.1) )
        val1 = connector.get_pointvalue(U,loc1,subspace=1)
        _,sol1 = connector.reconstruct_function_in_element(U,triangles_x*triangles_x*2-1).split()
        loc1_rel = Point( (width/triangles_x-0.2,width/triangles_x-0.1) )
        
        self.assertAlmostEqual( val0, sol0(loc0) )
        self.assertAlmostEqual( val1, sol1(loc1_rel) )

        # Export test, must be verified in Paraview
        name = "./Output/ConnectorTestExport"
        connector.exportSolution(U, name, subspace=1)
        connector.exportSolution(U, name, subspace=0, fieldname="lmp")

    def test_global_dof_from_loc(self):
        width = 3
        elements_coarse = 3
        elements_fine = 5
        connector1D = Connector_1D(width,elements_coarse)
        connector1D.create_local_spaces( [('CG',1)], refinement=elements_fine )
        loc = width/(elements_coarse*elements_fine) * ( (elements_coarse-1)*elements_fine - 1 ) + 0.02
        dof = connector1D.get_global_dof_from_loc(loc)
        self.assertEqual( dof , elements_fine+2 )
        
        connector2D = Connector_2D_triangular(width,elements_coarse)
        connector2D.create_local_spaces( [('BDM',1),('DG',0)], refinement=1 )
        W = connector2D.get_element(7).W
        dof = connector2D.get_global_dof_from_loc( (1.83333333,1.66666667) ,subspace=1)-W.dim()*7
        loc = W.tabulate_dof_coordinates()[dof] + 1

        self.assertAlmostEqual( loc[0], 1.8 + 1/30 )
        self.assertAlmostEqual( loc[1], 1 + 2/3 )

    def test_get_coarse_dof_from_loc(self):
        width = 3
        elements_x = 3
        connector1D = Connector_1D(width,elements_x)
        connector1D.create_local_spaces( [('CG',1),('CG',2),('CG',1)],[0] )
        connector1D.create_coarse_spaces( [('CG',1),('DG',2)] )

        # CG1 goes globally from right to left
        self.assertEqual( connector1D.get_coarse_dof_from_loc(width) , 0 )
        self.assertEqual( connector1D.get_coarse_dof_from_loc(0) , connector1D.coarse_spaces[0].dim()-1 )
        # DG2 per element as right,left,center
        self.assertEqual( connector1D.get_coarse_dof_from_loc(0,subspace=1) , 1 )
        self.assertEqual( connector1D.get_coarse_dof_from_loc( 1.5 ,subspace=1) , 5 )
                
        connector2D = Connector_2D_triangular(width,elements_x)
        connector2D.create_local_spaces( [('BDM',2),('BDM',2),('DG',1)] ,[0] )
        connector2D.create_coarse_spaces( [ ('BDM',1),('DG',1)] )

        # DG2 per element as lr, ll, ul for which=lower and ul, lr, ur for which=upper
        loc = np.array((1.7,1.6))
        self.assertEqual( connector2D.get_coarse_dof_from_loc( loc , subspace=1) , 7*3+3 - 1 )
        BDM_dof = connector2D.get_coarse_dof_from_loc( loc )
        BDM_loc = connector2D.coarse_spaces[0].tabulate_dof_coordinates()[BDM_dof]
        self.assertAlmostEqual( sum((BDM_loc-np.array([1+2/3, 1+2/3]) )**2) , 0  )

    def test_coarse_spaces(self):
        width = 3
        elements_x = 3
        connector1D = Connector_1D(width,elements_x)
        connector2D = Connector_2D_triangular(width,elements_x)
        
        connector1D.create_local_spaces( [('CG',1)] ) # Purely for mesh creation
        connector2D.create_local_spaces( [('CG',1)] ) # Purely for mesh creation
        
        connector1D.create_coarse_spaces( [('CG',2)] )
        connector2D.create_coarse_spaces( [('CG',1),('BDM',1)] )

        self.assertEqual(len(connector1D.coarse_spaces),connector1D.coarse_space.num_sub_spaces()+1)
        self.assertEqual(len(connector2D.coarse_spaces),connector2D.coarse_space.num_sub_spaces())

        self.assertEqual(sum([Vh.dim() for Vh in connector1D.coarse_spaces]),connector1D.coarse_space.dim())
        self.assertEqual(sum([Vh.dim() for Vh in connector2D.coarse_spaces]),connector2D.coarse_space.dim())

        Uh1D_1 = interpolate(Expression('pow(x[0],2)',degree=2),connector1D.coarse_spaces[0]).vector()
        Uh1D_2 = interpolate(Expression('pow(x[0],2)',degree=2),connector1D.coarse_space).vector()
        self.assertTrue( np.allclose( connector1D.to_mixed_coarse_scale_space(Uh1D_1), Uh1D_2 ) )
        self.assertTrue( np.allclose( connector1D.from_mixed_coarse_scale_space(Uh1D_2), Uh1D_1 ) )

        Uh2D_1_1 = interpolate(Expression('x[0]',degree=2),connector2D.coarse_spaces[0]).vector()
        Uh2D_1_2 = interpolate(Expression(('pow(x[0],2)','pow(x[0],3)'),degree=2),connector2D.coarse_spaces[1]).vector()
        Uh2D_1 = np.hstack( (Uh2D_1_1,Uh2D_1_2) )
        Uh2D_2 = interpolate(Expression(('x[0]','pow(x[0],2)','pow(x[0],3)'),degree=2),connector2D.coarse_space).vector()        
        self.assertTrue( np.allclose( connector2D.to_mixed_coarse_scale_space(Uh2D_1), Uh2D_2 ) )
        self.assertTrue( np.allclose( connector2D.from_mixed_coarse_scale_space(Uh2D_2), Uh2D_1 ) )

        # Coarse-scale export test, must be verified in Paraview
        name = "./Output/ConnectorTestCoarseScaleExport_asMixed"
        connector2D.exportCoarseScaleSolution(Uh2D_2, name, subspace=0)
        connector2D.exportCoarseScaleSolution(Uh2D_2, name, subspace=1, fieldname="DG")
        name = "./Output/ConnectorTestCoarseScaleExport_asSet"
        connector2D.exportCoarseScaleSolution(Uh2D_1, name, subspace=0, representation="set")
        connector2D.exportCoarseScaleSolution(Uh2D_1, name, subspace=1, fieldname="DG", representation="set")

        connector2D.create_coarse_spaces( [('DG',2)] )
        Uh1 = np.linspace(0,1,connector2D.coarse_space.dim())
        Uh2 = connector2D._reshuffle_to_DG(Uh1)
        self.assertAlmostEqual(float(sum(Uh1)),float(sum(Uh2)))
        hash1 = 0
        hash2 = 0
        # Compute some obscure hash
        for el_nr,c in enumerate(cells(connector2D.coarse_space.mesh())):
            c_dofs = connector2D.coarse_space.dofmap().cell_dofs(c.index())
            c_locs = connector2D.coarse_space.tabulate_dof_coordinates()[c_dofs]
            c_mp = c.midpoint()
            hash2 += float( sum( np.asmatrix(c_locs).T*np.asmatrix(Uh2[c_dofs]).T ) )

            el = connector2D.get_element(el_nr)
            el_loc = connector2D.get_element_location(el_nr)
            el_B_locs = np.asarray(  [ (loc + el_loc).array()[:connector2D.dimension] for loc in el.Bs_dof_locs[0] ]  )
            el_dofs = [*range(el_nr*len(el_B_locs),(el_nr+1)*len(el_B_locs))] 
            hash1 += float( sum( np.asmatrix(el_B_locs).T*np.asmatrix(Uh1[el_dofs]).T ) )
        self.assertAlmostEqual(hash1,hash2)
        
    def test_map_coarse_dofs(self):
        width = 3
        elements_x = 3
        connector1D = Connector_1D(width,elements_x)
        connector2D = Connector_2D_triangular(width,elements_x)

        els1D = connector1D.total_elements # = elements_x
        els2D = connector2D.total_elements # = 2*elements_x**2
        edges2D = elements_x*((elements_x+elements_x+(elements_x+1))+1)

        bdy_space = [0]
        connector1D.create_local_spaces( [('CG',1),('CG',2),('CG',1)] , bdy_space )
        connector2D.create_local_spaces( [('BDM',2),('BDM',2),('DG',1)],bdy_space)
        
        connector1D.create_coarse_spaces( [('DG',2)] )
        connector2D.create_coarse_spaces( [('BDM',1),('DG',1)] )

        firstdof1D = connector1D.get_map_coarse_dof(0,0)
        firstdof2D = connector2D.get_map_coarse_dof(0,0,CS_space_nr=1)
        lastdof1D = connector1D.get_map_coarse_dof(els1D-1,2)
        lastdof2D = connector2D.get_map_coarse_dof(els2D-1,2,CS_space_nr=1)
        
        self.assertEqual( firstdof1D, els1D*connector1D.W_size )
        self.assertEqual( lastdof1D , els1D*connector1D.W_size + els1D*3 - 1 )
        self.assertEqual( firstdof2D, els2D*connector2D.W_size + edges2D*2 )
        self.assertEqual( lastdof2D , els2D*connector2D.W_size + edges2D*2 + els2D*3 - 1 )

        connector1D.create_coarse_spaces( [ ('CG',2) ] )
        X = np.linspace(0,width,elements_x+9)
        Y = np.linspace(0.0001,width-0.0001,elements_x+9)
        dofloctab2 = connector2D.coarse_spaces[0].tabulate_dof_coordinates()
        for x in X:
            p1 = Point(x)
            elnr1 = connector1D.get_element_number_from_location(p1)
            el1   = connector1D.get_element(elnr1)
            p1_loc = connector1D.get_relative_location(elnr1,p1)
            for locdof in range(3):
                # Check the 1D CG-connectivity
                cs_dof = connector1D.get_map_coarse_dof(elnr1,locdof) \
                         - connector1D.total_elements*connector1D.W_size
                cs_basis_loc = el1.Bs[0][locdof]
                cs_basis_glob = Function(connector1D.coarse_spaces[0])
                cs_basis_glob.vector()[cs_dof] = 1
                self.assertAlmostEqual( cs_basis_glob(p1), cs_basis_loc(p1_loc) )
            for y in Y:
                # Check the 2D BDM-connectivity
                p2 = Point(x,y)
                elnr2 = connector2D.get_element_number_from_location(p2)
                el2   = connector2D.get_element(elnr2)
                p2_loc = connector2D.get_relative_location(elnr2,p2)
                for locdof in range(6):
                    cs_dof = connector2D.get_map_coarse_dof(elnr2,locdof) \
                             - connector2D.total_elements*connector2D.W_size
                    cs_basis_loc = el2.Bs[0][locdof]
                    cs_basis_glob = Function(connector2D.coarse_spaces[0])
                    cs_basis_glob.vector()[cs_dof] = 1
                    self.assertTrue( np.allclose(cs_basis_glob(p2),cs_basis_loc(p2_loc)) )

    def test_get_global_dofs_locs_from_indicator(self):
        width = 3
        elements_x = 3
        connector1D = Connector_1D(width,elements_x)
        connector2D = Connector_2D_triangular(width,elements_x)
        connector1D.create_local_spaces( [('CG',1),('CG',2),('CG',1)]  , [0], refinement=1)
        connector2D.create_local_spaces( [('BDM',1),('BDM',1),('DG',1)], [0], refinement=0)

        d1,l1 = connector1D.get_global_dofs_locs_from_indicator(lambda x: np.where(x>1.45)[0], 1, False )
        self.assertEqual( len(d1) , 5 )
        for l in l1:
            self.assertTrue( l > 1.45 )

        d2,l2 = connector2D.get_global_dofs_locs_from_indicator(lambda x: np.where(x[:,0]>1.5)[0] )
        self.assertEqual( len(d2) , 4*6 + 2 + 4 ) 
        for l in l2:
            self.assertTrue( l[0] > 1.5 )



if __name__ == "__main__":
    VERBOSE[0]=True
    unittest.main()
